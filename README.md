# Mercurio :: Algar Telecom

* [System Properties](#Sytem properties)
* [Settings](#Settings)
* [Configuring SSL - standalone](#SSL-Standalone)
* [Access Control](#access control)

###System properties
* mercurio.cassandra.url
* mercurio.ldap.uri
* mercurio.ldap.password
* mercurio.cassandra.port
* mercurio.cassandra.keyspace
* mercurio.ldap.user

###Settings
* **mercurio.default.subscriber.type** -> The default value to be used by the SPS Resource when adding a new  subscriber whith a BSM not present into the SubscriberTypes.It must be a valid SubscriberType

###SSL-Standalone

1. Create a keystore with java
  * keytool -genkey -alias mercurio -keyalg RSA -sigalg MD5withRSA -keystore  **{path to keystore}**/mercurio.keystore -storepass secret  -keypass **{keystore password}** -validity 9999


2. Execute with jboss-cli
  * /core-service=management/security-realm=SSLRealm/server-identity=ssl:add( \
   keystore-path=**{path to keystore}**/mercurio.keystore, \
   keystore-password=**{keystore password}**, \
   alias=mercurio)
  * /subsystem=undertow/server=default-server/https-listener=https:add(socket-binding=https, security-realm=SSLRealm)
  * /subsystem=undertow/server=default-server/http-listener=default:remove()
  * /subsystem=remoting/http-connector=http-remoting-connector:write-attribute(name=connector-ref,value=https)

###Access Control

ROLES registered in cal:

* **ROLE_ADMIN** -> access to everything
* **ROLE_OPERACAO_GERAL** -> configurable
* **ROLE_OPERACAO_PLATAFORMAS** -> configurable
* **ROLE_ATENDIMENTO** -> configurable
* **ROLE_AID** -> configurable

The permissions of each configurable role can be set in the *roles* and *access-control* screen, that can be accessed only by the **ADMIN** user.