package br.com.algartelecom.mercurio.service.web.dto;

import br.com.algartelecom.mercurio.smpp.core.model.ServerBind;

public class ServerBindDecoratorDto {

  private ServerBind serverBind;
  private String server;
  
  public ServerBindDecoratorDto(ServerBind serverBind, String server) {
    this.serverBind = serverBind;
    this.server = server;
  }
  
  public ServerBind getServerBind() {
    return serverBind;
  }
  public void setServerBind(ServerBind serverBind) {
    this.serverBind = serverBind;
  }
  public String getServer() {
    return server;
  }
  public void setServer(String server) {
    this.server = server;
  }

}
