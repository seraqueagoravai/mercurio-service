package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.LARGE_ACCOUNT;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.LargeAccount;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.vw.LargeAccountVw;
import br.com.algartelecom.mercurio.relational.repositories.LargeAccountRepository;
import br.com.algartelecom.mercurio.relational.repositories.vw.LargeAccountVwRepository;

@Service
public class LargeAccountService {

	@Autowired
	LargeAccountRepository largeAccountRepository;

	@Autowired
	LargeAccountVwRepository largeAccountVwRepository;

	@Autowired
	AuditService auditService;

	@Transactional(readOnly = true)
	public Iterable<LargeAccount> getAll() {
		return largeAccountRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Iterable<LargeAccountVw> getAllVw() {
		return largeAccountVwRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<LargeAccount> getLargeAccount(String number) {
		return Optional.ofNullable(largeAccountRepository.findByLargeAccountNumber(number));
	}

	@Transactional(readOnly = true)
	public Optional<LargeAccount> getLargeAccount(LargeAccount largeAccount) {
		return Optional
				.ofNullable(largeAccountRepository.findByLargeAccountNumber(largeAccount.getLargeAccountNumber()));
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<String> update(LargeAccount after) {
		Optional<LargeAccount> old = getLargeAccount(after);
		if (!old.isPresent()) {
			createError("LargeAccount don't exists", BAD_REQUEST);
		}

		LargeAccountVw before = new LargeAccountVw(old.get());
		after.setUpdateDate(new Date());
		after.setUpdateUser(getLoggedUser());
		largeAccountRepository.save(after);
		
		auditUpdateLog(before, new LargeAccountVw(after));
		return new ResponseEntity<String>(OK);
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<String> createLargeAccount(LargeAccount largeAccount) {
		if (Optional.ofNullable(largeAccountRepository.findByLargeAccountNumber(largeAccount.getLargeAccountNumber()))
				.isPresent()) {
			return createError("LargeAccount already exists", BAD_REQUEST);
		}

		largeAccount.setCreationDate(new Date());
		largeAccount.setCreationUser(getLoggedUser());
		largeAccountRepository.save(largeAccount);
		auditLog(LogAction.SAVE, new LargeAccountVw(largeAccount));
		return new ResponseEntity<String>(OK);
	}

	@Transactional(rollbackFor = Exception.class)
	public void delete(LargeAccount largeAccount) {
		largeAccountRepository.deleteByLargeAccountNumber(largeAccount.getLargeAccountNumber());
		auditLog(LogAction.DELETE, new LargeAccountVw(largeAccount));
	}

	private void auditLog(LogAction action, LargeAccountVw largeAccount) {
		auditService.register(getLoggedUser(), LARGE_ACCOUNT, action, largeAccount.getLargeAccountNumber(),
				largeAccount);
	}

	private void auditUpdateLog(LargeAccountVw before, LargeAccountVw after) {
		auditService.registerUpdate(getLoggedUser(), LARGE_ACCOUNT, before.getLargeAccountNumber(), before, after);
	}

}
