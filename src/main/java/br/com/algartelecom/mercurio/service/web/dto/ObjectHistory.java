package br.com.algartelecom.mercurio.service.web.dto;

public class ObjectHistory {

  private Object before;
  private Object after;

  public ObjectHistory() {}

  public ObjectHistory(Object before, Object after) {
    this.before = before;
    this.after = after;
  }

  public Object getBefore() {
    return before;
  }

  public void setBefore(Object before) {
    this.before = before;
  }

  public Object getAfter() {
    return after;
  }

  public void setAfter(Object after) {
    this.after = after;
  }



}
