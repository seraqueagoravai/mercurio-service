package br.com.algartelecom.mercurio.service.web.dto;


public class AccessControlUpdateDto {
  
  private AccessControl accessControlOld;
  
  private AccessControl accessControlNew;

  public AccessControl getAccessControlOld() {
    return accessControlOld;
  }

  public void setAccessControlOld(AccessControl accessControlOld) {
    this.accessControlOld = accessControlOld;
  }

  public AccessControl getAccessControlNew() {
    return accessControlNew;
  }

  public void setAccessControlNew(AccessControl accessControlNew) {
    this.accessControlNew = accessControlNew;
  }
  
  

}
