package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.service.web.dto.AccessControl;
import br.com.algartelecom.mercurio.service.web.dto.AccessControlUpdateDto;
import br.com.algartelecom.mercurio.service.web.service.AccessControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/accesscontrol")
public class AccessControlResource {

  @Autowired
  AccessControlService accessControlService;

  @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
  public Set<AccessControl> getAll() {
    return accessControlService.getAllAccessControl();
  }

  @RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> update(@RequestBody AccessControlUpdateDto access) {
    accessControlService.updateAccessControl(access);
    return new ResponseEntity<>(OK);
  }

}
