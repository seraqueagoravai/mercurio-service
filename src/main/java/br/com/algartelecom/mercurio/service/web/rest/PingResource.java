package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.service.web.dto.Ping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/ping")
public class PingResource {

	@RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	public Ping get() {
		return new Ping(10);
	}

}
