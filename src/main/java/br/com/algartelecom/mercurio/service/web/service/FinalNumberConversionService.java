package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.FINAL_NUMBER_CONVERSION;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.FinalFormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.FinalNumberConversion;
import br.com.algartelecom.mercurio.relational.model.vo.FormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.vw.FinalNumberConversionVw;
import br.com.algartelecom.mercurio.relational.repositories.FinalFormatRuleRepository;
import br.com.algartelecom.mercurio.relational.repositories.FinalNumberConversionRepository;
import br.com.algartelecom.mercurio.relational.repositories.vw.FinalNumberConversionVwRepository;
import br.com.algartelecom.mercurio.service.web.dto.FinalNumberConversionCrudDto;

@Service
public class FinalNumberConversionService {

	@Autowired
	FinalNumberConversionRepository finalNumberConversionRepository;

	@Autowired
	FinalNumberConversionVwRepository finalNumberConversionVwRepository;
	
	@Autowired
	FinalFormatRuleRepository finalFormatRuleRepository;

	@Autowired
	AuditService auditService;

	@Transactional(readOnly = true)
	public Optional<FinalNumberConversion> getFinalNumberConversion(String destinationAccount) {
		return Optional.ofNullable(finalNumberConversionRepository.findByDestinationAccount_bindUsername(destinationAccount));
	}

	@Transactional(readOnly = true)
	public Iterable<FinalNumberConversionVw> getAll(){
		return finalNumberConversionVwRepository.findAll();
	}

	@Transactional( rollbackFor = Exception.class )
	public ResponseEntity<String> update(FinalNumberConversionCrudDto after) {
		Optional<FinalNumberConversion> old = getFinalNumberConversion(after.getDestinationAccount().getBindUsername());
		if(!old.isPresent()){
			return createError("Final Number Conversion don't exists", BAD_REQUEST);
		}

		FinalNumberConversion before = new FinalNumberConversion(old.get());
		FinalNumberConversion finalNumberConversionNew = new FinalNumberConversion(before.getId(), after.getDestinationAccount(),
				before.getCreationDate(), before.getCreationUser(), new Date(), getLoggedUser(), null);

		List<Long> rulesListIdNew = updateFinalFormatRule(before, after.getRules(), finalNumberConversionNew);

		finalNumberConversionRepository.save(finalNumberConversionNew);
		auditUpdateLog(new FinalNumberConversionVw(before, addOldrulesList(before)),
				new FinalNumberConversionVw(finalNumberConversionNew, rulesListIdNew));
		return new ResponseEntity<>(OK);
	}

	private List<Long> updateFinalFormatRule(FinalNumberConversion before, List<FormatRule> ruleListNew, FinalNumberConversion finalNumberConversionNew){
		List<Long> rulesListIdNew = new ArrayList<>();
		ruleListNew.stream().forEach(rulesNew -> {
			Optional<FinalFormatRule> opt = StreamSupport.stream(before.getFinalFormatRules().spliterator(), false)
					.filter(old -> old.getFormatRule().equals(rulesNew)).findAny();

			if (!opt.isPresent()){
				FinalFormatRule finalFormatRule = new FinalFormatRule(null, rulesNew, finalNumberConversionNew,
						new Date(), getLoggedUser());
				finalFormatRuleRepository.save(finalFormatRule);
				rulesListIdNew.add(finalFormatRule.getFormatRule().getId());
			} else {
				rulesListIdNew.add(rulesNew.getId());
			}
		});

		before.getFinalFormatRules().stream().forEach(rulesOld -> {
			if (!ruleListNew.contains(rulesOld.getFormatRule()))
				finalFormatRuleRepository.deleteById(rulesOld.getId());
		});

		return rulesListIdNew;
	}

	private List<Long> addOldrulesList(FinalNumberConversion before) {
		List<Long> rulesListIdOld = new ArrayList<>();
		before.getFinalFormatRules().forEach(c -> rulesListIdOld.add(c.getFormatRule().getId()));
		return rulesListIdOld;
	}

	@Transactional( rollbackFor = Exception.class )
	public ResponseEntity<String> createFinalNumberConversion(FinalNumberConversionCrudDto finalNumberConversionCrudDto) {
		if (getFinalNumberConversion(finalNumberConversionCrudDto.getDestinationAccount().getBindUsername()).isPresent()){
			return createError("Final Number Conversion already exists", BAD_REQUEST);
		}

		FinalNumberConversion finalNumberConversion = new FinalNumberConversion(null, finalNumberConversionCrudDto.getDestinationAccount(),
				new Date(), getLoggedUser(), null, null, null);
		finalNumberConversionRepository.save(finalNumberConversion);
		List<Long> rulesListId = new ArrayList<>();

		for (FormatRule formatRule : finalNumberConversionCrudDto.getRules()) {
			FinalFormatRule finalFormatRule = new FinalFormatRule(null, formatRule, finalNumberConversion,
					new Date(), getLoggedUser());
			finalFormatRuleRepository.save(finalFormatRule);
			rulesListId.add(finalFormatRule.getFormatRule().getId());
		}

		auditLog(LogAction.SAVE, new FinalNumberConversionVw(finalNumberConversion, rulesListId));
		return new ResponseEntity<>(OK);
	}

	@Transactional( rollbackFor = Exception.class )
	public void delete(FinalNumberConversion finalNumberConversion) {
		FinalNumberConversionVw finalNumberConversionVw = new FinalNumberConversionVw(finalNumberConversion, getRulesList(finalNumberConversion));
		finalFormatRuleRepository.deleteByFinalNumberConversion(finalNumberConversion);
		finalNumberConversionRepository.deleteByDestinationAccount_bindUsername(finalNumberConversion.getDestinationAccount().getBindUsername());
		auditLog(LogAction.DELETE, finalNumberConversionVw);
	}

	private List<Long> getRulesList(FinalNumberConversion finalNumberConversion) {
		List<Long> rulesListId = new ArrayList<>();
		finalNumberConversion.getFinalFormatRules().stream().forEach(c -> rulesListId.add(c.getFormatRule().getId()));

		return rulesListId;
	}

	private void auditLog(LogAction action, FinalNumberConversionVw finalNumberConversionVw) {
		auditService.register(getLoggedUser(), FINAL_NUMBER_CONVERSION, action,
				finalNumberConversionVw.getDestinationAccount(), finalNumberConversionVw);
	}

	private void auditUpdateLog(FinalNumberConversionVw before, FinalNumberConversionVw after) {
		auditService.registerUpdate(getLoggedUser(), FINAL_NUMBER_CONVERSION, before.getDestinationAccount(), before,
				after);
	}
}
