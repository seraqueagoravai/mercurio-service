package br.com.algartelecom.mercurio.service.web.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexResource {

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String getMainPage() {
    return "index.html";
  }

}
