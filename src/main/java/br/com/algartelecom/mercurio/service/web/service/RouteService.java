package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.ROUTE;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.Route;
import br.com.algartelecom.mercurio.relational.model.vo.RoutesRules;
import br.com.algartelecom.mercurio.relational.repositories.RouteRepository;
import br.com.algartelecom.mercurio.relational.repositories.RoutesRulesRepository;

@Service
public class RouteService {

	@Autowired
	RouteRepository routeRepository;
	
	@Autowired
	RoutesRulesRepository routesRulesRepository;
	
	@Autowired
	AuditService auditService;

	public Optional<Route> getRoute(String sourceAccount) {
		return Optional.ofNullable(routeRepository.findBySourceAccount_bindUsername(sourceAccount));
	}

	public Optional<Route> getRoute(Route route) {
		return Optional.ofNullable(routeRepository.findBySourceAccount_bindUsername(route.getSourceAccount().getBindUsername()));
	}

	public List<Route> getAll(){
		return routeRepository.findAll();
	}
	
	public ResponseEntity<String> update(Route before, Route after) {
		after.setUpdateDate(new Date());
		after.setUpdateUser(getLoggedUser());
        
        if(!CollectionUtils.isEmpty(after.getRoutesRules())) {
        	for(RoutesRules routesRule : after.getRoutesRules()) {
        		if(routesRule.getCreationDate() == null && routesRule.getCreationUser() == null) {
        			routesRule.setCreationDate(new Date());
            		routesRule.setCreationUser(getLoggedUser());
//        		} else {
//        			routesRule.setUpdateDate(new Date());
//        			routesRule.setUpdateUser(getLoggedUser());
        		}
        		routesRulesRepository.save(routesRule);
        	}
        }
        
        if(!CollectionUtils.isEmpty(after.getRoutesRules())) {
        	for(RoutesRules routesRule : before.getRoutesRules()) {
        		if(!after.getRoutesRules().contains(routesRule)) {
        			routesRulesRepository.deleteById(routesRule.getId());
        		}
        	}
        }
        
		routeRepository.save(after);
		auditUpdateLog(before, after);
		return new ResponseEntity<String>(OK);
	}

	public ResponseEntity<String> createRoute(Route route) {
		route.setCreationDate(new Date());
		route.setCreationUser(getLoggedUser());

        Route savedRoute = routeRepository.save(route);
        
        if(!CollectionUtils.isEmpty(route.getRoutesRules())) {
        	for(RoutesRules routesRule : route.getRoutesRules()) {
        		routesRule.setRoute(savedRoute);
        		routesRule.setCreationDate(new Date());
        		routesRule.setCreationUser(getLoggedUser());
        		routesRulesRepository.save(routesRule);
        	}
        }
		auditLog(LogAction.SAVE, route);
		return new ResponseEntity<String>(OK);
	}

	public void delete(Route route) {
		
		if(!CollectionUtils.isEmpty(route.getRoutesRules())) {
    		for(RoutesRules routesRules : route.getRoutesRules()) {
    			routesRulesRepository.deleteById(routesRules.getId());
    		}
    	}
		
		routeRepository.deleteBySourceAccount_Id(route.getSourceAccount().getId());
		auditLog(LogAction.DELETE, route);
	}

	private void auditLog(LogAction action, Route route) {
		auditService.register(getLoggedUser(), ROUTE, action, route.getSourceAccount().getBindUsername(), route);
	}

	private void auditUpdateLog(Route before, Route after) {
		auditService.registerUpdate(getLoggedUser(), ROUTE, before.getSourceAccount().getBindUsername(), before, after);
	}
}
