package br.com.algartelecom.mercurio.service.web.dto;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.algartelecom.mercurio.relational.model.vo.AuditLog;

public class LogDto {

	private static final Logger log = LoggerFactory.getLogger(LogDto.class);

	private String user;
	private String entity;
	private String action;
	private String entityId;
	private Date date;
	private Long id;
	private Map content;

	public LogDto() {
	}

	public LogDto(AuditLog log) {
		ObjectMapper mapper = new ObjectMapper();
		this.user = log.getUsername();
		this.entity = log.getEntity();
		this.action = log.getAction();
		this.entityId = log.getEntityId();
		this.date = log.getDateTime();
		this.id = log.getId();
		this.date = log.getDateTime();
		try {
			this.content = mapper.readValue(log.getContent(), HashMap.class);
		} catch (IOException e) {
			this.log.error("Error parsing json ->", e);
			this.content = new HashMap<>();
		}
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, Object> getContent() {
		return content;
	}

	public void setContent(Map<String, Object> content) {
		this.content = content;
	}

}
