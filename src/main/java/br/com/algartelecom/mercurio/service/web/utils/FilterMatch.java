package br.com.algartelecom.mercurio.service.web.utils;

import br.com.algartelecom.mercurio.relational.model.vo.vw.FormatRuleVw;
import br.com.algartelecom.mercurio.relational.model.vo.vw.LargeAccountVw;
import br.com.algartelecom.mercurio.relational.util.NumberPatternService;

public class FilterMatch {

  String number;
  String numberB;
  NumberPatternService patternService;

  public FilterMatch(String number, NumberPatternService patternService) {
    this.number = number;
    this.patternService = patternService;
  }

  public FilterMatch(String number, String numberB, NumberPatternService patternService) {
    this.number = number;
    this.numberB = numberB;
    this.patternService = patternService;
  }

  public boolean filterMatch(LargeAccountVw account) {
    return patternService.validateRegex(account.getLargeAccountNumber(), number);
  }

  public boolean filterMatch(FormatRuleVw rule) {
    return patternService.validateRegex(rule.getNumberAPattern(), number)
        && patternService.validateRegex(rule.getNumberBPattern(), numberB);
  }


}
