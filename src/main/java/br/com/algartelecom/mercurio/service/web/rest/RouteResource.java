package br.com.algartelecom.mercurio.service.web.rest;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.Route;
import br.com.algartelecom.mercurio.service.web.service.RouteService;

@RestController
@RequestMapping("/route")
public class RouteResource {

	@Autowired
	RouteService routeService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<Route> getAll() {
		return routeService.getAll();
	}

	@RequestMapping(value = "/{sourceAccount}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Route> get(@PathVariable String sourceAccount) {
		return routeService.getRoute(sourceAccount)
				.map(result -> new ResponseEntity<Route>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody Route route) throws URISyntaxException {
		return routeService.getRoute(route).map(old -> createError("Route already exists", BAD_REQUEST)).orElse(routeService.createRoute(route));
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody Route route) throws URISyntaxException {
		return routeService.getRoute(route).map(old -> routeService.update(old, route)).orElse(createError("Route don't exists", BAD_REQUEST));
	}

	@RequestMapping(value = "/{sourceAccount}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public void delete(@PathVariable String sourceAccount) {
		routeService.getRoute(sourceAccount).ifPresent(routeService::delete);
	}

}
