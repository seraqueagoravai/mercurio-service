package br.com.algartelecom.mercurio.service.web.utils;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.algartelecom.mercurio.relational.model.vo.ClusterMember;
import br.com.algartelecom.mercurio.service.web.dto.BindCommandRespDto;
import br.com.algartelecom.mercurio.smpp.core.model.AccountConnectionsHolder;
import br.com.algartelecom.mercurio.smpp.core.model.CommandResp;
import br.com.algartelecom.mercurio.smpp.core.model.Esme;
import br.com.algartelecom.mercurio.smpp.core.model.ServerBind;
import br.com.algartelecom.mercurio.smpp.core.util.MapEsme;

@Service
@Scope(scopeName = SCOPE_PROTOTYPE)
public class SmppCommander {

  private static final Logger logger = LoggerFactory.getLogger(SmppCommander.class);

  @Autowired
  BindSmppUtils bindSmppUtils;
  @Autowired
  RestTemplate restTemplate;
  @Value("${mercurio.restart.bind.url}")
  String restartBindUrl;
  @Value("${mercurio.restart.all.binds.url}")
  String restartAllBindsUrl;
  @Value("${mercurio.show.bind.url}")
  String showBindUrl;
  @Value("${mercurio.show.esme.registered.url}")
  String showEsmeRegistered;
  @Value("${mercurio.stop.bind.url}")
  String stopBindUrl;
  @Value("${mercurio.start.new.bind.url}")
  String startNewBind;
  @Value("${mercurio.add.esme.server.url}")
  String addEsmeServer;
  @Value("${mercurio.get.binds.connections.url}")
  String getBindsConnectionsUrl;

  private ClusterMember smpp;

  public Optional<CommandResp> stopBind(String username) {
    String url =
        bindSmppUtils.replaceUrlWithParam(stopBindUrl, smpp.getIp(), smpp.getPort(), username);
    return makeGetRequest(url, CommandResp.class,
        e -> createErrorOptional("Error stop bind", smpp, e));
  }

  public Optional<CommandResp> startBind(String username) {
    ServerBind server = bindSmppUtils.createServerBind(username);
    String url = bindSmppUtils.replaceUrl(startNewBind, smpp.getIp(), smpp.getPort());
    return makePostRequest(url, server, CommandResp.class,
        e -> createErrorOptional("Error start bind", smpp, e));
  }

  public Optional<CommandResp> addEsme(Long id) {
    Esme esme = bindSmppUtils.convertEsme(id);
    String url = bindSmppUtils.replaceUrl(addEsmeServer, smpp.getIp(), smpp.getPort());
    return makePostRequest(url, esme, CommandResp.class,
        e -> createErrorOptional("Error add bind esme", smpp, e));
  }

  public Optional<BindCommandRespDto> executeRestartBind(ServerBind server) {
    String url = bindSmppUtils.replaceUrl(restartBindUrl, smpp.getIp(), smpp.getPort());
    return makePostRequest(url, server, CommandResp.class,
        e -> createErrorOptional("Error restart bind", smpp, e))
            .map(resp -> new BindCommandRespDto(resp, getClusterMemberNameString(smpp)));
  }

  public Optional<BindCommandRespDto> sendRestartAllBindsCommandToSmpp() {
    String url = bindSmppUtils.replaceUrl(restartAllBindsUrl, smpp.getIp(), smpp.getPort());
    return makeGetRequest(url, CommandResp.class,
        e -> createErrorOptional("Error restarting all binds", smpp, e))
            .map(resp -> new BindCommandRespDto(resp, getClusterMemberNameString(smpp)));
  }

  public Optional<ServerBind[]> getAllBindsFromSmpp() {
    String url = bindSmppUtils.replaceUrl(showBindUrl, smpp.getIp(), smpp.getPort());
    return makeGetRequest(url, ServerBind[].class, e -> Optional.empty());
  }

  public MapEsme getEsmeFromSmpp() {
    String url = bindSmppUtils.replaceUrl(showEsmeRegistered, smpp.getIp(), smpp.getPort());
    return makeGetRequest(url, MapEsme.class, e -> Optional.empty()).map(map -> map)
        .orElse(new MapEsme());
  }

  public Optional<AccountConnectionsHolder[]> getBindsConnections() {
    String url = bindSmppUtils.replaceUrl(getBindsConnectionsUrl, smpp.getIp(), smpp.getPort());
    return makeGetRequest(url, AccountConnectionsHolder[].class, e -> Optional.empty());
  }

  private String getClusterMemberNameString(ClusterMember clusterMember) {
    return clusterMember.getName() + " - " + clusterMember.getIp() + ":" + clusterMember.getPort();
  }

  private <T> Optional<T> makeGetRequest(String url, Class<T> responseType,
      Function<Exception, Optional<T>> onError) {
    return makeRequest(() -> restTemplate.getForObject(url, responseType), onError);
  }

  private <T> Optional<T> makePostRequest(String url, Object body, Class<T> responseType,
      Function<Exception, Optional<T>> onError) {
    return makeRequest(() -> restTemplate.postForObject(url, body, responseType), onError);
  }

  private <T> Optional<T> makeRequest(Supplier<T> requestSupplier,
      Function<Exception, Optional<T>> onError) {
    try {
      return Optional.ofNullable(requestSupplier.get());
    } catch (Exception e) {
      logger.error("Error making request to smpp :: url -> {} ", e);
      return onError.apply(e);
    }
  }

  private Optional<CommandResp> createErrorOptional(String message, ClusterMember smpp,
      Exception e) {
    return Optional.of(bindSmppUtils.createResponseError(message, smpp, e));
  }

  public ClusterMember getSmpp() {
    return smpp;
  }

  public void setSmpp(ClusterMember smpp) {
    this.smpp = smpp;
  }

  public String getSmppName() {
    return getClusterMemberNameString(smpp);
  }

}
