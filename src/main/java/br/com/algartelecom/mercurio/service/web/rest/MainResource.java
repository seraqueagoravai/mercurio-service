package br.com.algartelecom.mercurio.service.web.rest;

//import static br.com.algartelecom.analytics.model.StatisticsConstants.RECEIVED;
//import static br.com.algartelecom.analytics.model.StatisticsConstants.SENT;
import static br.com.algartelecom.mercurio.service.web.dto.BindStatusDto.CONNECTED;
import static br.com.algartelecom.mercurio.service.web.dto.BindStatusDto.DISCONNECTED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

//import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.algartelecom.mercurio.relational.model.vo.Bind;
import br.com.algartelecom.mercurio.relational.model.vo.ClusterMember;
import br.com.algartelecom.mercurio.relational.repositories.BindRepository;
import br.com.algartelecom.mercurio.service.web.dto.BindStatusDto;
import br.com.algartelecom.mercurio.service.web.dto.GraphRequestDto;
import br.com.algartelecom.mercurio.service.web.dto.SMSQueuedDto;
import br.com.algartelecom.mercurio.service.web.service.ClusterMemberService;
import br.com.algartelecom.mercurio.service.web.utils.BindSmppUtils;
import br.com.algartelecom.mercurio.service.web.utils.DateUtils;
import br.com.algartelecom.mercurio.smpp.core.model.ServerBind;

@RestController
@RequestMapping("/main")
public class MainResource {

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ClusterMemberService clusterMemberService;
	@Autowired
	BindSmppUtils bindSmppUtils;
	@Autowired
	BindRepository bindRepository;
//	@Autowired
//	GraphStatisticsRepository queryRepository;
//	@Autowired
//	ShortMessageStatisticsRepository smsRepository;
//	@Autowired
//	HazelcastInstance hazelInstance;
	@Value("${mercurio.show.bind.url}")
	String showBindUrl;
	private Instant lastRequestSent;
//	List<SMSDataSeries> cacheDtoSent = null;
	private Instant lastRequestReceiv;
	List<SMSQueuedDto> cacheDtoReceiv = null;
	private Instant lastRequestFifty;
//	List<SMSDataQuery> cacheDtoFifty = null;

	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	private static final Logger logger = LoggerFactory.getLogger(MainResource.class);

	@RequestMapping(value = "/show-binds", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<BindStatusDto> showBinds() throws URISyntaxException {
		return getStatusBind();
	}

	@RequestMapping(value = "/graph/generate", method = GET)
    public String getErrorsByTypeAndDate() throws ParseException {
//	public GraphRepresentation getErrorsByTypeAndDate() throws ParseException {
//		if (cacheSentExpired()) {
//			List<SMSDataSeries> resp = getNumberData(createGraphRequest(SENT));
//			cacheDtoSent = resp;
//			lastRequestSent = Instant.now();
//		}
//		return transformResponse(cacheDtoSent);
        return null;
	}

	@RequestMapping(value = "/top-sent", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<SMSQueuedDto> getTopSent() throws ParseException {
//		if (cacheDtoSent != null)
//			return transformResult(cacheDtoSent, SENT);
//		else
//			return transformResult(getNumberData(createGraphRequest(SENT)), SENT);
        return null;
	}

	@RequestMapping(value = "/top-received", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<SMSQueuedDto> getTopReceived() throws ParseException {
//		if (cacheReceivExpired()) {
//			List<SMSQueuedDto> result = transformResult(getNumberData(createGraphRequest(RECEIVED)), RECEIVED);
//			cacheDtoReceiv = result;
//			lastRequestReceiv = Instant.now();
//		}
//		return cacheDtoReceiv;
        return null;
	}

	@RequestMapping(value = "/last-sms", method = GET, produces = APPLICATION_JSON_VALUE)
    public List<String> getLastSMS() throws URISyntaxException {
//	public List<SMSDataQuery> getLastSMS() throws URISyntaxException {
//		if (cacheFiftyExpired()) {
//			List<SMSDataQuery> result = getLastFiftySMS();
//			cacheDtoFifty = result;
//			lastRequestFifty = Instant.now();
//		}
//		return cacheDtoFifty;
        return null;
	}

//	private List<SMSQueuedDto> transformResult(List<SMSDataSeries> series, String type) {
//		return series.stream().map(data -> createSMSQueued(data, type)).collect(Collectors.toList());
//	}
//
//	private GraphRepresentation transformResponse(List<SMSDataSeries> series) {
//		GraphRepresentation result = new GraphRepresentation();
//		List<Integer> resultData = new ArrayList<Integer>();
//		for (SMSDataSeries data : series) {
//			String metric = data.getMetric() != null ? data.getMetric() : "null";
//			result.getLabels().add(metric.split(" ")[0]);
//			resultData.add(data.getTotal());
//		}
//		result.getData().add(resultData);
//		return result;
//	}
//
//	private List<SMSDataSeries> getNumberData(GraphRequestDto r) throws ParseException {
//		String number = "specify".equals(r.number) ? r.customNumber : r.number;
//		return queryRepository.findByNumber(parseDate(false), parseDate(true), number, r.type);
//	}

	public List<ServerBind> getAllBinds() {
		List<ServerBind> list = new ArrayList<>();
		getListClusterMember().stream().filter(s -> s.getType().equals("smpp")).forEach(smpp -> {
			String url = bindSmppUtils.replaceUrl(showBindUrl, smpp.getIp(), smpp.getPort());
			try {
				ServerBind[] servers = restTemplate.getForObject(url, ServerBind[].class);
				if (servers.length > 0)
					Arrays.stream(servers).forEach(s -> list.add(s));
			} catch (Exception e) {
				logger.error("Erro ao carregar lista de binds para url: " + url, e);
			}
		});
		return list;
	}

	public List<BindStatusDto> getStatusBind() {
		boolean hasBind = false;
		List<BindStatusDto> result = new ArrayList<>();
		List<ServerBind> servers = getAllBinds();
		String[] binds = getAccounts();
		if (binds != null && servers != null) {
			for (String bind : binds) {
				hasBind = false;
				for (int i = 0; i < servers.size(); i++) {
					if (servers.get(i).getName().equals(bind)) {
						result.add(new BindStatusDto(bind, CONNECTED));
						hasBind = true;
						servers.remove(i);
						break;
					}
				}
				if (!hasBind)
					result.add(new BindStatusDto(bind, DISCONNECTED));
			}
		}
		return result;
	}

	public String[] getAccounts() {
		return bindRepository.findAll().stream().map(Bind::getBindUsername).sorted().toArray(String[]::new);
	}

	public List<ClusterMember> getListClusterMember() {
		return clusterMemberService.getAllClusterMembers();
	}

//	public List<SMSDataQuery> getLastFiftySMS() {
//		return queryRepository.findLastFiftySMS();
//	}

	private boolean cacheReceivExpired() {
		if (cacheDtoReceiv == null || lastRequestReceiv.isBefore(Instant.now().minusSeconds(12)))
			return true;
		return false;
	}

//	private boolean cacheSentExpired() {
//		if (cacheDtoSent == null || lastRequestSent.isBefore(Instant.now().minusSeconds(12)))
//			return true;
//		return false;
//	}
//
//	private boolean cacheFiftyExpired() {
//		if (cacheDtoFifty == null || lastRequestFifty.isBefore(Instant.now().minusSeconds(12)))
//			return true;
//		return false;
//	}

//	private SMSQueuedDto createSMSQueued(SMSDataSeries data, String type) {
//		SMSQueuedDto sms = new SMSQueuedDto();
//		String metric = data.getMetric() != null ? data.getMetric() : "null";
//		sms.setNumber(metric.split(" ")[0]);
//		sms.setTotalSMS(data.getTotal());
//		if (SENT.equals(type))
//			sms.setTotalQueued(hazelInstance.getQueue(createSmaKey(sms.getNumber())).size());
//		else
//			sms.setTotalQueued(hazelInstance.getQueue(createSmbKey(sms.getNumber())).size());
//		return sms;
//	}

	private Date parseDate(boolean last) throws ParseException {
		formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date data = new Date();
		String aux = formatter.format(data);
		Date result = formatter.parse(aux);
		if (last)
			result = DateUtils.setToLastSecondOfDay(result);
		return result;
	}

	private GraphRequestDto createGraphRequest(String type) {
		Date data = new Date();
		String date = formatter.format(data);
		return new GraphRequestDto(type, "all", date, date, "date", "top 10", "all");
	}

	private String createSmaKey(String number) {
		return "A:" + number;
	}

	private String createSmbKey(String number) {
		return "B:" + number;
	}

}
