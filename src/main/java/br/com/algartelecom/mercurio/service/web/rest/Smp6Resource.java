//package br.com.algartelecom.mercurio.service.web.rest;
//
//import br.com.algartelecom.mercurio.common.model.vo.Smp6;
//import br.com.algartelecom.mercurio.common.repository.Smp6Repository;
//import br.com.algartelecom.mercurio.service.web.dto.DatePair;
//import br.com.algartelecom.mercurio.service.web.dto.Smp6Entry;
//import br.com.algartelecom.mercurio.service.web.service.AuditService;
//import br.com.algartelecom.mercurio.service.web.views.Smp6ReportExcelView;
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.io.UnsupportedEncodingException;
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.ZoneId;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
//import static org.springframework.web.bind.annotation.RequestMethod.GET;
//
//@RestController
//@RequestMapping("/smp6")
//public class Smp6Resource {
//
//  @Autowired
//  Smp6Repository smp6Repository;
//  @Autowired
//  AuditService auditService;
//  SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
//  SimpleDateFormat dateFormatterQuery = new SimpleDateFormat("ddMMyyyy");
//  ObjectMapper mapper = new ObjectMapper();
//  private static final Logger log = LoggerFactory.getLogger(Smp6Resource.class);
//
//  @RequestMapping(value = "{dates}", method = GET, produces = APPLICATION_JSON_VALUE)
//  public ModelAndView getSmp6Report(@PathVariable String dates) {
//    dates = parseStringDate(dates);
//    List<DatePair> datesList = convertDatesToJson(dates);
//    Map<Date, Map> entries = getSmp6Entries(datesList);
//    ModelAndView model = new ModelAndView("smp6ReportExcelView");
//    model.addObject("entries", entries);
//    model.setView(new Smp6ReportExcelView());
//    return model;
//  }
//
//  public String parseStringDate(String date){
//    try {
//      return java.net.URLDecoder.decode(date, "UTF-8");
//    } catch (UnsupportedEncodingException e) {
//      log.error("Error", e);
//      return null;
//    }
//  }
//
//  private List<DatePair> convertDatesToJson(String dates) {
//    try {
//      mapper.setDateFormat(dateFormatterQuery);
//      return mapper.readValue(dates,  new TypeReference<List<DatePair>>(){});
//    } catch (Exception e) {
//      log.error("Error parsing dates list -> {}",e);
//      return null;
//    }
//  }
//
//  private Map<Date, Map> getSmp6Entries(List<DatePair> dates) {
//    return fillDatesMap(dates);
//  }
//
//  private Map<Date, Map> fillDatesMap(List<DatePair> dates) {
//    Map<Date, Map> datesMap = new HashMap<Date, Map>();
//    for (DatePair pair : dates) {
//      if (pair.getFinalDate() == null) {
//        datesMap.put(pair.getInitDate(),
//            createCnMap(smp6Repository.findByDate(convertSmp6Date(pair.getInitDate()))));
//      } else {
//        LocalDate date1 = convertDateToLocalDate(pair.getInitDate());
//        LocalDate date2 = convertDateToLocalDate(pair.getFinalDate());
//        while (!date1.isAfter(date2)) {
//          datesMap.put(convertLocalDateToDate(date1),
//              createCnMap(smp6Repository.findByDate(convertSmp6Date(date1))));
//          date1.plusDays(1);
//        }
//      }
//    }
//    return datesMap;
//  }
//
//  private Map<String, Smp6Entry> createCnMap(List<Smp6> smp6List) {
//    Map<String, Smp6Entry> cnMap = new HashMap<String, Smp6Entry>();
//    for (Smp6 smp : smp6List) {
//      String cn = smp.getCn();
//      Smp6Entry entry = calculate(cnMap.get(cn), smp.getDeliveryTime());
//      cnMap.put(cn, entry);
//    }
//    return cnMap;
//  }
//
//  private Smp6Entry calculate(Smp6Entry oldEntry, List<Integer> deliveryTimes) {
//    Smp6Entry entry = oldEntry != null ? oldEntry : new Smp6Entry();
//    entry.setTotal(deliveryTimes.size());
//    entry.setSent((int) deliveryTimes.stream().filter(num -> num <= 60).count());
//    return entry;
//  }
//
//  private String convertSmp6Date(LocalDate localDate) {
//    return convertSmp6Date(convertLocalDateToDate(localDate));
//  }
//
//  private String convertSmp6Date(Date initDate) {
//    return dateFormatter.format(initDate);
//  }
//
//  private LocalDate convertDateToLocalDate(Date date) {
//    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//  }
//
//  private Date convertLocalDateToDate(LocalDate localDate) {
//    return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
//  }
//
//}
