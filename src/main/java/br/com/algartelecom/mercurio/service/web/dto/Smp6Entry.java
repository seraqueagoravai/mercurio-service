package br.com.algartelecom.mercurio.service.web.dto;

public class Smp6Entry {
  
  private int total;
  private int sent;
  
  public int getTotal() {
    return total;
  }
  public void setTotal(int total) {
    this.total = total;
  }
  public int getSent() {
    return sent;
  }
  public void setSent(int sent) {
    this.sent = sent;
  }
  public float getPercentage() {
    return ((float)sent/total) * 100;
  }

}
