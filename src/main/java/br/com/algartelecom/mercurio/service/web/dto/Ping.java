package br.com.algartelecom.mercurio.service.web.dto;

public class Ping {

  private int o;

  public Ping(int o) {
    this.o = o;
  }

  public int getO() {
    return o;
  }

  public void setO(int o) {
    this.o = o;
  }

}
