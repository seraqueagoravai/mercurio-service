package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.FORMAT_RULE;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.algartelecom.mercurio.relational.model.vo.FinalFormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.FormatNumberParts;
import br.com.algartelecom.mercurio.relational.model.vo.FormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.InitialFormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.vw.FormatRuleVw;
import br.com.algartelecom.mercurio.relational.repositories.FinalFormatRuleRepository;
import br.com.algartelecom.mercurio.relational.repositories.FormatNumberPartsRepository;
import br.com.algartelecom.mercurio.relational.repositories.FormatRuleRepository;
import br.com.algartelecom.mercurio.relational.repositories.InitialFormatRuleRepository;
import br.com.algartelecom.mercurio.relational.repositories.vw.FormatRuleVwRepository;

@Service
public class FormatRuleService {

	@Autowired
	FormatRuleRepository formatRuleRepository;
	
	@Autowired
	FormatNumberPartsRepository formatNumberPartsRepository;

	@Autowired
	InitialFormatRuleRepository initialFormatRuleRepository;

	@Autowired
	FinalFormatRuleRepository finalFormatRuleRepository;

	@Autowired
	AuditService auditService;

	@Autowired
	FormatRuleVwRepository formatRuleVwRepository;


	@Transactional(readOnly = true)
	public Optional<FormatRule> getFormatRule(Long id) {
		return formatRuleRepository.findById(id);
	}

	@Transactional(readOnly = true)
	public List<FormatRule> getAll(){
		return formatRuleRepository.findAll();
	}

	@Transactional(readOnly = true)
	public List<FormatNumberParts> getAllNumberParts(){
		return formatNumberPartsRepository.findAll();
	}

	@Transactional(readOnly = true)
	public List<FormatRule> getAllByListId(List<Long> idList){
		return formatRuleRepository.findByIdIn(idList);
	}

	@Transactional( rollbackFor = Exception.class )
	public ResponseEntity<String> update(FormatRule newFormatRule) {
		Optional<FormatRule> old = getFormatRule(newFormatRule.getId());
		if (!old.isPresent()){
			return createError("FormatRule don't exists", BAD_REQUEST);
		}

		FormatRule oldFormatRule = new FormatRule(old.get());
		newFormatRule.setUpdateDate(new Date());
		newFormatRule.setUpdateUser(getLoggedUser());
		updateFormatRule(oldFormatRule, newFormatRule);

		formatRuleRepository.save(newFormatRule);
		auditUpdateLog(oldFormatRule, newFormatRule);
		return new ResponseEntity<>(OK);
	}

	private void updateFormatRule(FormatRule oldFormatRule, FormatRule newFormatRule) {
		newFormatRule.getFormatNumberParts().stream().forEach(formatNumberPartsNew -> {
			Optional<FormatNumberParts> opt = StreamSupport.stream(oldFormatRule.getFormatNumberParts().spliterator(), false)
					.filter(old -> old.equals(formatNumberPartsNew)).findAny();

			if (!opt.isPresent()){
				FormatNumberParts formatNumberParts = new FormatNumberParts(null, formatNumberPartsNew.getPart(), formatNumberPartsNew.getFlowType(),
						formatNumberPartsNew.getFormatRule(), new Date(), getLoggedUser());
				formatNumberPartsRepository.save(formatNumberParts);
			}
		});

		oldFormatRule.getFormatNumberParts().stream().forEach(numberPartsOld -> {
			if (!newFormatRule.getFormatNumberParts().contains(numberPartsOld))
				formatNumberPartsRepository.deleteById(numberPartsOld.getId());
		});
	}

	@Transactional( rollbackFor = Exception.class )
	public ResponseEntity<String> createFormatRule(FormatRule formatRule) {
		formatRule.setCreationDate(new Date());
		formatRule.setCreationUser(getLoggedUser());
		FormatRule savedFormatRule = formatRuleRepository.save(formatRule);

		if (!CollectionUtils.isEmpty(formatRule.getFormatNumberParts())) {
			for (FormatNumberParts numberParts : formatRule.getFormatNumberParts()) {
				FormatNumberParts formatNumberParts = new FormatNumberParts(null, numberParts.getPart(),
						numberParts.getFlowType(), savedFormatRule, new Date(), getLoggedUser());
				formatNumberPartsRepository.save(formatNumberParts);
			}
		}
		
		auditLog(LogAction.SAVE, formatRule);
		return new ResponseEntity<>(OK);
	}

	@Transactional( rollbackFor = Exception.class )
	public ResponseEntity<String> delete(Long id) {
		Optional<FormatRule> formatRuleOptional = getFormatRule(id);
		if (!formatRuleOptional.isPresent())
			return createError("FormatRule don't exists", BAD_REQUEST);

		FormatRule formatRule = new FormatRule(formatRuleOptional.get());
		List<InitialFormatRule> initialFormatRule = initialFormatRuleRepository.findByFormatRule_id(formatRule.getId());
		if(!CollectionUtils.isEmpty(initialFormatRule))
			return createError("FormatRule registered for Initial Number Convertion", BAD_REQUEST);

		List<FinalFormatRule> finalFormatRule = finalFormatRuleRepository.findByFormatRule_id(formatRule.getId());
		if(!CollectionUtils.isEmpty(finalFormatRule))
			return createError("FormatRule registered for Final Number Convertion", BAD_REQUEST);

		formatNumberPartsRepository.deleteByFormatRule(formatRule);
		formatRuleRepository.deleteById(formatRule.getId());
		auditLog(LogAction.DELETE, formatRule);
		return new ResponseEntity<>(OK);
	}

	private void auditLog(LogAction action, FormatRule formatRule) {
		auditService.register(getLoggedUser(), FORMAT_RULE, action, formatRule.getId().toString(), formatRule);
	}

	private void auditUpdateLog(FormatRule before, FormatRule after) {
		auditService.registerUpdate(getLoggedUser(), FORMAT_RULE, before.getId().toString(), before, after);
	}
	
}
