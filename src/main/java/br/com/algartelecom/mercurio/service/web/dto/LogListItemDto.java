package br.com.algartelecom.mercurio.service.web.dto;

public class LogListItemDto {
  
  private String timeuuid;
  private String entity;
  private String date;
  private String user;
  private String action;
  private String entityId;
  
  public String getTimeuuid() {
    return timeuuid;
  }
  public void setTimeuuid(String timeuuid) {
    this.timeuuid = timeuuid;
  }
  public String getEntity() {
    return entity;
  }
  public void setEntity(String entity) {
    this.entity = entity;
  }
  public String getDate() {
    return date;
  }
  public void setDate(String date) {
    this.date = date;
  }
  public String getUser() {
    return user;
  }
  public void setUser(String user) {
    this.user = user;
  }
  public String getAction() {
    return action;
  }
  public void setAction(String action) {
    this.action = action;
  }
  public String getEntityId() {
    return entityId;
  }
  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }
  
  

}
