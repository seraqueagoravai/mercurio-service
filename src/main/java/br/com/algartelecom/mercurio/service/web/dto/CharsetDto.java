package br.com.algartelecom.mercurio.service.web.dto;

public class CharsetDto {
  
  private String name;
  
  public CharsetDto(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
}
