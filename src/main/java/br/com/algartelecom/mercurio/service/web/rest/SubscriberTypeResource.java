package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URISyntaxException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.SubscriberType;
import br.com.algartelecom.mercurio.service.web.service.SubscriberTypeService;

@RestController
@RequestMapping("/subscriber-type")
public class SubscriberTypeResource {

	@Autowired
	SubscriberTypeService subscriberTypeService;

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody SubscriberType subscriberType) throws URISyntaxException {
		return subscriberTypeService.create(subscriberType);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody SubscriberType subscriberType) throws URISyntaxException {
		return subscriberTypeService.update(subscriberType);
	}

	@RequestMapping(value = "/{type}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<SubscriberType> get(@PathVariable String type) {
		return Optional.ofNullable(subscriberTypeService.getByType(type))
				.map(result -> new ResponseEntity<SubscriberType>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<SubscriberType> getAll() {
		return subscriberTypeService.getAll();
	}

	@RequestMapping(value = "/{type}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public void delete(@PathVariable String type) {
		subscriberTypeService.delete(type);
	}

}
