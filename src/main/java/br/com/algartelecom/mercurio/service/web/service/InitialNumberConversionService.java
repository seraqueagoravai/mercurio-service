package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.INITIAL_NUMBER_CONVERSION;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.FormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.InitialFormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.InitialNumberConversion;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.vw.InitialNumberConversionVw;
import br.com.algartelecom.mercurio.relational.repositories.InitialFormatRuleRepository;
import br.com.algartelecom.mercurio.relational.repositories.InitialNumberConversionRepository;
import br.com.algartelecom.mercurio.relational.repositories.vw.InitialNumberConversionVwRepository;
import br.com.algartelecom.mercurio.service.web.dto.InitialNumberConversionCrudDto;

@Service
public class InitialNumberConversionService {

	@Autowired
	InitialNumberConversionRepository initialNumberConversionRepository;

	@Autowired
	InitialFormatRuleRepository initialFormatRuleRepository;

	@Autowired
	InitialNumberConversionVwRepository initialNumberConversionVwRepository;

	@Autowired
	AuditService auditService;

	@Transactional(readOnly = true)
	public Optional<InitialNumberConversion> getInitialNumberConversion(String sourceAccount) {
		return Optional.ofNullable(initialNumberConversionRepository.findBySourceAccount_bindUsername(sourceAccount));
	}

	@Transactional(readOnly = true)
	public Iterable<InitialNumberConversion> getAll() {
		return initialNumberConversionRepository.findAll();
	}

	@Transactional( rollbackFor = Exception.class )
	public ResponseEntity<String> update(InitialNumberConversionCrudDto after) {
		Optional<InitialNumberConversion> old = getInitialNumberConversion(after.getSourceAccount().getBindUsername());
		if(!old.isPresent()){
			return createError("Initial Number Conversion don't exists", BAD_REQUEST);
		}

		InitialNumberConversion before = new InitialNumberConversion(old.get());
		InitialNumberConversion initialNumberConversionNew = new InitialNumberConversion(before.getId(), after.getSourceAccount(),
				before.getCreationDate(), before.getCreationUser(), new Date(), getLoggedUser(), null);

		List<Long> rulesListIdNew = updateInitialFormatRule(before, after.getRules(), initialNumberConversionNew);

		initialNumberConversionRepository.save(initialNumberConversionNew);
		auditUpdateLog(new InitialNumberConversionVw(before, addOldrulesList(before)),
				new InitialNumberConversionVw(initialNumberConversionNew, rulesListIdNew));
		return new ResponseEntity<>(OK);
	}

	private List<Long> updateInitialFormatRule(InitialNumberConversion before, List<FormatRule> ruleListNew, InitialNumberConversion initialNumberConversionNew){
		List<Long> rulesListIdNew = new ArrayList<>();
		ruleListNew.stream().forEach(rulesNew -> {
			Optional<InitialFormatRule> opt = StreamSupport.stream(before.getInitialFormatRules().spliterator(), false)
					.filter(old -> old.getFormatRule().equals(rulesNew)).findAny();

			if (!opt.isPresent()){
				InitialFormatRule initialFormatRule = new InitialFormatRule(null, rulesNew, initialNumberConversionNew,
						new Date(), getLoggedUser());
				initialFormatRuleRepository.save(initialFormatRule);
				rulesListIdNew.add(initialFormatRule.getFormatRule().getId());
			} else {
				rulesListIdNew.add(rulesNew.getId());
			}
		});

		before.getInitialFormatRules().stream().forEach(rulesOld -> {
			if (!ruleListNew.contains(rulesOld.getFormatRule()))
				initialFormatRuleRepository.deleteById(rulesOld.getId());
		});

		return rulesListIdNew;
	}

	private List<Long> addOldrulesList(InitialNumberConversion before) {
		List<Long> rulesListIdOld = new ArrayList<>();
		before.getInitialFormatRules().forEach(c -> rulesListIdOld.add(c.getFormatRule().getId()));
		return rulesListIdOld;
	}

	@Transactional( rollbackFor = Exception.class )
	public ResponseEntity<String> createInitialNumberConversion(InitialNumberConversionCrudDto initialNumberConversionCrudDto) {
		if (getInitialNumberConversion(initialNumberConversionCrudDto.getSourceAccount().getBindUsername()).isPresent()){
			return createError("Initial Number Conversion already exists", BAD_REQUEST);
		}

		InitialNumberConversion initialNumberConversion = new InitialNumberConversion(null, initialNumberConversionCrudDto.getSourceAccount(),
				new Date(), getLoggedUser(), null, null, null);
		initialNumberConversionRepository.save(initialNumberConversion);
		List<Long> rulesListId = new ArrayList<>();

		for (FormatRule formatRule : initialNumberConversionCrudDto.getRules()) {
			InitialFormatRule initialFormatRule = new InitialFormatRule(null, formatRule, initialNumberConversion,
					new Date(), getLoggedUser());
			initialFormatRuleRepository.save(initialFormatRule);
			rulesListId.add(initialFormatRule.getFormatRule().getId());
		}

		auditLog(LogAction.SAVE, new InitialNumberConversionVw(initialNumberConversion, rulesListId));
		return new ResponseEntity<>(OK);
	}

	@Transactional( rollbackFor = Exception.class )
	public void delete(InitialNumberConversion initialNumberConversion) {
		InitialNumberConversionVw initialNumberConversionVw = new InitialNumberConversionVw(initialNumberConversion, getRulesList(initialNumberConversion));
		initialFormatRuleRepository.deleteByInitialNumberConversion(initialNumberConversion);
		initialNumberConversionRepository.deleteBySourceAccount_bindUsername(initialNumberConversion.getSourceAccount().getBindUsername());
		auditLog(LogAction.DELETE, initialNumberConversionVw);
	}

	private List<Long> getRulesList(InitialNumberConversion initialNumberConversion) {
		List<Long> rulesListId = new ArrayList<>();
			initialNumberConversion.getInitialFormatRules().stream().forEach(c -> rulesListId.add(c.getFormatRule().getId()));

		return rulesListId;
	}

	private void auditLog(LogAction action, InitialNumberConversionVw initialNumberConversionVw) {
		auditService.register(getLoggedUser(), INITIAL_NUMBER_CONVERSION, action,
				initialNumberConversionVw.getSourceAccount(), initialNumberConversionVw);
	}

	private void auditUpdateLog(InitialNumberConversionVw before, InitialNumberConversionVw after) {
		auditService.registerUpdate(getLoggedUser(), INITIAL_NUMBER_CONVERSION,
				before.getSourceAccount(), before, after);
	}
}
