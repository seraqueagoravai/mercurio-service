package br.com.algartelecom.mercurio.service.web.dto;

public class GraphRequestDto {

	public String type;
	public String errorCode;
	public String initialDate;
	public String finalDate;
	public String metric;
	public String number;
	public String customNumber;
	public String errorType;

	public GraphRequestDto() {

	}

	public GraphRequestDto(String type, String errorCode, String initialDate, String finalDate, String metric,
			String number, String errorType) {
		super();
		this.type = type;
		this.errorCode = errorCode;
		this.initialDate = initialDate;
		this.finalDate = finalDate;
		this.metric = metric;
		this.number = number;
		this.errorType = errorType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	public String getMetric() {
		return metric;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCustomNumber() {
		return customNumber;
	}

	public void setCustomNumber(String customNumber) {
		this.customNumber = customNumber;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

}
