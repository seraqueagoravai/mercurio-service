package br.com.algartelecom.mercurio.service.web.dto;

public class CdrRequestDto {

	public String numberA;
	public String numberB;
	public String initialDate;
	public String endDate;
	public boolean queued;
	public boolean delivered;
	public boolean failed;
	public int limit;

	public String getNumberA() {
		return numberA;
	}

	public void setNumberA(String numberA) {
		this.numberA = numberA;
	}

	public String getNumberB() {
		return numberB;
	}

	public void setNumberB(String numberB) {
		this.numberB = numberB;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public boolean isQueued() {
		return queued;
	}

	public void setQueued(boolean queued) {
		this.queued = queued;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CdrRequestDto [numberA=").append(numberA).append(", numberB=").append(numberB)
				.append(", initialDate=").append(initialDate).append(", endDate=").append(endDate).append(", queued=")
				.append(queued).append(", delivered=").append(delivered).append(", failed=").append(failed)
				.append(", limit=").append(limit).append("]");
		return builder.toString();
	}

}
