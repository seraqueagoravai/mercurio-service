package br.com.algartelecom.mercurio.service.web.rest;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URISyntaxException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.Setting;
import br.com.algartelecom.mercurio.service.web.service.SettingsService;

@RestController
@RequestMapping("/settings")
public class SettingsResource {

	@Autowired
	SettingsService settingsService;

	@RequestMapping(value = "/{key}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Setting> get(@PathVariable String key) {
		return Optional.ofNullable(settingsService.findByKey(key))
				.map(result -> new ResponseEntity<Setting>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<Setting> getAll() {
		return settingsService.findAll();
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody Setting setting) throws URISyntaxException {
		return settingsService.createSetting(setting);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody Setting setting) throws URISyntaxException {
		return settingsService.getSetting(setting).map(old -> settingsService.update(old, setting))
				.orElse(createError("Setting don't exists", BAD_REQUEST));
	}

	@RequestMapping(value = "/{key}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public void delete(@PathVariable String key) {
		Setting setting = settingsService.findByKey(key);
		settingsService.delete(setting);
	}

}
