package br.com.algartelecom.mercurio.service.web.dto;

public class SMSQueuedDto {

	private String number;
	private Integer totalQueued;
	private String typeOfNumber;
	private Integer queuedLimit;
	private Integer totalSMS;

	public SMSQueuedDto() {

	}

	public SMSQueuedDto(String number, Integer totalQueued, Integer totalSMS) {
		super();
		this.number = number;
		this.totalQueued = totalQueued;
		this.totalSMS = totalSMS;
	}

	public SMSQueuedDto(String number, Integer totalQueued, String typeOfNumber, Integer queuedLimit,
			Integer totalSMS) {
		super();
		this.number = number;
		this.totalQueued = totalQueued;
		this.typeOfNumber = typeOfNumber;
		this.queuedLimit = queuedLimit;
		this.totalSMS = totalSMS;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getTotalQueued() {
		return totalQueued;
	}

	public void setTotalQueued(Integer totalQueued) {
		this.totalQueued = totalQueued;
	}

	public String getTypeOfNumber() {
		return typeOfNumber;
	}

	public void setTypeOfNumber(String typeOfNumber) {
		this.typeOfNumber = typeOfNumber;
	}

	public Integer getQueuedLimit() {
		return queuedLimit;
	}

	public void setQueuedLimit(Integer queuedLimit) {
		this.queuedLimit = queuedLimit;
	}

	public Integer getTotalSMS() {
		return totalSMS;
	}

	public void setTotalSMS(Integer totalSMS) {
		this.totalSMS = totalSMS;
	}

	@Override
	public String toString() {
		return "SMSQueuedDto [number=" + number + ", totalQueued=" + totalQueued + ", typeOfNumber=" + typeOfNumber
				+ ", queuedLimit=" + queuedLimit + ", totalSMS=" + totalSMS + "]";
	}

}
