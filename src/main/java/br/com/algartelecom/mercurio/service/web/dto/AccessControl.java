package br.com.algartelecom.mercurio.service.web.dto;

import java.util.ArrayList;
import java.util.List;

public class AccessControl {
  
  private String role;
  private List<AccessRule> rules;
  
  public AccessControl(){
  }
  
  public AccessControl(String role) {
    this.role = role;
    this.rules = new ArrayList<AccessRule>();
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public List<AccessRule> getRules() {
    return rules;
  }

  public void setRules(List<AccessRule> rules) {
    this.rules = rules;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((role == null) ? 0 : role.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AccessControl other = (AccessControl) obj;
    if (role == null) {
      if (other.role != null)
        return false;
    } else if (!role.equals(other.role))
      return false;
    return true;
  }
  
}
