package br.com.algartelecom.mercurio.service.web.dto;

public class CdrSearchData {
  
  private String numA;
  private String numB;
  private String initDate;
  private String endDate;
  
  public String getNumA() {
    return numA;
  }
  public void setNumA(String numA) {
    this.numA = numA;
  }
  public String getNumB() {
    return numB;
  }
  public void setNumB(String numB) {
    this.numB = numB;
  }
  public String getInitDate() {
    return initDate;
  }
  public void setInitDate(String initDate) {
    this.initDate = initDate;
  }
  public String getEndDate() {
    return endDate;
  }
  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("CdrSearchData [numA=").append(numA).append(", numB=").append(numB)
        .append(", initDate=").append(initDate).append(", endDate=").append(endDate).append("]");
    return builder.toString();
  }

}
