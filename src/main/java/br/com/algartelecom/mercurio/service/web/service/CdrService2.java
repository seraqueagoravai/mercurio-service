package br.com.algartelecom.mercurio.service.web.service;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static br.com.algartelecom.mercurio.vendor.constant.VendorCodes.DROPPED_BY_OPERATOR;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.OK;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.RolesMapping;
import br.com.algartelecom.mercurio.relational.model.vo.ShortMessage;
import br.com.algartelecom.mercurio.relational.model.vo.ShortMessageHistory;
import br.com.algartelecom.mercurio.relational.repositories.RolesMappingRepository;
import br.com.algartelecom.mercurio.relational.repositories.ShortMessageHistoryRepository;
import br.com.algartelecom.mercurio.relational.repositories.ShortMessageRepository;
import br.com.algartelecom.mercurio.service.web.dto.CdrRequestDto;
import br.com.algartelecom.mercurio.service.web.dto.ShortMessageDto;
import br.com.algartelecom.mercurio.service.web.utils.SecurityUtils;
import rx.Observable;
import rx.schedulers.Schedulers;

@Service
public class CdrService2 {
    private static final Logger log = LoggerFactory.getLogger(CdrService.class);

    ExecutorService rxExecutorService = Executors.newFixedThreadPool(8);

    @Autowired
    ShortMessageRepository shortMessageRepository;
    @Autowired
    ShortMessageHistoryRepository shortMessageHistoryRepository;
    // @Autowired
    // CdrNumberARepository cdrNumberARepository;
    // @Autowired
    // CdrNumberBRepository cdrNumberBRepository;
    @Autowired
    RolesMappingRepository rolesMappingRepository;
    @Autowired
    MetricRegistry cdrMetrics;
    // @Autowired
    // ShortMessageTemplate smsTemplate;
    // @Autowired
    // HazelcastInstance hazelInstance;

    ObjectMapper mapper = new ObjectMapper();
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    Timer databaseTimer;
    Timer getCdrTimer;

    private static final String USER_DATA_LIST = "user-data.list";

    @PostConstruct
    public void initTimers() {
        databaseTimer = cdrMetrics.timer(MetricRegistry.name(CdrService.class, "database"));
        getCdrTimer = cdrMetrics.timer(MetricRegistry.name(CdrService.class, "getCdr"));
    }

    public void delete(ShortMessage sms) {
        // TODO: isso não é necessário mais
        // hazelInstance.getQueue(createSmaKey(sms.getSourceAddr())).remove(sms.getId());
        // hazelInstance.getQueue(createSmbKey(sms.getDestinationAddr())).remove(sms.getId());
        // cdrNumberARepository.save(new CdrNumberA(sms));
        // cdrNumberBRepository.save(new CdrNumberB(sms));
        sms.setVendorCodes(DROPPED_BY_OPERATOR.getCode());
        shortMessageRepository.save(sms);
    }

    public ResponseEntity<ShortMessage> findById(String id) {
        return shortMessageRepository.findById(id).map(this::secureSmsData).orElse(new ResponseEntity<>(NOT_FOUND));
    }

    public ResponseEntity<ShortMessage> secureSmsData(ShortMessage sms) {
        if (!hasAccessToUserData()) {
            sms.setUserData(null);
        }
        return new ResponseEntity<ShortMessage>(sms, OK);
    }

    public ShortMessageHistory saveOffline(ShortMessageHistory shortMessage) {
        return shortMessageHistoryRepository.save(shortMessage);
    }

    public ShortMessage saveOnline(ShortMessage shortMessage) {
        return shortMessageRepository.save(shortMessage);
    }

    public List<ShortMessageDto> getCdr(CdrRequestDto r) {
        try {
            List<ShortMessage> ids = getShortMessages(r);
            List<ShortMessageDto> shortMessageDtos = new ArrayList<>();

            for (ShortMessage id : ids) {
                ShortMessageDto shortMessageDto = new ShortMessageDto(id, true);
                System.out.println(shortMessageDto);
                shortMessageDtos.add(shortMessageDto);
            }
            return shortMessageDtos;
        } catch (Exception e) {
            log.error("Error in rx stream ->{}", e);
        }
        return null;
    }

    public void getCdr(HttpServletResponse response, CdrRequestDto r) {
        try {
            DelegatingSecurityContextExecutor executor
            = new DelegatingSecurityContextExecutor(rxExecutorService, SecurityContextHolder.getContext());
            final CountDownLatch latch = new CountDownLatch(1);
            JsonFactory jfactory = new JsonFactory();
            JsonGenerator generator = jfactory.createGenerator(response.getWriter());

            Timer.Context cdrContext = getCdrTimer.time();
            Timer.Context databaseContext = databaseTimer.time();

            generator.writeStartArray();
            Observable.from(getShortMessages(r))
                    .subscribeOn(Schedulers.from(executor))
                    .observeOn(Schedulers.from(executor))
                    .filter(sms -> filterByAdvanced(r, sms))
                    .filter(sms -> filterByBothNumbers(r, sms))
                    .map(rs -> new ShortMessageDto(rs, true))
                    .subscribe(success -> writeToStream(success, generator),
                            error -> log.error("Error parsing result -> {}", error),
                            () -> endCdrStream(cdrContext, databaseContext, generator, latch));

            latch.await();
            response.flushBuffer();
            generator.close();
        } catch (Exception e) {
            log.error("Error in rx stream ->{}", e);
        }
    }

    public boolean filterByAdvanced(CdrRequestDto r, ShortMessage sms) {
        String status = sms.getDeliveryStatus();
        if (status.equals("QUEUED") || status.equals("SENDING") || status.equals("SCHEDULED")) {
            return r.queued;
        } else if (status.equals("DELIVERED")) {
            return r.delivered;
        } else {
            return r.failed;
        }
    }

    public boolean filterByBothNumbers(CdrRequestDto r, ShortMessage sms) {
        if (isPresent(r.numberA) && isPresent(r.numberB)) {
            return r.numberA.equals(sms.getSourceAddr()) && r.numberB.equals(sms.getDestinationAddr());
        } else {
            return true;
        }
    }

    public void writeToStream(ShortMessageDto success, JsonGenerator generator) {
        try {
            mapper.writeValue(generator, success);
        } catch (Exception e) {
            log.error("Error parssing cdr Stream -> {} ", e);
        }
    }

    public void endCdrStream(Timer.Context cdrContext, Timer.Context databaseContext, JsonGenerator generator,
            CountDownLatch latch) {
        try {
            generator.writeEndArray();
            stopTimers(cdrContext, databaseContext);
        } catch (Exception e) {
            log.error("Error ending cdr Stream -> {} ", e);
        } finally {
            latch.countDown();
        }
    }

    private List<ShortMessage> getShortMessages(CdrRequestDto r) throws ParseException {
        List<ShortMessage> shortMessages = new ArrayList<ShortMessage>();
        if (r.delivered || r.failed)
            shortMessages.addAll(getDelivered(r, parseDate(r.initialDate), parseDate(r.endDate)));
        if (r.queued)
            shortMessages.addAll(getQueued(r, parseDate(r.initialDate), parseDate(r.endDate)));
            
        return shortMessages;
    }

    private List<ShortMessage> getDelivered(CdrRequestDto r, Date initDate, Date endDate) {
        if (isPresent(r.numberA) && isPresent(r.numberB)) {
            return getByBothNumbers(r.numberA, r.numberB, initDate, endDate);
        } else if (isPresent(r.numberB)) {
            return getByBNumber(r.numberB, initDate, endDate).stream().filter(Objects::nonNull).map(shortMessageHistory -> new ShortMessage(shortMessageHistory)).collect(toList());
        } else {
            return getByANumber(r.numberA, initDate, endDate).stream().filter(Objects::nonNull).map(shortMessageHistory -> new ShortMessage(shortMessageHistory)).collect(toList());
        }
    }

    private List<ShortMessage> getByBothNumbers(String numberA, String numberB, Date initDate, Date endDate) {
        return getByANumber(numberA, initDate, endDate).stream().filter(cdr -> hasOfflineNumberB(numberB, cdr))
                .filter(Objects::nonNull).map(shortMessageHistory -> new ShortMessage(shortMessageHistory)).collect(toList());
    }

    private List<ShortMessageHistory> getByANumber(String number, Date initDate, Date endDate) {
        if (initDate == null && endDate == null) {
            return shortMessageHistoryRepository.findBySourceAddr(number);
        } else if (initDate == null) {
            return shortMessageHistoryRepository.findBySourceAddrAndFinalSubmissionDate(number, endDate);
        } else if (endDate == null) {
            return shortMessageHistoryRepository.findBySourceNumberAndInitialSubmissionDate(number, initDate);
        } else {
            return shortMessageHistoryRepository.findBySourceNumberBetweenSubmissionDates(number, initDate, endDate);
        }
    }

    private List<ShortMessageHistory> getByBNumber(String number, Date initDate, Date endDate) {
        if (initDate == null && endDate == null) {
            return shortMessageHistoryRepository.findByDestinationAddr(number);
        } else if (initDate == null) {
            return shortMessageHistoryRepository.findByDestinationNumberAndFinalSubmissionDate(number, endDate);
        } else if (endDate == null) {
            return shortMessageHistoryRepository.findByDestinationNumberAndInitialSubmissionDate(number, initDate);
        } else {
            return shortMessageHistoryRepository.findByDestinationNumberBetweenSubmissionDates(number, initDate,
                    endDate);
        }
    }

    //get queued messages
    private List<ShortMessage> getQueued(CdrRequestDto r, Date initDate, Date endDate) {
        if (isPresent(r.numberA) && isPresent(r.numberB)) {
            return getQueuedByBothNumbers(r.numberA, r.numberB, initDate, endDate);
        } else if (isPresent(r.numberB)) {
            return getQueuedByBNumber(r.numberB, initDate, endDate).stream().filter(Objects::nonNull).collect(toList());
        } else {
            return getQueuedByANumber(r.numberA, initDate, endDate).stream().filter(Objects::nonNull).collect(toList());
        }
    }

    private List<ShortMessage> getQueuedByBothNumbers(String numberA, String numberB, Date initDate, Date endDate) {
        return getQueuedByANumber(numberA, initDate, endDate).stream().filter(cdr -> hasNumberB(numberB, cdr))
                .filter(Objects::nonNull).collect(toList());
    }

    private List<ShortMessage> getQueuedByANumber(String number, Date initDate, Date endDate) {
        if (initDate == null && endDate == null) {
            return shortMessageRepository.findBySourceAddr(number);
        } else if (initDate == null) {
            return shortMessageRepository.findBySourceAddrAndFinalSubmissionDate(number, endDate);
        } else if (endDate == null) {
            return shortMessageRepository.findBySourceNumberAndInitialSubmissionDate(number, initDate);
        } else {
            return shortMessageRepository.findBySourceNumberBetweenSubmissionDates(number, initDate, endDate);
        }
    }

    private List<ShortMessage> getQueuedByBNumber(String number, Date initDate, Date endDate) {
        if (initDate == null && endDate == null) {
            return shortMessageRepository.findByDestinationAddr(number);
        } else if (initDate == null) {
            return shortMessageRepository.findByDestinationNumberAndFinalSubmissionDate(number, endDate);
        } else if (endDate == null) {
            return shortMessageRepository.findByDestinationNumberAndInitialSubmissionDate(number, initDate);
        } else {
            return shortMessageRepository.findByDestinationNumberBetweenSubmissionDates(number, initDate, endDate);
        }
    }

    public boolean hasAccessToUserData() {
        if (!SecurityUtils.isAdmin()) {
            List<RolesMapping> mapping = rolesMappingRepository.findByAccessRole_roleResource(USER_DATA_LIST);
            if (mapping != null && !mapping.isEmpty()) {
                List<String> roles = mapping.stream().map(c -> c.getRoles().getRoles()).collect(Collectors.toList());
                if (roles != null && !roles.isEmpty()) {
                    for (String role : roles) {
                        if (SecurityUtils.hasAuthority(role)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } else {
            return true;
        }
    }

    private boolean hasNumberB(String numberB, ShortMessage cdr) {
        return numberB.equals(cdr.getDestinationAddr());
    }

    private boolean hasOfflineNumberB(String numberB, ShortMessageHistory cdr) {
        return numberB.equals(cdr.getDestinationAddr());
    }

    private boolean isPresent(String s) {
        return s != null && !s.isEmpty();
    }

    private Date parseDate(String date) throws ParseException {
        if (date != null && !date.isEmpty()) {
            return dateFormatter.parse(date);
        } else {
            return null;
        }
    }

    private void stopTimers(Timer.Context... contexts) {
        for (Timer.Context context : contexts) {
            long time = context.stop();
            log.debug("[***timer***] Logging timer -> {}", time);
        }
    }
}
