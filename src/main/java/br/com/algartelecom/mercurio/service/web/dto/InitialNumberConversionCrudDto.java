package br.com.algartelecom.mercurio.service.web.dto;

import java.util.List;

import br.com.algartelecom.mercurio.relational.model.vo.Bind;
import br.com.algartelecom.mercurio.relational.model.vo.FormatRule;

public class InitialNumberConversionCrudDto {

    private Bind sourceAccount;
    private List<FormatRule> rules;

    public Bind getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Bind sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public List<FormatRule> getRules() {
        return rules;
    }

    public void setRules(List<FormatRule> rules) {
        this.rules = rules;
    }

    @Override
    public String toString() {
        return "InitialNumberConversionCrudDto{" +
                "sourceAccount=" + sourceAccount +
                ", rules=" + rules +
                '}';
    }
}
