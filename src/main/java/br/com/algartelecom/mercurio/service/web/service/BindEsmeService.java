package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.BIND_ESME;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.BindEsme;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.repositories.BindEsmeBindRepository;
import br.com.algartelecom.mercurio.relational.repositories.BindEsmeRepository;
import br.com.algartelecom.mercurio.relational.repositories.BindRepository;

@Service
public class BindEsmeService {

	@Autowired
	BindEsmeRepository bindEsmeRepository;
	
	@Autowired
	BindRepository bindRepository;

	@Autowired
	BindEsmeBindRepository bindEsmeBindRepository;
	
	@Autowired
	AuditService auditService;

	public ResponseEntity<String> delete(Long id) {
		Optional<BindEsme> bindEsme = getBindEsmeId(id);
		
		if(!bindEsme.isPresent())
			return createError("Bind Esme don't exists", BAD_REQUEST);

		// if(bindEsme.get().getBind() != null)
		// 	return createError("Bind Esme registered for Bind", BAD_REQUEST);
		if(!bindEsmeBindRepository.findByBindEsme_id(id).isEmpty()){
			return createError("BindEsme registered for Bind", BAD_REQUEST);
		}

		bindEsmeRepository.deleteById(bindEsme.get().getId());
		auditLog(LogAction.DELETE, bindEsme.get());
		return new ResponseEntity<>(OK);
		// updateListIds(bindEsme.getId());
	}

	public ResponseEntity<String> update(BindEsme bindEsme) {
		Optional<BindEsme> old = getBindEsmeId(bindEsme.getId());
		if(!old.isPresent())
			createError("Bind Esme doensn't exists", BAD_REQUEST);

		BindEsme before = new BindEsme(old.get());
		bindEsme.setPassword(old.get().getPassword());
		bindEsme.setUpdateDate(new Date());
		bindEsme.setUpdateUser(getLoggedUser());
		bindEsmeRepository.save(bindEsme);
		auditUpdateLog(before, bindEsme);
		return new ResponseEntity<String>(OK);
	}

	public ResponseEntity<String> createBindEsme(BindEsme bindEsme) {
		bindEsme.setCreationUser(getLoggedUser());
		bindEsme.setCreationDate(new Date());
		bindEsmeRepository.save(bindEsme);
		auditLog(LogAction.SAVE, bindEsme);
		return new ResponseEntity<String>(OK);
	}

	public Optional<BindEsme> getBindEsmeId(Long id) {
		return bindEsmeRepository.findById(id);
	}

	public Optional<BindEsme> getBindEsme(BindEsme bindEsme) {
		return bindEsmeRepository.findById(bindEsme.getId());
	}

	public List<BindEsme> getAllBindEsme(){
		return bindEsmeRepository.findAll();
	}
	
	private void auditLog(LogAction action, BindEsme bindEsme) {
		bindEsme.setPassword("**********");
		auditService.register(getLoggedUser(), BIND_ESME, action, bindEsme.getUserEsme(), bindEsme);
	}

	private void auditUpdateLog(BindEsme before, BindEsme after) {
		before.setPassword("***********");
		after.setPassword("***********");
		auditService.registerUpdate(getLoggedUser(), BIND_ESME, before.getUserEsme(), before, after);
	}

	// private void updateListIds(Long id) {
	// 	List<Bind> result = bindRepository.findAll().stream().filter(bind -> bind.getBindEsmes() != null)
	// 			.map(bind -> removeEsmeFromBind(bind, id)).collect(Collectors.toList());
	// 	//TODO: comentar sobre isso aqui
    //     bindRepository.save(result.get(0));
	// }

	// private Bind removeEsmeFromBind(Bind bind, Long esmeId) {
	// 	bind.setBindEsmes(bind.getBindEsmes().stream().filter(esme -> !esme.equals(esmeId)).collect(Collectors.toList()));
	// 	return bind;
	// }
}
