package br.com.algartelecom.mercurio.service.web.dto;

import static br.com.algartelecom.mercurio.service.web.utils.DateUtils.sijuFormatter;

import br.com.algartelecom.mercurio.relational.model.vo.SijuNumber;

public class SijuResponseDto {

  private String srcTerminal;
  private String dstTerminal;
  private String submissionDate;
  private String content;
  private Integer messageParts;
  private Integer partNumber;
  private Integer multipartId;

  public SijuResponseDto(SijuNumber sijuNumber) {
    this.srcTerminal = sijuNumber.getNumberA();
    this.dstTerminal = sijuNumber.getNumberB();
    this.submissionDate = sijuNumber.getSubmissionDate() != null
        ? sijuFormatter.format(sijuNumber.getSubmissionDate()) : "";
    this.content = sijuNumber.getContent();
    this.messageParts = sijuNumber.getMessageParts();
    this.partNumber = sijuNumber.getPartNumber();
    this.multipartId = sijuNumber.getMultipartId();
  }

  public String getSrcTerminal() {
    return srcTerminal;
  }

  public void setSrcTerminal(String srcTerminal) {
    this.srcTerminal = srcTerminal;
  }

  public String getDstTerminal() {
    return dstTerminal;
  }

  public void setDstTerminal(String dstTerminal) {
    this.dstTerminal = dstTerminal;
  }

  public String getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(String submissionDate) {
    this.submissionDate = submissionDate;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Integer getMessageParts() {
    return messageParts;
  }

  public void setMessageParts(Integer messageParts) {
    this.messageParts = messageParts;
  }

  public Integer getPartNumber() {
    return partNumber;
  }

  public void setPartNumber(Integer partNumber) {
    this.partNumber = partNumber;
  }

  public Integer getMultipartId() {
    return multipartId;
  }

  public void setMultipartId(Integer multipartId) {
    this.multipartId = multipartId;
  }

}
