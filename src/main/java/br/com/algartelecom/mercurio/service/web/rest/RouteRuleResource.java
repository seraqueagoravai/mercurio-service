package br.com.algartelecom.mercurio.service.web.rest;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.RouteRule;
import br.com.algartelecom.mercurio.service.web.service.RouteRuleService;

@RestController
@RequestMapping("/route-rule")
public class RouteRuleResource {

	@Autowired
	RouteRuleService routeRuleService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<RouteRule> getAll() {
		return routeRuleService.getAll();
	}

	@RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<RouteRule> get(@PathVariable Long id) {
		return routeRuleService.getRouteRule(id)
				.map(result -> new ResponseEntity<RouteRule>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody RouteRule route) throws URISyntaxException {
		return routeRuleService.createRoute(route);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody RouteRule routeRule) throws URISyntaxException {
		return routeRuleService.getRouteRule(routeRule).map(old -> routeRuleService.update(old, routeRule))
				.orElse(createError("RouteRule don't exists", BAD_REQUEST));
	}

}
