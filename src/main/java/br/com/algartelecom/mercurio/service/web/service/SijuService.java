package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.service.web.utils.DateUtils.sijuFormatter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.SijuNumber;
import br.com.algartelecom.mercurio.relational.repositories.SijuNumberRepository;
import br.com.algartelecom.mercurio.service.web.dto.SijuResponseDto;

@Service
public class SijuService {

	@Autowired
	SijuNumberRepository numberRepository;

	public List<SijuResponseDto> findByNumberAndInitialSubmissionDate(String number, String initialDate,
			String finalDate, Integer limit) throws Exception {

		Date firstDate = convertDate(initialDate, "initial");
		Date secondDate = convertDate(finalDate, "final");

		return numberRepository.findByNumberAndInitialSubmissionDate(number, firstDate, secondDate, limit).stream()
				.map(SijuResponseDto::new).collect(Collectors.toList());
	}

	private Date convertDate(String date, String wich) throws Exception {
		try {
			return sijuFormatter.parse(date);
		} catch (ParseException e) {
			throw new Exception("Format of " + wich + "Date is wrong, must be dd-MM-yyy hh:mm:ss");
		}
	}

	public SijuNumber save(SijuNumber sijuNumber) {
		return numberRepository.save(sijuNumber);
	}

	public List<SijuNumber> getAll() {
		return numberRepository.findAll();
	}
}
