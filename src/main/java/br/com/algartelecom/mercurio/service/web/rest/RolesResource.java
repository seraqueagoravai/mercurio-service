package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.Roles;
import br.com.algartelecom.mercurio.service.web.service.RolesService;

@RestController
@RequestMapping("/roles")
public class RolesResource {

  @Autowired
  RolesService rolesService;

  @RequestMapping(value = "/{platform}", method = GET, produces = APPLICATION_JSON_VALUE)
  public List<Roles> get(@PathVariable String platform) {
    return rolesService.findByPlatform(platform);
  }

  @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
  public List<Roles> getAll() {
    return rolesService.getAll();
  }

  @RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<String> create(@RequestBody Roles roles) throws URISyntaxException {
    return rolesService.createRoles(roles);
  }

  @RequestMapping(value = "/{platform}/{role}", method = DELETE, produces = APPLICATION_JSON_VALUE)
  public void delete(@PathVariable String platform, @PathVariable String role) {
    rolesService.getRoleByPlatformAndRole(platform, role).ifPresent(rolesService::delete);
  }

}
