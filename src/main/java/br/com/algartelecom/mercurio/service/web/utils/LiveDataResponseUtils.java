package br.com.algartelecom.mercurio.service.web.utils;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import br.com.algartelecom.mercurio.relational.model.vo.ClusterMember;
import br.com.algartelecom.mercurio.relational.repositories.ClusterMemberRepository;
import br.com.algartelecom.mercurio.service.web.dto.LastSecondDto;
import br.com.algartelecom.mercurio.service.web.dto.LiveDataMetric;
import br.com.algartelecom.mercurio.service.web.dto.LiveDataResponseDto;
import br.com.algartelecom.mercurio.service.web.rest.LiveDataResponseResource;

public class LiveDataResponseUtils {
	
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ClusterMemberRepository clusterMemberRepository;
	@Value("${message.error.mo}")
	String messageErrorMo;
	@Value("${message.error.mt}")
	String messageErrorMt;
	@Value("${mercurio.map.mt.last.second.url}")
	String mapMtLastSecond;
	@Value("${mercurio.map.mo.last.second.url}")
	String mapMoLastSecond;
	@Value("${mercurio.smpp.mt.last.second.url}")
	String smppMtLastSecond;
	@Value("${mercurio.smpp.mo.last.second.url}")
	String smppMoLastSecond;
	private Instant lastRequestInstant;
	LiveDataResponseDto cacheDto = null;

	private static final Logger logger = LoggerFactory.getLogger(LiveDataResponseResource.class);

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public LiveDataResponseDto get() {
		if (cacheExpired()) {
			LiveDataResponseDto result = getLiveData();
			cacheDto = result;
			lastRequestInstant = Instant.now();
		}
		return cacheDto;
	}

	private boolean cacheExpired() {
		if (cacheDto == null || lastRequestInstant.isBefore(Instant.now().minusSeconds(5)))
			return true;
		return false;
	}

	@RequestMapping(value = "/series/{type}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<String> getSeriesSmpp(@PathVariable String type) {
		return getAllSeries(type);
	}

	public LiveDataResponseDto getLiveData() {
		LiveDataResponseDto mapMo = getTotalMap(mapMoLastSecond, messageErrorMo);
		LiveDataResponseDto mapMt = getTotalMap(mapMtLastSecond, messageErrorMt);
		LiveDataResponseDto smppMo = getTotalSmpp(smppMoLastSecond, messageErrorMo);
		LiveDataResponseDto smppMt = getTotalSmpp(smppMtLastSecond, messageErrorMt);
		return getResult(mapMo, mapMt, smppMo, smppMt);
	}

	private LiveDataResponseDto getResult(LiveDataResponseDto mapMo, LiveDataResponseDto mapMt,
			LiveDataResponseDto smppMo, LiveDataResponseDto smppMt) {
		LiveDataResponseDto result = new LiveDataResponseDto();
		result.setMapTotalMo(mapMo.getMapTotalMo());
		result.setMapTotalMt(mapMt.getMapTotalMt());
		result.setSmppTotalMo(smppMo.getSmppTotalMo());
		result.setSmppTotalMt(smppMt.getSmppTotalMt());
		result.setMapValueMo(mapMo.getMapValueMo());
		result.setMapValueMt(mapMt.getMapValueMt());
		result.setSmppValueMo(smppMo.getSmppValueMo());
		result.setSmppValueMt(smppMt.getSmppValueMt());
		return result;
	}

	private LiveDataResponseDto getTotalMap(String param, String message) {
		Integer sum = 0;
		LiveDataResponseDto response = new LiveDataResponseDto();
		List<LiveDataMetric> listMetrics = new ArrayList<>();
		List<ClusterMember> list = getAllClusterMembers();
		for (ClusterMember cluster : list) {
			if (cluster.getType().equals("map")) {
				String url = replaceUrl(param, cluster.getIp(), cluster.getPort());
				try {
					LastSecondDto result = restTemplate.getForObject(url, LastSecondDto.class);
					listMetrics.add(getMetric(cluster, result.getValue()));
					sum += result.getValue();
				} catch (Exception e) {
					listMetrics.add(getMetric(cluster, 0));
					logger.error(message + url, e.getMessage());
				}
			}
		}
		listMetrics.add(setTotalMetric(sum));
		if (param.contains("mo")) {
			response.setMapTotalMo(sum);
			response.setMapValueMo(listMetrics);
		} else if (param.contains("mt")) {
			response.setMapTotalMt(sum);
			response.setMapValueMt(listMetrics);
		}
		return response;
	}

	private LiveDataResponseDto getTotalSmpp(String param, String message) {
		Integer sum = 0;
		LiveDataResponseDto response = new LiveDataResponseDto();
		List<LiveDataMetric> listMetrics = new ArrayList<>();
		List<ClusterMember> list = getAllClusterMembers();
		for (ClusterMember cluster : list) {
			if (cluster.getType().equals("smpp")) {
				String url = replaceUrl(param, cluster.getIp(), cluster.getPort());
				try {
					LastSecondDto result = restTemplate.getForObject(url, LastSecondDto.class);
					listMetrics.add(getMetric(cluster, result.getValue()));
					sum += result.getValue();
				} catch (Exception e) {
					listMetrics.add(getMetric(cluster, 0));
					logger.error(message + url, e.getMessage());
				}
			}
		}
		listMetrics.add(setTotalMetric(sum));
		if (param.contains("mo")) {
			response.setSmppTotalMo(sum);
			response.setSmppValueMo(listMetrics);
		} else if (param.contains("mt")) {
			response.setSmppTotalMt(sum);
			response.setSmppValueMt(listMetrics);
		}
		return response;
	}

	public LiveDataMetric getMetric(ClusterMember member, Integer value) {
		LiveDataMetric liveDataMetric = new LiveDataMetric();
		liveDataMetric.setIp(replaceIp(member.getIp().toString()));
		liveDataMetric.setPort(member.getPort());
		liveDataMetric.setName(member.getName());
		liveDataMetric.setValue(value);
		return liveDataMetric;
	}

	private LiveDataMetric setTotalMetric(Integer sum) {
		LiveDataMetric metric = new LiveDataMetric();
		metric.setName("total");
		metric.setValue(sum);
		return metric;
	}

	public List<String> getAllSeries(String type) {
		List<String> result = new ArrayList<>();
		getAllClusterMembers().stream().filter(c -> c.getType().equals(type.toString()))
				.forEach(c -> result.add(c.getName()));
		result.add("total");
		return result;
	}

	public List<ClusterMember> getAllClusterMembers() {
		return clusterMemberRepository.findAll();
	}

	private String replaceIp(String ip) {
		ip = ip.replace("/", "");
		return ip;
	}

	private String replaceUrl(String url, String ip, String port) {
		url = url.replace("ip", ip);
		url = url.replace("port", port);
		return url;
	}

}
