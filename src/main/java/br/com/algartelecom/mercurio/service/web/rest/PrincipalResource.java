package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.service.web.dto.AccessResource;
import br.com.algartelecom.mercurio.service.web.service.PrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/principal")
public class PrincipalResource {

  @Autowired
  PrincipalService principalService;

  @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
  public List<AccessResource> getAll() {
    return principalService.getallAccessResource();
  }

  @RequestMapping(value = "/{resource:.+}", method = GET, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> hasAccess(@PathVariable String resource) {
    return principalService.hasAccessResource(resource);
  }

  @RequestMapping(value = "/auth/valid", method = GET, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> hasValidAccess() {
    return principalService.hasValidAccessPrincipal();
  }



}
