package br.com.algartelecom.mercurio.service.web.utils;

import br.com.algartelecom.mercurio.map.model.constant.MapErrorCodes;
import br.com.algartelecom.mercurio.smpp.core.constant.SmppErrorCodes;
import br.com.algartelecom.mercurio.vendor.constant.VendorCodes;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ConstantsFilters {

  public static String[] getListMapErrors() {
    return Arrays.stream(MapErrorCodes.values()).map(MapErrorCodes::getCode).sorted().map(code -> {
      return code + " - " + MapErrorCodes.getInstance(code).name();
    }).collect(Collectors.toList()).toArray(new String[] {});
  }

  public static String[] getListSmppErrors() {
    return Arrays.stream(SmppErrorCodes.values()).map(SmppErrorCodes::getCode).sorted()
        .map(code -> {
          return code + " - " + SmppErrorCodes.getInstance(code).name();
        }).collect(Collectors.toList()).toArray(new String[] {});
  }
  
  public static String[] getVendorCodes() {
    return Arrays.stream(VendorCodes.values()).map(VendorCodes::getCode).sorted()
        .map(code -> {
          return code + " - " + VendorCodes.getInstance(code).name();
        }).collect(Collectors.toList()).toArray(new String[] {});
  }

}
