package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.model.vo.InitialNumberConversion;
import br.com.algartelecom.mercurio.relational.model.vo.vw.InitialNumberConversionVw;
import br.com.algartelecom.mercurio.service.web.dto.InitialNumberConversionCrudDto;
import br.com.algartelecom.mercurio.service.web.service.FormatRuleService;
import br.com.algartelecom.mercurio.service.web.service.InitialNumberConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/initial-number-conversion")
public class InitialNumberConversionResource {
	
	@Autowired
	InitialNumberConversionService initialNumberConversionService;
	
	@Autowired
	FormatRuleService formatRuleService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	  public Iterable<InitialNumberConversion> getAll() {
	    return initialNumberConversionService.getAll();
	  }

	  @RequestMapping(value = "/{sourceAccount}", method = GET, produces = APPLICATION_JSON_VALUE)
	  public ResponseEntity<InitialNumberConversion> get(@PathVariable String sourceAccount) {
	    return initialNumberConversionService.getInitialNumberConversion(sourceAccount)
	        .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
	        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	  }

	  @RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	  public ResponseEntity<String> create(@RequestBody InitialNumberConversionCrudDto initialNumberConversionCrudDto) throws URISyntaxException {
	    return initialNumberConversionService.createInitialNumberConversion(initialNumberConversionCrudDto);
	  }

	  @RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	  public ResponseEntity<String> update(@RequestBody InitialNumberConversionCrudDto initialNumberConversionCrudDto) throws URISyntaxException {
	    return initialNumberConversionService.update(initialNumberConversionCrudDto);
	  }

	  @RequestMapping(value = "/{sourceAccount}", method = DELETE,
	      produces = APPLICATION_JSON_VALUE)
	  public void delete(@PathVariable String sourceAccount) {
		  initialNumberConversionService.getInitialNumberConversion(sourceAccount).ifPresent(initialNumberConversionService::delete);
	  }
	  
	  @RequestMapping(value = "/description/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	  public String getFormatRuleDescription(@PathVariable Long id) {
	    return formatRuleService.getFormatRule(id).get().getDescription();
	  }
	
}
