package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.SijuNumber;
import br.com.algartelecom.mercurio.service.web.dto.SijuResponseDto;
import br.com.algartelecom.mercurio.service.web.service.SijuService;

@RestController
@RequestMapping("/siju")
public class SijuResource {

  @Value("${mercurio.siju.query.limit:1000}")
  int sijuQueryLimit;

  @Autowired
  SijuService sijuService;

  @RequestMapping(value = "/search", method = GET)
  public List<SijuResponseDto> search(@RequestParam String terminal, @RequestParam String initialDate,
      @RequestParam String finalDate) throws Exception {

    return sijuService.findByNumberAndInitialSubmissionDate(terminal, initialDate, finalDate, sijuQueryLimit);

  }

  @RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
  public SijuNumber save(@RequestBody SijuNumber sijuNumber) {
    return sijuService.save(sijuNumber);
  }

  @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
  public List<SijuNumber> getAll() {
    return sijuService.getAll();
  }
}
