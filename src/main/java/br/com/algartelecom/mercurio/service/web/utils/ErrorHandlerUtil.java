package br.com.algartelecom.mercurio.service.web.utils;

import br.com.algartelecom.mercurio.service.web.dto.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ErrorHandlerUtil {
  
  private static final Logger log = LoggerFactory.getLogger(ErrorHandlerUtil.class);

  public static ResponseEntity<String> createError(String message, HttpStatus  statusCode){
    log.error(message);
    return new ResponseEntity<String>(new ErrorMessage(message,statusCode.toString()).toString(),statusCode);
  }
  
}
