package br.com.algartelecom.mercurio.service.web.dto;

import br.com.algartelecom.mercurio.smpp.core.model.Esme;

public class EsmeDecoratorDto {

  private Esme esme;
  private String server;

  public EsmeDecoratorDto(Esme esme, String server) {
    this.esme = esme;
    this.server = server;
  }

  public Esme getEsme() {
    return esme;
  }

  public void setEsme(Esme esme) {
    this.esme = esme;
  }

  public String getServer() {
    return server;
  }

  public void setServer(String server) {
    this.server = server;
  }

}
