package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.model.vo.ClusterMember;
import br.com.algartelecom.mercurio.service.web.service.AuditService;
import br.com.algartelecom.mercurio.service.web.service.ClusterMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.net.UnknownHostException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/cluster-member")
public class ClusterMemberResource {

	@Autowired
	ClusterMemberService clusterMemberService;

	@Autowired
	AuditService auditService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<ClusterMember> getAll() {
		return clusterMemberService.getAllClusterMembers();
	}

	@RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<ClusterMember> get(@PathVariable Long id)
			throws UnknownHostException {
		return clusterMemberService.getClusterMember(id)
				.map(result -> new ResponseEntity<ClusterMember>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody ClusterMember clusterMember) throws URISyntaxException {
		return clusterMemberService.createClusterMember(clusterMember);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody ClusterMember clusterMember) throws URISyntaxException {
		return clusterMemberService.update(clusterMember);
	}

	@RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public void delete(@PathVariable Long id) throws UnknownHostException {
		clusterMemberService.getClusterMember(id).ifPresent(clusterMemberService::delete);
	}

}
