package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.SETTINGS;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.Setting;
import br.com.algartelecom.mercurio.relational.repositories.SettingsRepository;

@Service
public class SettingsService {

	@Autowired
	SettingsRepository settingsRepository;
	
	@Autowired
	AuditService auditService;
	
	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> createSetting(Setting setting) {
		if(settingsRepository.findBySettingKey(setting.getSettingKey()) != null)
			return createError("Setting already exists", BAD_REQUEST);
	    setting.setCreationDate(new Date());
	    setting.setCreationUser(getLoggedUser());
	    settingsRepository.save(setting);
	    auditLog(LogAction.SAVE, setting);
	    return new ResponseEntity<String>(OK);
	}
	
	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> update(Setting old, Setting after) {
		Setting before = new Setting(old);
		after.setUpdateDate(new Date());
		after.setUpdateUser(getLoggedUser());
		settingsRepository.save(after);
		auditUpdateLog(before, after);
	    return new ResponseEntity<String>(OK);
	}
	
	@Transactional(readOnly = true)
	public Optional<Setting> getSetting(Setting setting) {
	    return Optional.ofNullable(settingsRepository.findBySettingKey(setting.getSettingKey()));
	}
	
	@Transactional(readOnly = true)
	public Setting findByKey(String settingKey) {
		return settingsRepository.findBySettingKey(settingKey);
	}
	
	@Transactional(readOnly = true)
	public Iterable<Setting> findAll(){
		return settingsRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class )
	public void delete(Setting setting) {
	    settingsRepository.deleteById(setting.getId());
	    auditLog(LogAction.DELETE, setting);
	}
	
	private void auditLog(LogAction action, Setting setting) {
	    auditService.register(getLoggedUser(), SETTINGS, action, setting.getSettingKey(), setting);
	}
	
	private void auditUpdateLog(Setting before, Setting after) {
	    auditService.registerUpdate(getLoggedUser(), SETTINGS, before.getSettingKey(), before, after);
	}
}
