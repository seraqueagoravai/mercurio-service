package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.model.vo.Bind;
import br.com.algartelecom.mercurio.relational.model.vo.vw.BindVw;
import br.com.algartelecom.mercurio.service.web.dto.BindCommandRespDto;
import br.com.algartelecom.mercurio.service.web.dto.BindCrudDto;
import br.com.algartelecom.mercurio.service.web.dto.BindEsmeDto;
import br.com.algartelecom.mercurio.service.web.dto.CharsetDto;
import br.com.algartelecom.mercurio.service.web.dto.ServerBindDecoratorDto;
import br.com.algartelecom.mercurio.service.web.service.BindSmppService;
import br.com.algartelecom.mercurio.service.web.utils.SmppCommander;
import br.com.algartelecom.mercurio.smpp.core.model.CommandResp;
import com.cloudhopper.commons.charset.CharsetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/bind-smpp")
public class BindSmppResource {

	@Autowired
	BindSmppService bindService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<BindVw> getAll() {
		return bindService.getListBindVw();
	}

	@RequestMapping(value = "/get-accounts", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<Bind> getAccounts() {
		return bindService.getListBind();
	}

	@RequestMapping(value = "/{username}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Bind> get(@PathVariable String username) {
		return bindService.getBind(username).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody BindCrudDto bind) throws URISyntaxException {
		return bindService.createBind(bind);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody BindCrudDto bindDto) throws URISyntaxException {
		return bindService.update(bindDto);
	}

	@RequestMapping(value = "/{username}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public void delete(@PathVariable String username) throws Exception {
		bindService.delete(username);
	}

	@RequestMapping(value = "/show-binds", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<ServerBindDecoratorDto> showBinds() throws URISyntaxException {
		return bindService.getSmppMembersStream().map(bindService::createSmppCommander)
				.flatMap(bindService::getAllBindsDecorator).collect(Collectors.toList());
	}

	@RequestMapping(value = "/show-esme-registered", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<BindEsmeDto> showEsmeRegistered() throws URISyntaxException {
		return bindService.getSmppMembersStream().map(bindService::createSmppCommander)
				.flatMap(bindService::getEsmesEntrySet).collect(Collectors.toList());
	}

	@RequestMapping(value = "/start-new-bind/{username}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<CommandResp> startAnBind(@PathVariable String username) throws URISyntaxException {
		return bindService.getSmppMembersStream().map(bindService::createSmppCommander)
				.map(command -> command.startBind(username)).filter(Optional::isPresent).map(Optional::get)
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/add-esme-server/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<CommandResp> addEsmeServer(@PathVariable Long id) throws URISyntaxException {
		return bindService.getSmppMembersStream().map(bindService::createSmppCommander)
				.map(command -> command.addEsme(id)).filter(Optional::isPresent).map(Optional::get)
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/stop-bind/{username}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<CommandResp> stopAnBind(@PathVariable String username) throws URISyntaxException {
		return bindService.getSmppMembersStream().map(bindService::createSmppCommander)
				.map(command -> command.stopBind(username)).filter(Optional::isPresent).map(Optional::get)
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/restart-all-binds", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<BindCommandRespDto> reloadAllBinds() throws URISyntaxException {
		return bindService.getSmppMembersStream().map(bindService::createSmppCommander)
				.map(SmppCommander::sendRestartAllBindsCommandToSmpp).filter(Optional::isPresent).map(Optional::get)
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/charsets", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<CharsetDto> charsets() throws URISyntaxException {
		return CharsetUtil.getCharsetMap().keySet().stream().map(CharsetDto::new).collect(Collectors.toList());
	}

	@RequestMapping(value = "/restart-bind/{username}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<BindCommandRespDto> restartAnBind(@PathVariable String username) throws URISyntaxException {
		return bindService.restartBind(username);
	}

}
