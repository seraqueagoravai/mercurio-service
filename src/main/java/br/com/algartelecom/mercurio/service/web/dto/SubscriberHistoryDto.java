package br.com.algartelecom.mercurio.service.web.dto;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.algartelecom.mercurio.relational.model.vo.SubscriberHistory;

public class SubscriberHistoryDto {

  private String msisdn;
  private Date date;
  private String user;
  private String action;
  private Map content;

  public SubscriberHistoryDto() {}

  public SubscriberHistoryDto(SubscriberHistory subscriber) {
    ObjectMapper mapper = new ObjectMapper();
    this.msisdn = subscriber.getMsisdn();
    this.date = subscriber.getDateTime();
    this.user = subscriber.getUsername();
    this.action = subscriber.getAction();
    try {
      this.content = mapper.readValue(subscriber.getContent(), HashMap.class);
    } catch (IOException e) {
      this.content = new HashMap<>();
    }
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public Map getContent() {
    return content;
  }

  public void setContent(Map content) {
    this.content = content;
  }

}
