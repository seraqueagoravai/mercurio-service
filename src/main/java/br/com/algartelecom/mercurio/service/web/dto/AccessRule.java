package br.com.algartelecom.mercurio.service.web.dto;

public class AccessRule {

  private String resource;
  private boolean allowed;

  public AccessRule() {}

  public AccessRule(String resource, boolean allowed) {
    this.resource = resource;
    this.allowed = allowed;
  }

  public String getResource() {
    return resource;
  }

  public void setResource(String resource) {
    this.resource = resource;
  }

  public boolean isAllowed() {
    return allowed;
  }

  public void setAllowed(boolean allowed) {
    this.allowed = allowed;
  }

}
