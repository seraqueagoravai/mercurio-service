package br.com.algartelecom.mercurio.service.web.dto;

import java.util.ArrayList;
import java.util.List;

public class GraphicLiveData {
  
  private List<String> labels;
  private List<String> series;
  private List<List<Integer>> data;
  
  public GraphicLiveData(){
    this.labels = new ArrayList<String>();
    this.series = new ArrayList<String>();
    this.data = new ArrayList<List<Integer>>();
  }
  
  public List<String> getLabels() {
    return labels;
  }
  public void setLabels(List<String> labels) {
    this.labels = labels;
  }
  public List<String> getSeries() {
    return series;
  }
  public void setSeries(List<String> series) {
    this.series = series;
  }
  public List<List<Integer>> getData() {
    return data;
  }
  public void setData(List<List<Integer>> data) {
    this.data = data;
  }
  
  

}
