package br.com.algartelecom.mercurio.service.web.dto;

public class PatternDto {
  
  private String pattern;
  private String text;
  
  public String getPattern() {
    return pattern;
  }
  public void setPattern(String pattern) {
    this.pattern = pattern;
  }
  public String getText() {
    return text;
  }
  public void setText(String text) {
    this.text = text;
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("PatternDto [pattern=").append(pattern).append(", text=").append(text)
        .append("]");
    return builder.toString();
  }

}
