package br.com.algartelecom.mercurio.service.web.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomValidationExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(CustomValidationExceptionHandler.class);

  @ExceptionHandler(value = MethodArgumentNotValidException.class)
  public ResponseEntity<String> handleMethodArgumentNotValidException(HttpServletRequest req,
      MethodArgumentNotValidException e) throws Exception {
    if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
      throw e;
    }
    LOG.error(e.getMessage());
    return createError(e.getBindingResult().getFieldError().getDefaultMessage(), BAD_REQUEST);
  }

}
