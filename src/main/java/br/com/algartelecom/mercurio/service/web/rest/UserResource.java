package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.User;
import br.com.algartelecom.mercurio.service.web.service.UserService;

@RestController
@RequestMapping("/user")
public class UserResource {

	@Autowired
	UserService userService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<User> getUsers() {
		return userService.getUsers();
	}

}
