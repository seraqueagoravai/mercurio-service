package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.vendor.constant.VendorCodes.DROPPED_BY_OPERATOR;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.OK;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.datastax.oss.driver.api.core.cql.Row;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.RolesMapping;
import br.com.algartelecom.mercurio.relational.model.vo.ShortMessage;
import br.com.algartelecom.mercurio.relational.model.vo.ShortMessageHistory;
import br.com.algartelecom.mercurio.relational.repositories.RolesMappingRepository;
import br.com.algartelecom.mercurio.relational.repositories.ShortMessageHistoryRepository;
import br.com.algartelecom.mercurio.relational.repositories.ShortMessageRepository;
import br.com.algartelecom.mercurio.service.web.dto.CdrRequestDto;
import br.com.algartelecom.mercurio.service.web.dto.ShortMessageDto;
import br.com.algartelecom.mercurio.service.web.utils.SecurityUtils;

@Service
public class CdrService {

    private static final Logger log = LoggerFactory.getLogger(CdrService.class);

    @Autowired
    ShortMessageRepository shortMessageRepository;
    @Autowired
    ShortMessageHistoryRepository shortMessageOfflineRepository;
    // @Autowired
    // CdrNumberARepository cdrNumberARepository;
    // @Autowired
    // CdrNumberBRepository cdrNumberBRepository;
    @Autowired
    RolesMappingRepository rolesMappingRepository;
    @Autowired
    MetricRegistry cdrMetrics;
    // @Autowired
    // ShortMessageTemplate smsTemplate;
    // @Autowired
    // HazelcastInstance hazelInstance;

    ObjectMapper mapper = new ObjectMapper();
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    Timer databaseTimer;
    Timer getCdrTimer;

    private static final String USER_DATA_LIST = "user-data.list";

    @PostConstruct
    public void initTimers() {
        databaseTimer = cdrMetrics.timer(MetricRegistry.name(CdrService.class, "database"));
        getCdrTimer = cdrMetrics.timer(MetricRegistry.name(CdrService.class, "getCdr"));
    }

    public void delete(ShortMessage sms) {
        // TODO: isso não é necessário mais
        // hazelInstance.getQueue(createSmaKey(sms.getSourceAddr())).remove(sms.getId());
        // hazelInstance.getQueue(createSmbKey(sms.getDestinationAddr())).remove(sms.getId());
        // cdrNumberARepository.save(new CdrNumberA(sms));
        // cdrNumberBRepository.save(new CdrNumberB(sms));
        sms.setVendorCodes(DROPPED_BY_OPERATOR.getCode());
        shortMessageRepository.save(sms);
    }

    // public Optional<ShortMessage> findById(Long id) {
    // return shortMessageRepository.findById(id);
    // }

    public Future<ResultSet> findByIdFuture(Long id) {
        // return shortMessageRepository.findByIdFuture(id);
        return null;
    }

    public ShortMessageHistory saveOffline(ShortMessageHistory shortMessage) {
        return shortMessageOfflineRepository.save(shortMessage);
    }

    public ShortMessage saveOnline(ShortMessage shortMessage) {
        return shortMessageRepository.save(shortMessage);
    }

    public ResponseEntity<ShortMessage> secureSmsData(ShortMessage sms) {
        if (!hasAccessToUserData()) {
            sms.setUserData(null);
        }
        return new ResponseEntity<ShortMessage>(sms, OK);
    }

    public void getCdr(CdrRequestDto r, JsonGenerator generator, CountDownLatch latch,
            DelegatingSecurityContextExecutor executor) {
        try {
            Timer.Context cdrContext = getCdrTimer.time();
            // Set<UUID> ids = getIds(r);
            Timer.Context databaseContext = databaseTimer.time();
            generator.writeStartArray();
            List<ShortMessage> shortMessages = new ArrayList<>();

            // Observable.from(smsTemplate.findByIdAsync(r.limit, ids.toArray())).subscribeOn(Schedulers.from(executor))
            //         .flatMap(future -> Observable.from(future, Schedulers.io()))
            //         .flatMap(rs -> Observable.from(rs.all())).observeOn(Schedulers.from(executor))
            //         .filter(sms -> filterByAdvanced(r, sms)).filter(sms -> filterByBothNumbers(r, sms))
            //         .map(rs -> new ShortMessageDto(rs, canSeeUserData))
            //         .subscribe(success -> writeToStream(success, generator),
            //                 error -> log.error("Error parsing cassandra result -> {}", error),
            //                 () -> endCdrStream(cdrContext, databaseContext, generator, latch));
        } catch (Exception e) {
            log.error("Error in rx stream ->{}", e);
        }
    }

    public boolean filterByAdvanced(CdrRequestDto r, Row sms) {
        String status = sms.getString("delivery_status");
        if (status.equals("QUEUED") || status.equals("SENDING") || status.equals("SCHEDULED")) {
            return r.queued;
        } else if (status.equals("DELIVERED")) {
            return r.delivered;
        } else {
            return r.failed;
        }
    }

    public boolean filterByBothNumbers(CdrRequestDto r, Row sms) {
        if (isPresent(r.numberA) && isPresent(r.numberB)) {
            return r.numberA.equals(sms.getString("source_addr"))
                    && r.numberB.equals(sms.getString("destination_addr"));
        } else {
            return true;
        }
    }

    public void writeToStream(ShortMessageDto success, JsonGenerator generator) {
        try {
            mapper.writeValue(generator, success);
        } catch (Exception e) {
            log.error("Error parssing cdr Stream -> {} ", e);
        }
    }

    public void endCdrStream(Timer.Context cdrContext, Timer.Context databaseContext, JsonGenerator generator,
            CountDownLatch latch) {
        try {
            generator.writeEndArray();
            stopTimers(cdrContext, databaseContext);
        } catch (Exception e) {
            log.error("Error ending cdr Stream -> {} ", e);
        } finally {
            latch.countDown();
        }
    }

    private Set<UUID> getIds(CdrRequestDto r) throws ParseException {
        Set<UUID> ids = new HashSet<UUID>();
        if (r.delivered || r.failed) {

            // ids.addAll(getDelivered(r, parseDate(r.initialDate), parseDate(r.endDate)));

        }
        if (r.queued)
            ids = null;
            // ids.addAll(getQueued(r));
        return ids;
    }

    // private List<UUID> getDelivered(CdrRequestDto r, Long initDate, Long endDate) {
    //     System.out.println("PASSEI AQUI");
    //     System.out.println(r);
    //     if (isPresent(r.numberA) && isPresent(r.numberB)) {
    //         return getByBothNumbers(r.numberA, r.numberB, initDate, endDate);
    //     } else if (isPresent(r.numberB)) {
    //         System.out.println("PASSEI AQUI");
    //         return getByBNumber(r.numberB, initDate, endDate).stream().map(ShortMessage::getId).filter(Objects::nonNull)
    //                 .collect(toList());
    //     } else {
    //         return getByANumber(r.numberA, initDate, endDate).stream().map(ShortMessage::getId).filter(Objects::nonNull)
    //                 .collect(toList());
    //     }
    // }

    // private List<UUID> getByBothNumbers(String numberA, String numberB, Long initDate, Long endDate) {
    //     return getByANumber(numberA, initDate, endDate).stream().filter(cdr -> hasNumberB(numberB, cdr))
    //             .map(ShortMessage::getId).filter(Objects::nonNull).collect(toList());
    // }

    // TODO: olhar esse e o método de baixo
    public List<ShortMessage> getByANumber(String number, Long initDate, Long endDate) {
        if (initDate == null && endDate == null) {
            return shortMessageRepository.findBySourceAddr(number);
        } else if (initDate == null) {
            return null;
            // return shortMessageRepository.findBySourceAddrAndFinalSubmissionDate(number,
            // endDate);
        } else if (endDate == null) {
            return null;
            // return
            // shortMessageRepository.findBySourceNumberAndInitialSubmissionDate(number,
            // initDate);
        } else {
            return null;
            // return
            // shortMessageRepository.findBySourceNumberBetweenSubmissionDates(number,
            // initDate, endDate);
        }
    }

    public List<ShortMessage> getByBNumber(String number, Long initDate, Long endDate) {
        return null;
        // if (initDate == null && endDate == null) {
        //     return shortMessageRepository.findByDestinationAddr(number);
        // } else if (initDate == null) {
        //     return null;
        //     return shortMessageRepository.findByDestinationNumberAndFinalSubmissionDate(number, endDate);
        // } else if (endDate == null) {
        //     return null;
        //     return shortMessageRepository.findByDestinationNumberAndInitialSubmissionDate(number, initDate);
        // } else {
        //     return shortMessageRepository.findByDestinationNumberBetweenSubmissionDates(number, initDate, endDate);
        // }
    }

    // TODO: hazelcast
    // private List<Long> getQueued(CdrRequestDto r) {
    // List<Long> result = new ArrayList<>();
    // if (isPresent(r.numberB)) {
    // result.addAll(getRunnignSmbMessages(r.numberB).stream().collect(toList()));
    // }
    // if (isPresent(r.numberA)) {
    // result.addAll(getRunnignSmaMessages(r.numberA).stream().collect(toList()));
    // }
    // return result;
    // }

    // TODO: hazelcast
    // public List<Long> getRunnignSmaMessages(String number) {
    // return hazelInstance.getQueue(createSmaKey(number))
    // .stream()
    // .map(ob -> (Long) ob)
    // .collect(Collectors.toList());
    // }

    // TODO: hazelcast
    // private List<Long> getRunnignSmbMessages(String number) {
    // return hazelInstance.getQueue(createSmbKey(number))
    // .stream()
    // .map(ob -> (Long) ob)
    // .collect(Collectors.toList());
    // }

    public boolean hasAccessToUserData() {
        if (!SecurityUtils.isAdmin()) {
            List<RolesMapping> mapping = rolesMappingRepository.findByAccessRole_roleResource(USER_DATA_LIST);
            if (mapping != null && !mapping.isEmpty()) {
                List<String> roles = mapping.stream().map(c -> c.getRoles().getRoles()).collect(Collectors.toList());
                if (roles != null && !roles.isEmpty()) {
                    for (String role : roles) {
                        if (SecurityUtils.hasAuthority(role)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } else {
            return true;
        }
    }

    private boolean hasNumberB(String numberB, ShortMessage cdr) {
        return numberB.equals(cdr.getDestinationAddr());
    }

    private boolean isPresent(String s) {
        return s != null && !s.isEmpty();
    }

    private Long parseDate(String date) throws ParseException {
        System.out.println((date));
        if (date != null && !date.isEmpty()) {
            System.out.println("Antes de retornar a porra da data");
            System.out.println(dateFormatter.parse(date).getTime());
            return dateFormatter.parse(date).getTime();
        } else {
            return null;
        }
    }

    private void stopTimers(Timer.Context... contexts) {
        for (Timer.Context context : contexts) {
            long time = context.stop();
            log.debug("[***timer***] Logging timer -> {}", time);
        }
    }

    private String createSmaKey(String number) {
        return "A:" + number;
    }

    private String createSmbKey(String number) {
        return "B:" + number;
    }
}
