package br.com.algartelecom.mercurio.service.web.dto;

import java.util.Date;

import br.com.algartelecom.mercurio.relational.model.vo.ShortMessage;

public class ShortMessageDto {

  private String id;
  private String sourceAddr;
  private String destinationAddr;
  private int scAddrType;
  private String scAddr;
  private int moMscAddrType;
  private String moMscAddr;
  private int mtMscAddrType;
  private String mtMscAddr;
  private int submitReportRequest;
  private int dataCodingScheme;
  private Date submissionDate;
  private Date finalDate;
  private Date scheduleDate;
  private Date expireDate;
  private String deliveryStatus;
  private int bStatusReport;
  private int deliveryErrorCode;
  private String lastError;
  private int shortMessageType;
  private int deliverCount;
  private String originalSourceAddr;
  private String originalDestinationAddr;
  private String originAccount;
  private String destinationAccount;
  private boolean prepaid;
  private String fakeImsi;
  private String routeNumber;
  private int submitMultiId;
  private int messageParts;
  private int partNumber;
  private int userDataLength;
  private String userData;
  private int destinationNetworkType;
  private Integer vendorCodes;

  public ShortMessageDto(ShortMessage shortMessage, boolean canSeeuserData) {
    this.id = shortMessage.getId();
    this.sourceAddr = shortMessage.getSourceAddr();
    this.destinationAddr = shortMessage.getDestinationAddr();
    this.scAddrType = 0;
    this.scAddr = shortMessage.getScAddr();
    this.moMscAddrType = 0;
    this.moMscAddr = shortMessage.getMoMscAddr();
    this.mtMscAddrType = 0;
    this.mtMscAddr = shortMessage.getMtMscAddr();
    this.submitReportRequest = shortMessage.getSubmitReportRequest();
    this.dataCodingScheme = shortMessage.getDataCodingScheme();
    this.submissionDate = shortMessage.getSubmissionDate();
    this.finalDate = shortMessage.getFinalDate();
    this.scheduleDate = shortMessage.getScheduleDate();
    this.expireDate = shortMessage.getExpireDate();
    this.deliveryStatus = shortMessage.getDeliveryStatus();
    this.bStatusReport = 0;    
    System.out.println(shortMessage.getId());
    System.out.println("String:" + String.valueOf(shortMessage.getDeliveryErrorCode()));
    System.out.println("Integer:" + shortMessage.getDeliveryErrorCode());
    System.out.println(String.valueOf(shortMessage.getDeliveryErrorCode()) == null);
    this.deliveryErrorCode = String.valueOf(shortMessage.getDeliveryErrorCode()) == "null" ? 99999 : shortMessage.getDeliveryErrorCode();
    System.out.println("Delivery: " + getDeliveryErrorCode());
    System.out.println();
    this.lastError = shortMessage.getLastError();
    this.shortMessageType = 0;
    this.deliverCount = 0;
    this.originalSourceAddr = shortMessage.getOriginalSourceAddr();
    this.originalDestinationAddr = shortMessage.getOriginalDestinationAddr();
    this.originAccount = shortMessage.getOriginAccount();
    this.destinationAccount = shortMessage.getDestinationAccount();
    this.prepaid = true;
    this.fakeImsi = shortMessage.getFakeImsi();
    this.routeNumber = shortMessage.getRouteNumber();
    this.submitMultiId = shortMessage.getSubmitMultiId();
    this.messageParts = shortMessage.getMessageParts();
    this.partNumber = shortMessage.getPartNumber();
    this.userDataLength = 0;
    this.userData = canSeeuserData ? shortMessage.getUserData() : "";
    this.destinationNetworkType = 0;
    this.vendorCodes = 0;
  }

  public ShortMessageDto(ShortMessage shortMessage) {
    this.sourceAddr = shortMessage.getSourceAddr();
    this.destinationAddr = shortMessage.getDestinationAddr();
    this.scAddrType = shortMessage.getScAddrType();
    this.scAddr = shortMessage.getScAddr();
    this.moMscAddrType = shortMessage.getMoMscAddrType();
    this.moMscAddr = shortMessage.getMoMscAddr();
    this.mtMscAddrType = shortMessage.getMtMscAddrType();
    this.mtMscAddr = shortMessage.getMtMscAddr();
    this.submitReportRequest = shortMessage.getSubmitReportRequest();
    this.dataCodingScheme = shortMessage.getDataCodingScheme();
    this.submissionDate = shortMessage.getSubmissionDate();
    this.finalDate = shortMessage.getFinalDate();
    this.scheduleDate = shortMessage.getScheduleDate();
    this.expireDate = shortMessage.getExpireDate();
    this.deliveryStatus = shortMessage.getDeliveryStatus();
    this.bStatusReport = shortMessage.getbStatusReport();
    this.deliveryErrorCode = shortMessage.getDeliveryErrorCode();
    this.shortMessageType = shortMessage.getShortMessageType();
    this.deliverCount = shortMessage.getDeliverCount();
    this.originalSourceAddr = shortMessage.getOriginalSourceAddr();
    this.originalDestinationAddr = shortMessage.getOriginalDestinationAddr();
    this.originAccount = shortMessage.getOriginAccount();
    this.destinationAccount = shortMessage.getDestinationAccount();
    this.prepaid = shortMessage.getPrepaid();
    this.fakeImsi = shortMessage.getFakeImsi();
    this.routeNumber = shortMessage.getRouteNumber();
    this.submitMultiId = shortMessage.getSubmitMultiId();
    this.messageParts = shortMessage.getMessageParts();
    this.partNumber = shortMessage.getPartNumber();
    this.userDataLength = shortMessage.getUserDataLength();
    this.userData = shortMessage.getUserData();
    this.destinationNetworkType = shortMessage.getDestinationNetworkType();
    this.vendorCodes = shortMessage.getVendorCodes();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSourceAddr() {
    return sourceAddr;
  }

  public void setSourceAddr(String sourceAddr) {
    this.sourceAddr = sourceAddr;
  }

  public String getDestinationAddr() {
    return destinationAddr;
  }

  public void setDestinationAddr(String destinationAddr) {
    this.destinationAddr = destinationAddr;
  }

  public int getScAddrType() {
    return scAddrType;
  }

  public void setScAddrType(int scAddrType) {
    this.scAddrType = scAddrType;
  }

  public String getScAddr() {
    return scAddr;
  }

  public void setScAddr(String scAddr) {
    this.scAddr = scAddr;
  }

  public int getMoMscAddrType() {
    return moMscAddrType;
  }

  public void setMoMscAddrType(int moMscAddrType) {
    this.moMscAddrType = moMscAddrType;
  }

  public String getMoMscAddr() {
    return moMscAddr;
  }

  public void setMoMscAddr(String moMscAddr) {
    this.moMscAddr = moMscAddr;
  }

  public int getMtMscAddrType() {
    return mtMscAddrType;
  }

  public void setMtMscAddrType(int mtMscAddrType) {
    this.mtMscAddrType = mtMscAddrType;
  }

  public String getMtMscAddr() {
    return mtMscAddr;
  }

  public void setMtMscAddr(String mtMscAddr) {
    this.mtMscAddr = mtMscAddr;
  }

  public int getSubmitReportRequest() {
    return submitReportRequest;
  }

  public void setSubmitReportRequest(int submitReportRequest) {
    this.submitReportRequest = submitReportRequest;
  }

  public int getDataCodingScheme() {
    return dataCodingScheme;
  }

  public void setDataCodingScheme(int dataCodingScheme) {
    this.dataCodingScheme = dataCodingScheme;
  }

  public Date getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(Date submissionDate) {
    this.submissionDate = submissionDate;
  }

  public Date getFinalDate() {
    return finalDate;
  }

  public void setFinalDate(Date finalDate) {
    this.finalDate = finalDate;
  }

  public Date getScheduleDate() {
    return scheduleDate;
  }

  public void setScheduleDate(Date scheduleDate) {
    this.scheduleDate = scheduleDate;
  }

  public Date getExpireDate() {
    return expireDate;
  }

  public void setExpireDate(Date expireDate) {
    this.expireDate = expireDate;
  }

  public String getDeliveryStatus() {
    return deliveryStatus;
  }

  public void setDeliveryStatus(String deliveryStatus) {
    this.deliveryStatus = deliveryStatus;
  }

  public int getbStatusReport() {
    return bStatusReport;
  }

  public void setbStatusReport(int bStatusReport) {
    this.bStatusReport = bStatusReport;
  }

  public int getDeliveryErrorCode() {
    return deliveryErrorCode;
  }

  public void setDeliveryErrorCode(int deliveryErrorCode) {
    this.deliveryErrorCode = deliveryErrorCode;
  }

  public String getLastError() {
    return lastError;
  }

  public void setLastError(String lastError) {
    this.lastError = lastError;
  }

  public int getShortMessageType() {
    return shortMessageType;
  }

  public void setShortMessageType(int shortMessageType) {
    this.shortMessageType = shortMessageType;
  }

  public int getDeliverCount() {
    return deliverCount;
  }

  public void setDeliverCount(int deliverCount) {
    this.deliverCount = deliverCount;
  }

  public String getOriginalSourceAddr() {
    return originalSourceAddr;
  }

  public void setOriginalSourceAddr(String originalSourceAddr) {
    this.originalSourceAddr = originalSourceAddr;
  }

  public String getOriginalDestinationAddr() {
    return originalDestinationAddr;
  }

  public void setOriginalDestinationAddr(String originalDestinationAddr) {
    this.originalDestinationAddr = originalDestinationAddr;
  }

  public String getOriginAccount() {
    return originAccount;
  }

  public void setOriginAccount(String originAccount) {
    this.originAccount = originAccount;
  }

  public String getDestinationAccount() {
    return destinationAccount;
  }

  public void setDestinationAccount(String destinationAccount) {
    this.destinationAccount = destinationAccount;
  }

  public boolean isPrepaid() {
    return prepaid;
  }

  public void setPrepaid(boolean prepaid) {
    this.prepaid = prepaid;
  }

  public String getFakeImsi() {
    return fakeImsi;
  }

  public void setFakeImsi(String fakeImsi) {
    this.fakeImsi = fakeImsi;
  }

  public String getRouteNumber() {
    return routeNumber;
  }

  public void setRouteNumber(String routeNumber) {
    this.routeNumber = routeNumber;
  }

  public int getSubmitMultiId() {
    return submitMultiId;
  }

  public void setSubmitMultiId(int submitMultiId) {
    this.submitMultiId = submitMultiId;
  }

  public int getMessageParts() {
    return messageParts;
  }

  public void setMessageParts(int messageParts) {
    this.messageParts = messageParts;
  }

  public int getPartNumber() {
    return partNumber;
  }

  public void setPartNumber(int partNumber) {
    this.partNumber = partNumber;
  }

  public int getUserDataLength() {
    return userDataLength;
  }

  public void setUserDataLength(int userDataLength) {
    this.userDataLength = userDataLength;
  }

  public String getUserData() {
    return userData;
  }

  public void setUserData(String userData) {
    this.userData = userData;
  }

  public int getDestinationNetworkType() {
    return destinationNetworkType;
  }

  public void setDestinationNetworkType(int destinationNetworkType) {
    this.destinationNetworkType = destinationNetworkType;
  }

  public Integer getVendorCodes() {
    return vendorCodes;
  }

  public void setVendorCodes(Integer vendorCodes) {
    this.vendorCodes = vendorCodes;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ShortMessageDto [id=").append(id).append(", sourceAddr=").append(sourceAddr)
        .append(", destinationAddr=").append(destinationAddr).append(", scAddrType=").append(scAddrType)
        .append(", scAddr=").append(scAddr).append(", moMscAddrType=").append(moMscAddrType).append(", moMscAddr=")
        .append(moMscAddr).append(", mtMscAddrType=").append(mtMscAddrType).append(", mtMscAddr=").append(mtMscAddr)
        .append(", submitReportRequest=").append(submitReportRequest).append(", dataCodingScheme=")
        .append(dataCodingScheme).append(", submissionDate=").append(submissionDate).append(", finalDate=")
        .append(finalDate).append(", scheduleDate=").append(scheduleDate).append(", expireDate=").append(expireDate)
        .append(", deliveryStatus=").append(deliveryStatus).append(", bStatusReport=").append(bStatusReport)
        .append(", deliveryErrorCode=").append(deliveryErrorCode).append(", lastError=").append(lastError)
        .append(", shortMessageType=").append(shortMessageType).append(", deliverCount=").append(deliverCount)
        .append(", originalSourceAddr=").append(originalSourceAddr).append(", originalDestinationAddr=")
        .append(originalDestinationAddr).append(", originAccount=").append(originAccount)
        .append(", destinationAccount=").append(destinationAccount).append(", prepaid=").append(prepaid)
        .append(", fakeImsi=").append(fakeImsi).append(", routeNumber=").append(routeNumber).append(", submitMultiId=")
        .append(submitMultiId).append(", messageParts=").append(messageParts).append(", partNumber=").append(partNumber)
        .append(", userDataLength=").append(userDataLength).append(", userData=").append(userData)
        .append(", destinationNetworkType=").append(destinationNetworkType).append(", vendorCodes=").append(vendorCodes)
        .append("]");
    return builder.toString();
  }

}
