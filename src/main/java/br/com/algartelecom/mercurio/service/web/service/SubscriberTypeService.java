package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.SUBSCRIBER_TYPE;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.SubscriberType;
import br.com.algartelecom.mercurio.relational.repositories.SubscriberTypeRepository;

@Service
public class SubscriberTypeService {

	@Autowired
	SubscriberTypeRepository subscriberTypeRepository;

	@Autowired
	AuditService auditService;

	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> create(SubscriberType subscriberType) {
		if (subscriberTypeRepository.findByType(subscriberType.getType()) != null) {
			return createError("Subscriber Type already exists", BAD_REQUEST);
		}
		subscriberType.setCreationDate(new Date());
		subscriberType.setCreationUser(getLoggedUser());
		subscriberTypeRepository.save(subscriberType);
		auditLog(LogAction.SAVE, subscriberType);
		return new ResponseEntity<String>(OK);
	}

	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> update(SubscriberType subscriberType) {
		SubscriberType old = subscriberTypeRepository.findByType(subscriberType.getType());
		if (old == null) {
			return createError("Subscriber Type don't exist", BAD_REQUEST);
		}
		SubscriberType before = new SubscriberType(old);
		subscriberType.setUpdateDate(new Date());
		subscriberType.setUpdateUser(getLoggedUser());
		subscriberTypeRepository.save(subscriberType);
		auditUpdateLog(before, subscriberType);
		return new ResponseEntity<String>(OK);
	}

	@Transactional(readOnly = true)
	public SubscriberType getByType(String type) {
		return subscriberTypeRepository.findByType(type);
	}

	@Transactional(readOnly = true)
	public List<SubscriberType> getAll() {
		return subscriberTypeRepository.findAll();
	}

	@Transactional(rollbackFor = Exception.class )
	public void delete(String type) {
		SubscriberType before = subscriberTypeRepository.findByType(type);
		if (before != null) {
			auditLog(LogAction.DELETE, before);
			subscriberTypeRepository.delete(before);
		}
	}

	private void auditLog(LogAction action, SubscriberType type) {
		auditService.register(getLoggedUser(), SUBSCRIBER_TYPE, action, type.getType(), type);
	}

	private void auditUpdateLog(SubscriberType before, SubscriberType after) {
		auditService.registerUpdate(getLoggedUser(), SUBSCRIBER_TYPE, before.getType(), before, after);
	}
}
