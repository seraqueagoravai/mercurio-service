package br.com.algartelecom.mercurio.service.web.dto;

import java.util.Date;

public class DatePair {
  
  private Date initDate;
  private Date finalDate;
  
  public Date getInitDate() {
    return initDate;
  }
  public void setInitDate(Date initDate) {
    this.initDate = initDate;
  }
  public Date getFinalDate() {
    return finalDate;
  }
  public void setFinalDate(Date finalDate) {
    this.finalDate = finalDate;
  } 

}
