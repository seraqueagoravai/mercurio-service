package br.com.algartelecom.mercurio.service.web.utils;

import br.com.algartelecom.mercurio.service.web.dto.AccessResource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SecurityUtils {

  public static String getLoggedUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      return auth.getName();
    }
    return "anonymous";
  }

  public static boolean isAdmin() {
    return hasAuthentication() && hasAuthority(getAdminRole());
  }
  
  public static String forbiddenRole(){
    return "ROLE_" + UUID.randomUUID().toString().substring(0, 8);
  }

  public static boolean hasAuthority(String authority) {
    return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
        .anyMatch(auth -> auth.getAuthority().equals(authority));

  }

  public static boolean isAuthenticatedAndNotAnonymous() {
    return hasAuthentication() && !isAnonymous() && isAuthenticated();
  }

  public static boolean isAnonymous() {
    return (SecurityContextHolder.getContext()
        .getAuthentication() instanceof AnonymousAuthenticationToken);
  }

  public static boolean hasAuthentication() {
    return SecurityContextHolder.getContext().getAuthentication() != null;
  }

  public static boolean isAuthenticated() {
    return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
  }

  public static boolean isFreeUrlAndIsAuthenticated(String url) {
    return (isIndexUrl(url) || isPrincipalUrl(url) || isPingUrl(url)) && isAuthenticatedAndNotAnonymous();
  }

  public static boolean isSPSUrl(String url) {
    if (url.split("/").length > 1)
      return "sps".equals(url.split("/")[1]);
    else
      return false;
  }
  
  public static boolean isPingUrl(String url) {
    if (url.split("/").length > 1)
      return "ping".equals(url.split("/")[1]);
    else
      return false;
  }
  
  public static boolean isSijuUrl(String url) {
    if (url.split("/").length > 1)
      return "siju".equals(url.split("/")[1]);
    else
      return false;
  }

  public static boolean isLoginUrl(String url) {
    return "/login".equals(url);
  }

  public static boolean isPrincipalUrl(String url) {
    if (url.split("/").length > 1)
      return "principal".equals(url.split("/")[1]);
    else
      return false;
  }

  public static boolean isIndexUrl(String url) {
    return "/".equals(url);
  }

  public static String getAdminRole() {
    return "ROLE_ADMIN";
  }

  public static List<AccessResource> adminOnlyRoles() {
    return Arrays
        .stream(
            new String[] {"roles.list", "access-control.list", "roles.edit", "access-control.edit"})
        .map(AccessResource::new).collect(Collectors.toList());
  }

}
