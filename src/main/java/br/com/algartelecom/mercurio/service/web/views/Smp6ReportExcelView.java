package br.com.algartelecom.mercurio.service.web.views;

import br.com.algartelecom.mercurio.service.web.dto.Smp6Entry;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

public class Smp6ReportExcelView extends AbstractExcelView {

  private final String INDICATOR_FIELD = "SMP6";
  private final String TOTAL_INDEX = "028";
  private final String SENT_INDEX = "027";
  private final String SENT_DESCRIPTION =
      "Número total de Mensagens de Texto enviadas a partir da rede da prestadora e entregues ao usuário em até 60 (sessenta) segundos, no mês;";
  private final String TOTAL_DESCRIPTION =
      "Número total de tentativas de envio de Mensagens de Texto na rede da prestadora, no mês.";
  private final String PERCENTAGE_DESCRIPTION = "Resultado";

  SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

  @Override
  protected void buildExcelDocument(Map<String, Object> model, Workbook workbook,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    Map<Date, Map> datesMap = (Map<Date, Map>) model.get("entries");
    List<Map<String, Smp6Entry>> listAll = new ArrayList<>();
    for (Date date : datesMap.keySet()) {

      Sheet sheet = workbook.createSheet(convertDateToString(date));
      createHeader(sheet, getTittles());


      Map<String, Smp6Entry> cnMap = datesMap.get(date);
      Map<String, Smp6Entry> cnMapCopy = null;
      int count = 1;
      String[] cns = initCn();
      for (String cn : cnMap.keySet()) {
        Smp6Entry entry = cnMap.get(cn);
        Row row = sheet.createRow(count++);
        row.createCell(0).setCellValue(INDICATOR_FIELD);
        row.createCell(1).setCellValue(cn);
        row.createCell(2).setCellValue(getPrimaryUnit(cn));
        row.createCell(3).setCellValue(SENT_INDEX);
        row.createCell(4).setCellValue(SENT_DESCRIPTION);
        row.createCell(5).setCellValue(entry.getSent());

        row = sheet.createRow(count++);
        row.createCell(0).setCellValue(INDICATOR_FIELD);
        row.createCell(1).setCellValue(cn);
        row.createCell(2).setCellValue(getPrimaryUnit(cn));
        row.createCell(3).setCellValue(TOTAL_INDEX);
        row.createCell(4).setCellValue(TOTAL_DESCRIPTION);
        row.createCell(5).setCellValue(entry.getTotal());


        row = sheet.createRow(count++);
        row.createCell(0).setCellValue(INDICATOR_FIELD);
        row.createCell(1).setCellValue(cn);
        row.createCell(2).setCellValue(getPrimaryUnit(cn));
        // row.createCell(3).setCellValue(TOTAL_INDEX);
        row.createCell(4).setCellValue(PERCENTAGE_DESCRIPTION);
        if (entry.getSent() != 0 && entry.getTotal() != 0)
          row.createCell(5).setCellValue(entry.getPercentage());

        cnMapCopy = new HashMap<>();
        cnMapCopy.put(cn, entry);
        listAll.add(cnMapCopy);
      }
    }

    Sheet sheet = workbook.createSheet("Geral");
    createHeader(sheet, getTittles());
    int count = 1;
    String[] cns = initCn();
    for (String cn : cns) {
      int[] result = getTotal(cn, listAll);
      int totalSent = 0;
      int totalIndex = 0;
      if (result != null) {
        totalSent = result[0];
        totalIndex = result[1];
      }

      Row row = sheet.createRow(count++);
      row.createCell(0).setCellValue(INDICATOR_FIELD);
      row.createCell(1).setCellValue(cn);
      row.createCell(2).setCellValue(getPrimaryUnit(cn));
      row.createCell(3).setCellValue(SENT_INDEX);
      row.createCell(4).setCellValue(SENT_DESCRIPTION);
      row.createCell(5).setCellValue(totalSent);

      row = sheet.createRow(count++);
      row.createCell(0).setCellValue(INDICATOR_FIELD);
      row.createCell(1).setCellValue(cn);
      row.createCell(2).setCellValue(getPrimaryUnit(cn));
      row.createCell(3).setCellValue(TOTAL_INDEX);
      row.createCell(4).setCellValue(TOTAL_DESCRIPTION);
      row.createCell(5).setCellValue(totalIndex);

      row = sheet.createRow(count++);
      row.createCell(0).setCellValue(INDICATOR_FIELD);
      row.createCell(1).setCellValue(cn);
      row.createCell(2).setCellValue(getPrimaryUnit(cn));
      // row.createCell(3).setCellValue(TOTAL_INDEX);
      row.createCell(4).setCellValue(PERCENTAGE_DESCRIPTION);
      if (totalSent != 0 && totalIndex != 0)
        row.createCell(5).setCellValue(getTotalPercentage(totalSent, totalIndex));
    }


    response.setHeader("Content-Disposition", "attachment; filename=\"SMP6.xlsx\"");

  }

  private String[] initCn() {
    return new String[] {"16", "17", "34", "35", "37", "64", "67"};
  }
  
  private int[] initCns(){
    return new int[] {16, 17, 34, 35, 37, 64, 67};
  }

  private int[] getTotal(String cn, List<Map<String, Smp6Entry>> list) {
    int[] result = new int[2];
    int totalSent = 0;
    int totalIndex = 0;
    if (list.size() > 0) {
      for (int i = 0; i < list.size(); i++) {
        Map<String, Smp6Entry> map = list.get(i);
        if (map.containsKey(cn)) {
          Smp6Entry entry = map.get(cn);
          totalSent += entry.getSent();
          totalIndex += entry.getTotal();
        }
      }
      result[0] = totalSent;
      result[1] = totalIndex;
    }
    return result;
  }


  public float getTotalPercentage(int sent, int total) {
    return ((float) sent / total) * 100;
  }

  private String getPrimaryUnit(String cn) {
    switch (cn) {
      case "16":
        return "00546";
      case "17":
        return "00549";
      case "34":
        return "00551";
      case "35":
        return "00554";
      case "37":
        return "00556";
      case "64":
        return "00558";
      case "67":
        return "00561";
      default:
        break;
    }
    return null;
  }

  private String convertDateToString(Date date) {
    return dateFormatter.format(date);
  }

  private String[] getTittles() {
    return new String[] {"Indicador", "CN", "UnidadePrimaria", "indice", "DescriçãoIndice",
        "valor"};
  }

}
