package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.AuditLog;
import br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity;
import br.com.algartelecom.mercurio.relational.model.vo.User;
import br.com.algartelecom.mercurio.service.web.dto.ConfigurationEntityDto;
import br.com.algartelecom.mercurio.service.web.dto.LogDto;
import br.com.algartelecom.mercurio.service.web.service.LogService;
import br.com.algartelecom.mercurio.service.web.service.UserService;

@RestController
@RequestMapping("/log")
public class LogResource {

	@Autowired
	UserService userService;
	
	@Autowired
	LogService logService;

	@RequestMapping(value = "/entity", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<ConfigurationEntityDto> getEntities() {
		return Arrays.stream(ConfigurationEntity.values()).map(ConfigurationEntityDto::new)
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public LogDto getLog(@PathVariable Long id) {
		return new LogDto(logService.findAuditLogById(id));
	}

	@RequestMapping(value = "/users/all", method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<User> getUsers() {
		return userService.getUsers();
	}

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public List<AuditLog> getLogList(@RequestParam String entity, @RequestParam String user) {
		if ("all".equals(user)) {
			return logService.findAuditLogByEntity(entity);
		}
		return logService.findAuditLogByEntityAndUsername(entity, user);
	}

}
