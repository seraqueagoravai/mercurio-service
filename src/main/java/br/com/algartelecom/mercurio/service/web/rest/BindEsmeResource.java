package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.model.vo.BindEsme;
import br.com.algartelecom.mercurio.relational.model.vo.BindType;
import br.com.algartelecom.mercurio.service.web.dto.PasswordUpdateDto;
import br.com.algartelecom.mercurio.service.web.service.BindEsmeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.Arrays;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/bind-esme")
public class BindEsmeResource {

	@Autowired
	BindEsmeService bindEsmeService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<BindEsme> getAll() {
		return bindEsmeService.getAllBindEsme();
	}

	@RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<BindEsme> get(@PathVariable Long id) {
		return bindEsmeService.getBindEsmeId(id).map(result -> new ResponseEntity<BindEsme>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody BindEsme bindEsme) throws URISyntaxException {
		return bindEsmeService.createBindEsme(bindEsme);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody BindEsme bindEsme) throws URISyntaxException {
		return bindEsmeService.update(bindEsme);
	}

	@RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@PathVariable Long id) throws URISyntaxException {
		return bindEsmeService.delete(id);
	}

	@RequestMapping(value = "/password", method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updatePassword(@RequestBody PasswordUpdateDto passwordUpdate) {
		return bindEsmeService.getBindEsmeId(passwordUpdate.getId()).map(bindEsme -> {
			if (bindEsme.getPassword().equals(passwordUpdate.getOldPassword())) {
				bindEsme.setPassword(passwordUpdate.getNewPassword());
				bindEsmeService.createBindEsme(bindEsme);
				return new ResponseEntity<String>(OK);
			} else {
				return createError("Old password don't match", BAD_REQUEST);
			}
		}).orElse(createError("BindEsme doensn't exists", BAD_REQUEST));
	}

	@RequestMapping(value = "/bind-types", method = GET, produces = APPLICATION_JSON_VALUE)
	public String[] getBindTypes() {
		return Arrays.stream(BindType.values()).map(Enum::name).toArray(String[]::new);
	}

}
