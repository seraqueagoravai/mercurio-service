package br.com.algartelecom.mercurio.service.web.dto;

public class LastSecondDto {

  Integer value;
  
  public LastSecondDto() {}

  public LastSecondDto(Integer value) {
    this.value = value;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }
  
  
  
}
