//package br.com.algartelecom.mercurio.service.web.rest;
//
//import br.com.algartelecom.mercurio.relational.model.vo.online.ClusterMember;
//import br.com.algartelecom.mercurio.service.web.dto.JsonValueDto;
//import br.com.algartelecom.mercurio.service.web.service.ClusterMemberService;
//import br.com.algartelecom.mercurio.service.web.utils.SmppCommander;
//import br.com.algartelecom.mercurio.smpp.core.model.AccountConnectionsHolder;
//import br.com.algartelecom.mercurio.trace.smpp.model.SmppFields;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.ApplicationContext;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.Optional;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
//import static org.springframework.web.bind.annotation.RequestMethod.GET;
//
//@RestController
//@RequestMapping("/trace")
//public class TraceResource {
//
//  private static final Logger log = LoggerFactory.getLogger(TraceResource.class);
//
//  @Value("${mercurio.trace.server.url}")
//  String traceServerUrl;
//  @Autowired
//  ClusterMemberService clusterMemberService;
//  @Autowired
//  ApplicationContext applicationContext;
//
//  @RequestMapping(value = "/server", method = GET, produces = APPLICATION_JSON_VALUE)
//  public ResponseEntity<JsonValueDto> getServerUrl() {
//    return new ResponseEntity<>(new JsonValueDto(traceServerUrl), HttpStatus.OK);
//  }
//
//  @RequestMapping(value = "/filter/fields", method = GET, produces = APPLICATION_JSON_VALUE)
//  public Map<String, String> getSmppFields() {
//    return SmppFields.SMPP_FIELDS_MAP;
//  }
//
//  @RequestMapping(value = "/binds", method = GET, produces = APPLICATION_JSON_VALUE)
//  public List<AccountConnectionsHolder> getBinds() {
//    Map<String, List<AccountConnectionsHolder>> groupedAccounts =
//        getSmppMembersStream().map(this::createSmppCommander)
//            .map(command -> command.getBindsConnections())
//            .filter(Optional::isPresent)
//            .map(Optional::get)
//            .flatMap(Arrays::stream)
//            .collect(Collectors.groupingBy(AccountConnectionsHolder::getAccount));
//
//    return groupedAccounts.entrySet().stream().map(entry -> {
//      AccountConnectionsHolder result = new AccountConnectionsHolder(entry.getKey());
//      result.setConnections(entry.getValue()
//          .stream()
//          .filter(conn -> conn.getConnections() != null)
//          .flatMap(conn -> conn.getConnections().stream())
//          .collect(Collectors.toSet()));
//      return result;
//    }).collect(Collectors.toList());
//
//  }
//
//  private Stream<ClusterMember> getSmppMembersStream() {
//    return getListClusterMember().stream().filter(s -> s.getType().equals("smpp"));
//  }
//
//  public List<ClusterMember> getListClusterMember() {
//    return clusterMemberService.getAllClusterMembers();
//  }
//
//  private SmppCommander createSmppCommander(ClusterMember smpp) {
//    SmppCommander command = applicationContext.getBean(SmppCommander.class);
//    command.setSmpp(smpp);
//    return command;
//  }
//}
