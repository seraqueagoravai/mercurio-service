package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.DELIVERY_STRATEGY;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.DeliveryStrategy;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.repositories.DeliveryStrategyRepository;

@Service
public class DeliveryStrategyService {

	@Autowired
	DeliveryStrategyRepository dsRepository;

	@Autowired
	AuditService auditService;

	public ResponseEntity<String> createDeliveryStrategy(DeliveryStrategy deliveryStrategy) {
		deliveryStrategy.setCreationDate(new Date());
		deliveryStrategy.setCreationUser(getLoggedUser());
		dsRepository.save(deliveryStrategy);
		auditLog(LogAction.SAVE, deliveryStrategy);
		return new ResponseEntity<String>(OK);
	}

	public ResponseEntity<String> update(DeliveryStrategy before, DeliveryStrategy after) {
		after.setUpdateDate(new Date());
		after.setUpdateUser(getLoggedUser());
		dsRepository.save(after);
		auditUpdateLog(before, after);
		return new ResponseEntity<String>(OK);
	}

	public void delete(DeliveryStrategy ds) {
		dsRepository.deleteByNetworkAndErrorAndLevel(ds.getProtocolErrorCode().getNetwork(),
				ds.getProtocolErrorCode().getErrorCode(), ds.getStage());
		auditLog(LogAction.DELETE, ds);
	}

	public Optional<DeliveryStrategy> getDeliveryStrategy(DeliveryStrategy deliveryStrategy) {
		return Optional.ofNullable(
				dsRepository.findByNetworkAndErrorAndLevel(deliveryStrategy.getProtocolErrorCode().getNetwork(),
						deliveryStrategy.getProtocolErrorCode().getErrorCode(), deliveryStrategy.getStage()));
	}

	public Optional<DeliveryStrategy> getDeliveryStrategy(String network, Integer error, Integer level) {
		return Optional.ofNullable(dsRepository.findByNetworkAndErrorAndLevel(network, error, level));
	}

	public List<DeliveryStrategy> getAll() {
		return dsRepository.findAll();
	}

	private void auditLog(LogAction action, DeliveryStrategy deliveryStrategy) {
		auditService.register(getLoggedUser(), DELIVERY_STRATEGY, action, createKey(deliveryStrategy),
				deliveryStrategy);
	}

	private void auditUpdateLog(DeliveryStrategy before, DeliveryStrategy deliveryStrategy) {
		auditService.registerUpdate(getLoggedUser(), DELIVERY_STRATEGY, createKey(before), before, deliveryStrategy);
	}

	private String createKey(DeliveryStrategy ds) {
		return "network: " + ds.getProtocolErrorCode().getNetwork() + "\n error: "
				+ ds.getProtocolErrorCode().getErrorCode() + "\nlevel: " + ds.getStage();
	}
}
