package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getAdminRole;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.AccessRole;
import br.com.algartelecom.mercurio.relational.model.vo.Roles;
import br.com.algartelecom.mercurio.relational.model.vo.RolesMapping;
import br.com.algartelecom.mercurio.relational.model.vo.User;
import br.com.algartelecom.mercurio.relational.repositories.AccessRoleRepository;
import br.com.algartelecom.mercurio.relational.repositories.RolesMappingRepository;
import br.com.algartelecom.mercurio.relational.repositories.RolesRepository;
import br.com.algartelecom.mercurio.relational.repositories.UserRepository;
import br.com.algartelecom.mercurio.service.web.dto.AccessResource;
import br.com.algartelecom.mercurio.service.web.utils.SecurityUtils;

@Service
public class PrincipalService {

    @Autowired
    RolesRepository rolesRepository;

    @Autowired
    RolesMappingRepository rolesMappingRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccessRoleRepository accessRoleRepository;

    public List<AccessResource> getallAccessResource() {
        saveUser();

        List<String> userRoles = getUserRoles();

        Iterable<RolesMapping> rolemappings = rolesMappingRepository.findAll();

        if (userRoles.contains(getAdminRole())) {
            List<AccessResource> result =
                    Arrays.stream(accessRoleRepository.findAll().toArray())
                            .map(s -> new AccessResource((AccessRole) s)).collect(Collectors.toList());
            result.addAll(SecurityUtils.adminOnlyRoles());
            return result;
        }

        return StreamSupport.stream(rolemappings.spliterator(), false)
                .filter(
                        item -> item.getRoles() != null && userRoles.contains(item.getRoles().getRoles()))
                .map(AccessResource::new).collect(Collectors.toList());
    }

    public void saveUser(){
        String userName = getLoggedUser();
        User user = userRepository.findByUsername(userName);

        if (user != null){
            user.setUpdateDate(new Date());
        } else {
            user = new User();
            user.setUsername(userName);
            user.setCreationDate(new Date());
        }
        userRepository.save(user);
    }

    public ResponseEntity<Void> hasAccessResource(String resource) {
        List<RolesMapping> mapping = rolesMappingRepository.findByAccessRole_roleResource(resource);
        List<String> userRoles = getUserRoles();
        if (userRoles.contains(getAdminRole()) || hasAnyRole(mapping, userRoles)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    public ResponseEntity<Void> hasValidAccessPrincipal() {
        if (SecurityUtils.isAdmin()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return Optional.ofNullable(getUserRoles()).map(this::hasRegisteredRole)
                .orElse(new ResponseEntity<>(HttpStatus.FORBIDDEN));
    }

    private ResponseEntity<Void> hasRegisteredRole(List<String> userRoles) {
        List<Roles> roles = rolesRepository.findByPlatform("smsc");

        Optional<Roles> optionalRoles = StreamSupport.stream(roles.spliterator(), false)
                .filter(f -> f.getRoles() != null && userRoles.contains(f.getRoles())).findAny();

        if (optionalRoles.isPresent())
            return new ResponseEntity<>(HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);

    }

    private boolean hasAnyRole(List<RolesMapping> mapping, List<String> userRoles) {
        Optional<RolesMapping> rolesMapping = StreamSupport.stream(mapping.spliterator(), false)
                .filter(c -> c.getRoles() != null && userRoles.contains(c.getRoles().getRoles())).findAny();

        if (rolesMapping.isPresent()) {
            return true;
        }
        return false;
    }

    private List<String> getUserRoles() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .map(auth -> auth.getAuthority()).collect(Collectors.toList());
    }
}
