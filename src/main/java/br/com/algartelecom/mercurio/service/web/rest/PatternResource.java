package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.util.NumberPatternService;
import br.com.algartelecom.mercurio.service.web.dto.PatternDto;
import br.com.algartelecom.mercurio.service.web.dto.PatternResourceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("pattern")
public class PatternResource {

	@Autowired
	NumberPatternService numberPatternService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public PatternResourceDto get(@RequestBody PatternDto pattern) {
		return new PatternResourceDto(numberPatternService.validateRegex(pattern.getPattern(), pattern.getText()));
	}

}
