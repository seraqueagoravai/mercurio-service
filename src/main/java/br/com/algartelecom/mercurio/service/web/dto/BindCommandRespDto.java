package br.com.algartelecom.mercurio.service.web.dto;


import br.com.algartelecom.mercurio.smpp.core.constant.CommandStatus;
import br.com.algartelecom.mercurio.smpp.core.model.CommandResp;
import com.google.common.base.MoreObjects;

public class BindCommandRespDto {

  private CommandStatus status;
  private String message;
  private String server;

  public BindCommandRespDto() {};

  public BindCommandRespDto(CommandResp resp, String server) {
    this.status = resp.getStatus();
    this.message = resp.getMessage();
    this.server = server;
  }

  public CommandStatus getStatus() {
    return status;
  }

  public void setStatus(CommandStatus status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getServer() {
    return server;
  }

  public void setServer(String server) {
    this.server = server;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("server", server)
        .add("status", status)
        .add("message", message)
        .toString();
  }

}
