package br.com.algartelecom.mercurio.service.config.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class MetricsConfig {

  @Bean
  public Slf4jReporter slf4jReporter() {
    Slf4jReporter reporter = Slf4jReporter.forRegistry(metricRegistry())
        .outputTo(LoggerFactory.getLogger("br.com.algartelecom.report.metrics"))
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .withLoggingLevel(Slf4jReporter.LoggingLevel.DEBUG)
        .build();
    reporter.start(30, TimeUnit.SECONDS);
    return reporter;
  }

  @Bean
  public MetricRegistry metricRegistry() {
    return new MetricRegistry();
  }

}
