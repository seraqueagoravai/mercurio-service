package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.ShortMessage;
import br.com.algartelecom.mercurio.relational.model.vo.ShortMessageHistory;
import br.com.algartelecom.mercurio.relational.repositories.ShortMessageRepository;
import br.com.algartelecom.mercurio.service.web.dto.CdrRequestDto;
import br.com.algartelecom.mercurio.service.web.dto.ShortMessageDto;
import br.com.algartelecom.mercurio.service.web.service.CdrService2;
import br.com.algartelecom.mercurio.service.web.utils.ConstantsFilters;

@RestController
@RequestMapping(value = "/cdr")
public class CdrResource {

    @Autowired
    CdrService2 cdrService;

    @Autowired
    ShortMessageRepository shortMessageRepository;

    // ExecutorService rxExecutorService = Executors.newFixedThreadPool(8);

    @RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ShortMessage> get(@PathVariable String id) {
        return cdrService.findById(id);
        // .map(cdrService::secureSmsData)
                // .orElse(new ResponseEntity<>(NOT_FOUND));
    }

    // @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    // public void delete(@PathVariable Long id) throws URISyntaxException {
    //     cdrService.findById(id).ifPresent(cdrService::delete);
    // }

    @RequestMapping(value = "/offline", method = POST, produces = APPLICATION_JSON_VALUE)
    public ShortMessageHistory saveOffline(@RequestBody ShortMessageHistory shortMessage) {
        return cdrService.saveOffline(shortMessage);
    }

    @RequestMapping(value = "/online", method = POST, produces = APPLICATION_JSON_VALUE)
    public ShortMessage saveOline(@RequestBody ShortMessage shortMessage) {
        return cdrService.saveOnline(shortMessage);
    }

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public void search(HttpServletResponse response, CdrRequestDto request) throws Exception {
        // DelegatingSecurityContextExecutor executor
        //         = new DelegatingSecurityContextExecutor(rxExecutorService, SecurityContextHolder.getContext());
        // final CountDownLatch latch = new CountDownLatch(1);
        // JsonFactory jfactory = new JsonFactory();
        // JsonGenerator generator = jfactory.createGenerator(response.getWriter());
        // boolean canSeeUserData = cdrService.hasAccessToUserData();
        // cdrService.getCdr(request, generator, latch, executor);
        cdrService.getCdr(response, request);
        // latch.await();
        // response.flushBuffer();
        // generator.close();
    }

    @RequestMapping(value = "/all", method = GET, produces = APPLICATION_JSON_VALUE)
    public List<ShortMessageDto> search2(HttpServletResponse response, CdrRequestDto request) throws Exception {
        return cdrService.getCdr(request);
    }

    @RequestMapping(value = "/errors/map", method = GET, produces = APPLICATION_JSON_VALUE)
    public String[] getMapErrors() {
        return ConstantsFilters.getListMapErrors();
    }

    @RequestMapping(value = "/errors/smpp", method = GET, produces = APPLICATION_JSON_VALUE)
    public String[] getSmppErrors() {
        return ConstantsFilters.getListSmppErrors();
    }

    @RequestMapping(value = "/codes/vendor", method = GET, produces = APPLICATION_JSON_VALUE)
    public String[] VendorCodes() {
        return ConstantsFilters.getVendorCodes();
    }


}
