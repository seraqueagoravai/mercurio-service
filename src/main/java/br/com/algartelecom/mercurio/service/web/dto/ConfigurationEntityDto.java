package br.com.algartelecom.mercurio.service.web.dto;

import br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity;

public class ConfigurationEntityDto {

  private String name;
  private String value;

  public ConfigurationEntityDto(ConfigurationEntity entity) {
    this.name = entity.toString();
    this.value = entity.getEntity();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

}
