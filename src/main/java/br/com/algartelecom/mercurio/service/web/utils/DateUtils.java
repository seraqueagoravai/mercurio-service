package br.com.algartelecom.mercurio.service.web.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {

  public static final SimpleDateFormat sijuFormatter = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");

  public static Date setToLastSecondOfDay(Date date) {
    if (date != null) {
      LocalDateTime aux = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
          .withHour(23).withMinute(59).withSecond(59);
      return Date.from(aux.atZone(ZoneId.systemDefault()).toInstant());
    }
    return date;
  }

}
