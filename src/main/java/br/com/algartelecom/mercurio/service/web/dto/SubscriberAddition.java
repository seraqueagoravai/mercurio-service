package br.com.algartelecom.mercurio.service.web.dto;

import javax.validation.constraints.NotNull;

public class SubscriberAddition {
  
  @NotNull(message = "subid may not be null")
  private String subid;
  private String subname;
  private String subaddress;
  private Integer mstype;
  private Integer ton;
  private Integer npi;
  private Integer service;
  @NotNull(message = "bsm may not be null")
  private Integer bsm;
  private Integer ocos;
  private Integer tcos;
  private Integer schedulemod;
  @NotNull(message = "imsiid may not be null")
  private String imsiid;
  
  public String getSubid() {
    return subid;
  }
  public void setSubid(String subid) {
    this.subid = subid;
  }
  public String getSubname() {
    return subname;
  }
  public void setSubname(String subname) {
    this.subname = subname;
  }
  public String getSubaddress() {
    return subaddress;
  }
  public void setSubaddress(String subaddress) {
    this.subaddress = subaddress;
  }
  public Integer getMstype() {
    return mstype;
  }
  public void setMstype(Integer mstype) {
    this.mstype = mstype;
  }
  public Integer getTon() {
    return ton;
  }
  public void setTon(Integer ton) {
    this.ton = ton;
  }
  public Integer getNpi() {
    return npi;
  }
  public void setNpi(Integer npi) {
    this.npi = npi;
  }
  public Integer getService() {
    return service;
  }
  public void setService(Integer service) {
    this.service = service;
  }
  public Integer getBsm() {
    return bsm;
  }
  public void setBsm(Integer bsm) {
    this.bsm = bsm;
  }
  public Integer getOcos() {
    return ocos;
  }
  public void setOcos(Integer ocos) {
    this.ocos = ocos;
  }
  public Integer getTcos() {
    return tcos;
  }
  public void setTcos(Integer tcos) {
    this.tcos = tcos;
  }
  public Integer getSchedulemod() {
    return schedulemod;
  }
  public void setSchedulemod(Integer schedulemod) {
    this.schedulemod = schedulemod;
  }
  public String getImsiid() {
    return imsiid;
  }
  public void setImsiid(String imsiid) {
    this.imsiid = imsiid;
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SubscriberAddition [subid=").append(subid).append(", subname=").append(subname)
        .append(", subaddress=").append(subaddress).append(", mstype=").append(mstype)
        .append(", ton=").append(ton).append(", npi=").append(npi).append(", service=")
        .append(service).append(", bsm=").append(bsm).append(", ocos=").append(ocos)
        .append(", tcos=").append(tcos).append(", schedulemod=").append(schedulemod)
        .append(", imsiid=").append(imsiid).append("]");
    return builder.toString();
  }
  
}
