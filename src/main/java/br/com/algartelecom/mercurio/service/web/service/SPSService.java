package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.Setting;
import br.com.algartelecom.mercurio.relational.model.vo.Subscriber;
import br.com.algartelecom.mercurio.relational.model.vo.SubscriberType;
import br.com.algartelecom.mercurio.relational.repositories.SettingsRepository;
import br.com.algartelecom.mercurio.relational.repositories.SubscriberRepository;
import br.com.algartelecom.mercurio.relational.repositories.SubscriberTypeRepository;
import br.com.algartelecom.mercurio.service.web.dto.SubscriberAddition;
import br.com.algartelecom.mercurio.service.web.dto.SubscriberRemoval;

@Service
public class SPSService {

	String DEFAULT_SUBSCRIBER_TYPE = "mercurio.default.subscriber.type";
	String RESOURCE_USER = "sps";
	@Autowired
	SubscriberRepository subscriberRepository;
	@Autowired
	SettingsRepository settingsRepository;
	@Autowired
	SubscriberTypeRepository subscriberTypeRepository;
	@Autowired
	AuditService auditService;

	public ResponseEntity<String> delete(SubscriberRemoval subscriberRemoval) {
		String msisdn = subscriberRemoval.getSubid();
		Subscriber before = subscriberRepository.findByMsisdn(msisdn);
		if (before == null) {
			return createError("Subscriber don't exists", BAD_REQUEST);
	}
		subscriberRepository.delete(before);
		auditLog(LogAction.DELETE, before);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	public ResponseEntity<String> create(SubscriberAddition subscriberAddition) {
		String msisdn = subscriberAddition.getSubid();
		if (subscriberRepository.findByMsisdn(msisdn) != null) {
			return createError("Subscriber already exists", BAD_REQUEST);
		}
		Subscriber subscriber = createSubscriber(subscriberAddition);
		subscriberRepository.save(subscriber);
		auditLog(LogAction.SAVE, subscriber);
		return new ResponseEntity<String>(OK);
	}

	private Subscriber createSubscriber(SubscriberAddition subscriberAddition) {
		String type = String.valueOf(subscriberAddition.getBsm());
		SubscriberType subscriberType = subscriberTypeRepository.findByType(type);

		if (subscriberType == null) {
			Setting defaultSubscriberType = settingsRepository.findBySettingKey(DEFAULT_SUBSCRIBER_TYPE);
			subscriberType = subscriberTypeRepository.findByType(defaultSubscriberType.getSettingValue());
		}

		Subscriber subscriber = new Subscriber();
		subscriber.setMaxPerSecond(subscriberType.getMaxPerSecond());
		subscriber.setQueueReceiveLimit(validate(subscriberType.getQueueReceiveLimit(), subscriberAddition.getTcos()));
		subscriber.setQueueSendLimit(validate(subscriberType.getQueueSendLimit(), subscriberAddition.getOcos()));
		subscriber.setPriority(subscriberType.getPriority());
		subscriber.setMsisdn(subscriberAddition.getSubid());
		subscriber.setImsi(subscriberAddition.getImsiid());
		subscriber.setCreationDate(new Date());
		subscriber.setCreationUser(RESOURCE_USER);
		subscriber.setSubscriberType(subscriberType);
		subscriber.setPrepaid(subscriberType.getPrepaid());
		return subscriber;
	}

	private void auditLog(LogAction action, Subscriber subscriber) {
		auditService.addSubscriberHistoryEntry(RESOURCE_USER, action, subscriber);
	}

	private int validate(int value1, Integer value2) {
		return value2 == null ? value1 : value2;
	}

	public List<Subscriber> getAll() {
		return subscriberRepository.findAll();
	}
}
