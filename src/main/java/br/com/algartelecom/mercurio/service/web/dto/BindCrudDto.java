package br.com.algartelecom.mercurio.service.web.dto;

import java.util.Date;
import java.util.List;

import br.com.algartelecom.mercurio.relational.model.vo.Bind;
import br.com.algartelecom.mercurio.relational.model.vo.BindEsme;

public class BindCrudDto {

    private Long id;

    private String bindUsername;

    private Integer port;

    private Boolean requireDlr;

    private String type;

    private String serverIp;

    private String defaultCharset;

    private Boolean sendEnquireLink;

    private Integer enquireLinkTimeout;

    private Integer enquireLinkInterval;

    private Integer messagesPerSecond;

    private Integer maxConnections;

    private Date creationDate;

    private String creationUser;

    private Date updateDate;

    private String updateUser;

    private Boolean autoDlr;

    private String category;

    private List<String> bindAllowedIps;

    private List<String> bindAllowedNumbers;

    private List<BindEsme> bindEsmes;

    private Bind relatedAccount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBindUsername() {
        return bindUsername;
    }

    public void setBindUsername(String bindUsername) {
        this.bindUsername = bindUsername;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean getRequireDlr() {
        return requireDlr;
    }

    public void setRequireDlr(Boolean requireDlr) {
        this.requireDlr = requireDlr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getDefaultCharset() {
        return defaultCharset;
    }

    public void setDefaultCharset(String defaultCharset) {
        this.defaultCharset = defaultCharset;
    }

    public Boolean getSendEnquireLink() {
        return sendEnquireLink;
    }

    public void setSendEnquireLink(Boolean sendEnquireLink) {
        this.sendEnquireLink = sendEnquireLink;
    }

    public Integer getEnquireLinkTimeout() {
        return enquireLinkTimeout;
    }

    public void setEnquireLinkTimeout(Integer enquireLinkTimeout) {
        this.enquireLinkTimeout = enquireLinkTimeout;
    }

    public Integer getEnquireLinkInterval() {
        return enquireLinkInterval;
    }

    public void setEnquireLinkInterval(Integer enquireLinkInterval) {
        this.enquireLinkInterval = enquireLinkInterval;
    }

    public Integer getMessagesPerSecond() {
        return messagesPerSecond;
    }

    public void setMessagesPerSecond(Integer messagesPerSecond) {
        this.messagesPerSecond = messagesPerSecond;
    }

    public Integer getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(Integer maxConnections) {
        this.maxConnections = maxConnections;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Boolean getAutoDlr() {
        return autoDlr;
    }

    public void setAutoDlr(Boolean autoDlr) {
        this.autoDlr = autoDlr;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getBindAllowedIps() {
        return bindAllowedIps;
    }

    public void setBindAllowedIps(List<String> bindAllowedIps) {
        this.bindAllowedIps = bindAllowedIps;
    }

    public List<String> getBindAllowedNumbers() {
        return bindAllowedNumbers;
    }

    public void setBindAllowedNumbers(List<String> bindAllowedNumbers) {
        this.bindAllowedNumbers = bindAllowedNumbers;
    }

    public List<BindEsme> getBindEsmes() {
        return bindEsmes;
    }

    public void setBindEsmes(List<BindEsme> bindEsmes) {
        this.bindEsmes = bindEsmes;
    }

    public Bind getRelatedAccount() {
        return relatedAccount;
    }

    public void setRelatedAccount(Bind relatedAccount) {
        this.relatedAccount = relatedAccount;
    }

    @Override
    public String toString() {
        return "BindCrudDto [autoDlr=" + autoDlr + ", bindAllowedIps=" + bindAllowedIps + ", bindAllowedNumbers="
                + bindAllowedNumbers + ", bindEsmes=" + bindEsmes + ", bindUsername=" + bindUsername + ", category="
                + category + ", creationDate=" + creationDate + ", creationUser=" + creationUser + ", defaultCharset="
                + defaultCharset + ", enquireLinkInterval=" + enquireLinkInterval + ", enquireLinkTimeout="
                + enquireLinkTimeout + ", id=" + id + ", maxConnections=" + maxConnections + ", messagesPerSecond="
                + messagesPerSecond + ", port=" + port + ", relatedAccount=" + relatedAccount + ", requireDlr="
                + requireDlr + ", sendEnquireLink=" + sendEnquireLink + ", serverIp=" + serverIp + ", type=" + type
                + ", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
    }

}
