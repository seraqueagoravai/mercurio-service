package br.com.algartelecom.mercurio.service.web.dto;

public class PatternResourceDto {
  
  private boolean result;
  
  public PatternResourceDto(boolean result) {
    this.result = result;
  }

  public boolean isResult() {
    return result;
  }

  public void setResult(boolean result) {
    this.result = result;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("PatternResourceDto [result=").append(result).append("]");
    return builder.toString();
  }
  
}
