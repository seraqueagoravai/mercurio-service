//package br.com.algartelecom.mercurio.service.web.rest;
//
//import com.hazelcast.core.HazelcastInstance;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;
//
//import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
//import static org.springframework.web.bind.annotation.RequestMethod.GET;
//
//@RestController
//@RequestMapping("/hazelcast")
//public class HazelcastResource {
//
//  private static final Logger log = LoggerFactory.getLogger(HazelcastResource.class);
//
//  @Autowired
//  RestTemplate restTemplate;
//  @Value("${mercurio.hazelcast.members}")
//  String hazelcastMembers;
//  @Value("${mercurio.hazelcast.status.path}")
//  String hazelcastStatusPath;
//
//  @Autowired
//  HazelcastInstance hazelInstance;
//
//  @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
//  public ResponseEntity<Object> getClusters() {
//    return restTemplate.getForEntity(formatUrl(), Object.class);
//  }
//
//  private String formatUrl() {
//    return "http://" + hazelcastMembers.split(",")[0].split(":")[0] + hazelcastStatusPath;
//  }
//
//}
