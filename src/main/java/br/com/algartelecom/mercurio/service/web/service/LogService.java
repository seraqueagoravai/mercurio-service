package br.com.algartelecom.mercurio.service.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.AuditLog;
import br.com.algartelecom.mercurio.relational.repositories.AuditLogRepository;

@Service
public class LogService {

	@Autowired
	AuditLogRepository auditlogRepository;

	public AuditLog findAuditLogById(Long id)  {
		return auditlogRepository.findById(id).get();
	}
	
	public List<AuditLog> findAuditLogByEntity(String entity)  {
		return auditlogRepository.findByEntity(entity);
	}
	
	public List<AuditLog> findAuditLogByEntityAndUsername(String entity, String username)  {
		return auditlogRepository.findByEntityAndUsername(entity, username);
	}
}
