package br.com.algartelecom.mercurio.service.web.rest;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.LargeAccount;
import br.com.algartelecom.mercurio.relational.model.vo.vw.LargeAccountVw;
import br.com.algartelecom.mercurio.relational.util.NumberPatternService;
import br.com.algartelecom.mercurio.service.web.service.LargeAccountService;
import br.com.algartelecom.mercurio.service.web.utils.FilterMatch;

@RestController
@RequestMapping("/large-account")
public class LargeAccountResource {

	@Autowired
	LargeAccountService largeAccountService;

	@Autowired
	NumberPatternService numberPatternService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<LargeAccount> getAllVw() {
		return largeAccountService.getAll();
	}

	@RequestMapping(value = "/{number}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<LargeAccount> get(@PathVariable String number) {
		return largeAccountService.getLargeAccount(number)
				.map(result -> new ResponseEntity<LargeAccount>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody LargeAccount largeAccount) throws URISyntaxException {
		return largeAccountService.createLargeAccount(largeAccount);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody LargeAccount largeAccount) throws URISyntaxException {
		return largeAccountService.update(largeAccount);
	}

	@RequestMapping(value = "/{number}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public void delete(@PathVariable String number) {
		largeAccountService.getLargeAccount(number).ifPresent(largeAccountService::delete);
	}

	@RequestMapping(value = "/match/{number}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<LargeAccountVw> match(@PathVariable String number) {
		Iterable<LargeAccountVw> list = largeAccountService.getAllVw();
		FilterMatch filter = new FilterMatch(number, numberPatternService);
		return StreamSupport.stream(list.spliterator(), false).filter(filter::filterMatch).collect(toList());
	}

}
