package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.ROLES;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.Roles;
import br.com.algartelecom.mercurio.relational.model.vo.RolesMapping;
import br.com.algartelecom.mercurio.relational.repositories.RolesMappingRepository;
import br.com.algartelecom.mercurio.relational.repositories.RolesRepository;

@Service
public class RolesService {

    @Autowired
    RolesRepository rolesRepository;

    @Autowired
    RolesMappingRepository rolesMappingRepository;

    @Autowired
    AuditService auditService;


    public List<Roles> findByPlatform(String platform) {
        return rolesRepository.findByPlatform(platform);
    }

    public List<Roles> getAll(){
        return rolesRepository.findAll();
    }

    public ResponseEntity<String> createRoles(Roles roles) {
        roles.setCreationUser(getLoggedUser());
        roles.setCreationDate( new Date());
        rolesRepository.save(roles);
        auditLog(LogAction.SAVE, roles);
        return new ResponseEntity<>(OK);
    }

    public Optional<Roles> getRoleByPlatformAndRole(String plataform, String role) {
        return rolesRepository.findByPlatformAndRoles(plataform, role);
    }

    public void delete(Roles roles) {
        List<RolesMapping> mappings = rolesMappingRepository.findAll();
        for (RolesMapping rolesMapping : mappings){
            if (rolesMapping.getRoles().getId() == roles.getId()){
                rolesMappingRepository.deleteById(rolesMapping.getId());
            }
        }
        rolesRepository.deleteById(roles.getId());
        cleanRoleFromMappings(roles);
        auditLog(LogAction.DELETE, roles);
    }

    private void cleanRoleFromMappings(Roles roles) {
        rolesMappingRepository.deleteById(roles.getId());
    }

    private void auditLog(LogAction action, Roles roles) {
        auditService.register(getLoggedUser(), ROLES, action, roles.getPlatform(), roles);
    }
}
