package br.com.algartelecom.mercurio.service.web.dto;

import br.com.algartelecom.mercurio.relational.model.vo.AccessRole;
import br.com.algartelecom.mercurio.relational.model.vo.RolesMapping;

public class AccessResource {

  private String resource;

  public AccessResource(AccessRole resource) {
    this.resource = resource.getRoleResource();
  }
  
  public AccessResource(String resource) {
    this.resource = resource;
  }

  public AccessResource(RolesMapping mapping) {
    this.resource = mapping.getAccessRole().getRoleResource();
  }

  public String getResource() {
    return resource;
  }

  public void setResource(String resource) {
    this.resource = resource;
  }

}
