package br.com.algartelecom.mercurio.service.web.dto;

import java.util.List;

public class LiveDataResponseDto {

  private Integer mapTotalMt;
  private Integer mapTotalMo;
  private Integer smppTotalMt;
  private Integer smppTotalMo;

  private List<LiveDataMetric> smppValueMt;
  private List<LiveDataMetric> smppValueMo;
  private List<LiveDataMetric> mapValueMt;
  private List<LiveDataMetric> mapValueMo;
  
  public Integer getMapTotalMt() {
    return mapTotalMt;
  }
  public void setMapTotalMt(Integer mapTotalMt) {
    this.mapTotalMt = mapTotalMt;
  }
  public Integer getMapTotalMo() {
    return mapTotalMo;
  }
  public void setMapTotalMo(Integer mapTotalMo) {
    this.mapTotalMo = mapTotalMo;
  }
  public Integer getSmppTotalMt() {
    return smppTotalMt;
  }
  public void setSmppTotalMt(Integer smppTotalMt) {
    this.smppTotalMt = smppTotalMt;
  }
  public Integer getSmppTotalMo() {
    return smppTotalMo;
  }
  public void setSmppTotalMo(Integer smppTotalMo) {
    this.smppTotalMo = smppTotalMo;
  }
  public List<LiveDataMetric> getSmppValueMt() {
    return smppValueMt;
  }
  public void setSmppValueMt(List<LiveDataMetric> smppValueMt) {
    this.smppValueMt = smppValueMt;
  }
  public List<LiveDataMetric> getSmppValueMo() {
    return smppValueMo;
  }
  public void setSmppValueMo(List<LiveDataMetric> smppValueMo) {
    this.smppValueMo = smppValueMo;
  }
  public List<LiveDataMetric> getMapValueMt() {
    return mapValueMt;
  }
  public void setMapValueMt(List<LiveDataMetric> mapValueMt) {
    this.mapValueMt = mapValueMt;
  }
  public List<LiveDataMetric> getMapValueMo() {
    return mapValueMo;
  }
  public void setMapValueMo(List<LiveDataMetric> mapValueMo) {
    this.mapValueMo = mapValueMo;
  }

}
