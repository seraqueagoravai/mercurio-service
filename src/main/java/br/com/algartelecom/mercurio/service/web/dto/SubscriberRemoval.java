package br.com.algartelecom.mercurio.service.web.dto;

import javax.validation.constraints.NotNull;

public class SubscriberRemoval {

  @NotNull(message = "Subscriber subid may not be null")
  private String subid;

  public String getSubid() {
    return subid;
  }

  public void setSubid(String subid) {
    this.subid = subid;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SubscriberRemoval [subid=").append(subid).append("]");
    return builder.toString();
  }

}
