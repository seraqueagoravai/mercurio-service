package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.BIND_SMPP;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.algartelecom.mercurio.relational.model.vo.Bind;
import br.com.algartelecom.mercurio.relational.model.vo.BindAllowedIps;
import br.com.algartelecom.mercurio.relational.model.vo.BindAllowedNumbers;
import br.com.algartelecom.mercurio.relational.model.vo.BindEsme;
import br.com.algartelecom.mercurio.relational.model.vo.BindEsmeBind;
import br.com.algartelecom.mercurio.relational.model.vo.ClusterMember;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.vw.BindVw;
import br.com.algartelecom.mercurio.relational.repositories.BindAllowedIpsRepository;
import br.com.algartelecom.mercurio.relational.repositories.BindAllowedNumbersRepository;
import br.com.algartelecom.mercurio.relational.repositories.BindEsmeBindRepository;
import br.com.algartelecom.mercurio.relational.repositories.BindEsmeRepository;
import br.com.algartelecom.mercurio.relational.repositories.BindRepository;
import br.com.algartelecom.mercurio.relational.repositories.ClusterMemberRepository;
import br.com.algartelecom.mercurio.relational.repositories.vw.BindVwRepository;
import br.com.algartelecom.mercurio.service.web.dto.BindCommandRespDto;
import br.com.algartelecom.mercurio.service.web.dto.BindCrudDto;
import br.com.algartelecom.mercurio.service.web.dto.BindEsmeDto;
import br.com.algartelecom.mercurio.service.web.dto.EsmeDecoratorDto;
import br.com.algartelecom.mercurio.service.web.dto.ServerBindDecoratorDto;
import br.com.algartelecom.mercurio.service.web.utils.BindSmppUtils;
import br.com.algartelecom.mercurio.service.web.utils.SmppCommander;
import br.com.algartelecom.mercurio.smpp.core.model.Esme;
import br.com.algartelecom.mercurio.smpp.core.model.ServerBind;

@Service
public class BindSmppService {

	@Autowired
	BindRepository bindRepository;

	@Autowired
	BindVwRepository bindVwRepository;

	@Autowired
	BindEsmeRepository bindEsmeRepository;

	@Autowired
	BindAllowedIpsRepository bindAllowedIpsRepository;

	@Autowired
	BindAllowedNumbersRepository bindAllowedNumbersRepository;

	@Autowired
	BindEsmeBindRepository bindEsmeBindRepository;

	@Autowired
	AuditService auditService;

	@Autowired
	ClusterMemberRepository clusterMemberRepository;

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	BindSmppUtils bindSmppUtils;

	@Transactional(readOnly = true)
	public List<ClusterMember> getListClusterMember() {
		return clusterMemberRepository.findAll();
	}

	@Transactional(readOnly = true)
	public List<Bind> getListBind() {
		List<Bind> bindList = bindRepository.findAll();

		bindList.sort((c1, c2) -> {
			String usernameC1 = c1.getBindUsername();
			String usernameC2 = c2.getBindUsername();
			return usernameC1.compareTo(usernameC2);
		});

		return bindList;
	}

	@Transactional(readOnly = true)
	public List<BindVw> getListBindVw() {
		List<BindVw> bindList = bindVwRepository.findAll();

		bindList.sort((c1, c2) -> {
			String usernameC1 = c1.getBindUsername();
			String usernameC2 = c2.getBindUsername();
			return usernameC1.compareTo(usernameC2);
		});

		return bindList;
	}

	@Transactional(rollbackFor = Exception.class)
	public void delete(String username) throws Exception {
		try {
			Bind bind = getBind(username).get();

			if (!CollectionUtils.isEmpty(bind.getBindEsmeBinds())) {
				for (BindEsmeBind bindEsmeBind : bind.getBindEsmeBinds()) {
					bindEsmeBindRepository.delete(bindEsmeBind);
				}
			}

			if (!CollectionUtils.isEmpty(bind.getBindAllowedIps())) {
				for (BindAllowedIps bindAllowedIps : bind.getBindAllowedIps()) {
					bindAllowedIpsRepository.delete(bindAllowedIps);
				}
			}

			if (!CollectionUtils.isEmpty(bind.getBindAllowedNumbers())) {
				for (BindAllowedNumbers bindAllowedNumbers : bind.getBindAllowedNumbers()) {
					bindAllowedNumbersRepository.delete(bindAllowedNumbers);
				}
			}

			bindRepository.deleteBybindUsername(username);
			auditLog(LogAction.DELETE, bind);
		} catch (Exception e) {
			throw new Exception(e.getMessage() + e.getCause() + e.getClass());
		}

	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<String> update(BindCrudDto bindDto) {
		Optional<Bind> old = getBind(bindDto.getBindUsername());

		if (!old.isPresent()) {
			return createError("Bind doensn't exists", BAD_REQUEST);
		}

		Bind after = new Bind(bindDto.getId(), bindDto.getBindUsername(), bindDto.getPort(), bindDto.getRequireDlr(),
				bindDto.getType(), bindDto.getServerIp(), bindDto.getDefaultCharset(), bindDto.getSendEnquireLink(),
				bindDto.getEnquireLinkTimeout(), bindDto.getEnquireLinkInterval(), bindDto.getMessagesPerSecond(),
				bindDto.getMaxConnections(), new Date(), getLoggedUser(), new Date(), getLoggedUser(),
				bindDto.getAutoDlr(), bindDto.getCategory(), null, null, null, bindDto.getRelatedAccount());

		Bind before = new Bind(old.get());

		bindRepository.save(after);

		updateBindEsmeRelations(bindDto, after, before);

		updateBindAllowedIpsAndNumbersRelations(bindDto, after, before);

		auditUpdateLog(before, after);
		return new ResponseEntity<>(OK);
	}

	private void updateBindAllowedIpsAndNumbersRelations(BindCrudDto bindDto, Bind after, Bind before) {
		bindDto.getBindAllowedIps().stream().forEach(bindAllowedIpNew -> {
			Optional<BindAllowedIps> biOptional = StreamSupport.stream(before.getBindAllowedIps().spliterator(), false)
					.filter(bindAllowedIpOld -> bindAllowedIpOld.getAllowedIpId().equals(bindAllowedIpNew)).findAny();

			if (!biOptional.isPresent()) {
				bindAllowedIpsRepository
						.save(new BindAllowedIps(null, after, bindAllowedIpNew, new Date(), getLoggedUser()));
			}
		});

		before.getBindAllowedIps().stream().forEach(bindAllowedIpOld -> {
			if (!bindDto.getBindAllowedIps().contains(bindAllowedIpOld.getAllowedIpId())) {
				bindAllowedIpsRepository.delete(bindAllowedIpOld);
			}
		});

		bindDto.getBindAllowedNumbers().stream().forEach(bindAllowedNumberNew -> {
			Optional<BindAllowedNumbers> biOptional = StreamSupport
					.stream(before.getBindAllowedNumbers().spliterator(), false)
					.filter(bindAllowedNumberOld -> bindAllowedNumberOld.getAllowedNumbersId()
							.equals(bindAllowedNumberNew))
					.findAny();

			if (!biOptional.isPresent()) {
				bindAllowedNumbersRepository
						.save(new BindAllowedNumbers(null, after, bindAllowedNumberNew, new Date(), getLoggedUser()));
			}
		});

		before.getBindAllowedNumbers().stream().forEach(bindAllowedNumberOld -> {
			if (!bindDto.getBindAllowedNumbers().contains(bindAllowedNumberOld.getAllowedNumbersId().toString())) {
				bindAllowedNumbersRepository.delete(bindAllowedNumberOld);
			}
		});
	}

	private void updateBindEsmeRelations(BindCrudDto bindDto, Bind after, Bind before) {
		bindDto.getBindEsmes().stream().forEach(bindEsmeNew -> {
			Optional<BindEsmeBind> biOptional = StreamSupport.stream(before.getBindEsmeBinds().spliterator(), false)
					.filter(bindEsmeOld -> bindEsmeOld.getBindEsme().equals(bindEsmeNew)).findAny();

			if (!biOptional.isPresent()) {
				bindEsmeBindRepository.save(new BindEsmeBind(null, bindEsmeNew, after, new Date(), getLoggedUser()));
			}
		});

		before.getBindEsmeBinds().stream().forEach(bindEsmeOld -> {
			if (!bindDto.getBindEsmes().contains(bindEsmeOld.getBindEsme())) {
				bindEsmeBindRepository.delete(bindEsmeOld);
			}
		});
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<String> createBind(BindCrudDto bindDto) {
		if (getBind(bindDto.getBindUsername()).isPresent()) {
			return createError("Bind already exists", BAD_REQUEST);
		}

		Bind bind = new Bind(null, bindDto.getBindUsername(), bindDto.getPort(), bindDto.getRequireDlr(),
				bindDto.getType(), bindDto.getServerIp(), bindDto.getDefaultCharset(), bindDto.getSendEnquireLink(),
				bindDto.getEnquireLinkTimeout(), bindDto.getEnquireLinkInterval(), bindDto.getMessagesPerSecond(),
				bindDto.getMaxConnections(), new Date(), getLoggedUser(), null, null, bindDto.getAutoDlr(),
				bindDto.getCategory(), null, null, null, bindDto.getRelatedAccount());

		Bind savedBind = bindRepository.save(bind);

		if (!CollectionUtils.isEmpty(bindDto.getBindEsmes())) {
			for (BindEsme bindEsme : bindDto.getBindEsmes()) {
				bindEsmeBindRepository.save(new BindEsmeBind(null, bindEsme, savedBind, new Date(), getLoggedUser()));
			}
		}

		if (!CollectionUtils.isEmpty(bindDto.getBindAllowedIps())) {
			for (String allowedIpId : bindDto.getBindAllowedIps()) {
				bindAllowedIpsRepository
						.save(new BindAllowedIps(null, savedBind, allowedIpId, new Date(), getLoggedUser()));
			}
		}

		if (!CollectionUtils.isEmpty(bindDto.getBindAllowedNumbers())) {
			for (String allowedNumber : bindDto.getBindAllowedNumbers()) {
				bindAllowedNumbersRepository
						.save(new BindAllowedNumbers(null, savedBind, allowedNumber, new Date(), getLoggedUser()));
			}
		}

		auditLog(LogAction.SAVE, bind);
		return new ResponseEntity<>(OK);
	}

	@Transactional(readOnly = true)
	public Optional<Bind> getBind(String username) {
		return Optional.ofNullable(bindRepository.findBybindUsername(username));
	}

	@Transactional(readOnly = true)
	public Optional<Bind> getBind(Bind bind) {
		return Optional.ofNullable(bindRepository.findBybindUsername(bind.getBindUsername()));
	}

	@Transactional(readOnly = true)
	public Stream<ClusterMember> getSmppMembersStream() {
		return getListClusterMember().stream().filter(s -> s.getType().equals("smpp"));
	}

	public SmppCommander createSmppCommander(ClusterMember smpp) {
		SmppCommander command = applicationContext.getBean(SmppCommander.class);
		// command.setSmpp(smpp);
		return command;
	}

	public List<BindCommandRespDto> restartBind(String username) {
		ServerBind server = bindSmppUtils.createServerBind(username);
		return getSmppMembersStream().map(this::createSmppCommander).map(command -> command.executeRestartBind(server))
				.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
	}

	public Stream<ServerBindDecoratorDto> getAllBindsDecorator(SmppCommander smppCommander) {
		return smppCommander.getAllBindsFromSmpp().map(Arrays::stream).orElse(Stream.empty())
				.map(s -> new ServerBindDecoratorDto(s, smppCommander.getSmppName()));
	}

	public Stream<BindEsmeDto> getEsmesEntrySet(SmppCommander smppCommander) {
		return smppCommander.getEsmeFromSmpp().entrySet().stream()
				.map(entry -> createBindEsmeDto(entry, smppCommander.getSmppName()));
	}

	public BindEsmeDto createBindEsmeDto(Entry<String, HashSet<Esme>> entry, String server) {
		Set<EsmeDecoratorDto> esmes = entry.getValue().stream().map(es -> new EsmeDecoratorDto(es, server))
				.collect(Collectors.toSet());
		return new BindEsmeDto(entry.getKey(), esmes);
	}

	private void auditLog(LogAction action, Bind bind) {
		auditService.register(getLoggedUser(), BIND_SMPP, action, bind.getBindUsername(), bind);
	}

	private void auditUpdateLog(Bind before, Bind after) {
		auditService.registerUpdate(getLoggedUser(), BIND_SMPP, before.getBindUsername(), before, after);
	}

}
