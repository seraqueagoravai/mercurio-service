package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.CLUSTER_MEMBER;
import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.ClusterMember;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.repositories.ClusterMemberRepository;

@Service
public class ClusterMemberService {

	@Autowired
	ClusterMemberRepository clusterMemberRepository;

	@Autowired
	AuditService auditService;

	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> createClusterMember(ClusterMember clusterMember) {
		if(clusterMemberRepository.findByNameAndIp(clusterMember.getName(), clusterMember.getIp()).isPresent())
			return createError("ClusterMember already exists", BAD_REQUEST);

		clusterMember.setCreationDate(new Date());
		clusterMember.setCreationUser(getLoggedUser());
		clusterMemberRepository.save(clusterMember);
		auditLog(LogAction.SAVE, clusterMember);
		return new ResponseEntity<String>(OK);
	}

	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> update(ClusterMember after) {
		Optional<ClusterMember> old = clusterMemberRepository.findById(after.getId());
		if(!old.isPresent())
			return createError("ClusterMember don't exists", BAD_REQUEST);
		
		ClusterMember before = new ClusterMember(old.get());
		after.setUpdateDate(new Date());
		after.setUpdateUser(getLoggedUser());
		clusterMemberRepository.save(after);
		auditUpdateLog(before, after);
		return new ResponseEntity<String>(OK);
	}

	@Transactional(readOnly = true)
	public Optional<ClusterMember> getClusterMember(Long id) throws UnknownHostException {
		return clusterMemberRepository.findById(id);
	}

	@Transactional(readOnly = true)
	public Optional<ClusterMember> getClusterMember(ClusterMember clusterMember) {
		return clusterMemberRepository.findByNameAndIp(clusterMember.getName(), clusterMember.getIp());
	}

	@Transactional(readOnly = true)
	public List<ClusterMember> getAllClusterMembers() {
		return clusterMemberRepository.findAll();
	}

	@Transactional(rollbackFor = Exception.class )
	public void delete(ClusterMember clusterMember) {
		clusterMemberRepository.deleteById(clusterMember.getId());
		auditLog(LogAction.DELETE, clusterMember);
	}

	private void auditLog(LogAction action, ClusterMember clusterMember) {
		auditService.register(getLoggedUser(), CLUSTER_MEMBER, action, createKey(clusterMember), clusterMember);
	}

	private void auditUpdateLog(ClusterMember before, ClusterMember after) {
		auditService.registerUpdate(getLoggedUser(), CLUSTER_MEMBER, createKey(after), before, after);
	}

	private String createKey(ClusterMember cluster) {
		return "name: " + cluster.getName() + " ip: " + cluster.getIp();
	}

	//not necessary, because now, all transations use ID instead name and ip
	// private String replaceIp(String ip) {
	// 	ip = ip.replace("-", ".");
	// 	return ip;
	// }
}
