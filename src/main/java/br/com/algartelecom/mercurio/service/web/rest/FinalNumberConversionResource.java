package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.model.vo.FinalNumberConversion;
import br.com.algartelecom.mercurio.relational.model.vo.vw.FinalNumberConversionVw;
import br.com.algartelecom.mercurio.service.web.dto.FinalNumberConversionCrudDto;
import br.com.algartelecom.mercurio.service.web.service.FinalNumberConversionService;
import br.com.algartelecom.mercurio.service.web.service.FormatRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/final-number-conversion")
public class FinalNumberConversionResource {

	@Autowired
	FinalNumberConversionService finalNumberConversionService;
	
	@Autowired
	FormatRuleService formatRuleService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public Iterable<FinalNumberConversionVw> getAll() {
		return finalNumberConversionService.getAll();
	}

	@RequestMapping(value = "/{destinationAccount}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<FinalNumberConversion> get(@PathVariable String destinationAccount) {
		return finalNumberConversionService.getFinalNumberConversion(destinationAccount)
				.map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody FinalNumberConversionCrudDto finalNumberConversionCrudDto)
			throws URISyntaxException {
		return finalNumberConversionService.createFinalNumberConversion(finalNumberConversionCrudDto);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody FinalNumberConversionCrudDto finalNumberConversionCrudDto)
			throws URISyntaxException {
		return finalNumberConversionService.update(finalNumberConversionCrudDto);
	}

	@RequestMapping(value = "/{destinationAccount}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public void delete(@PathVariable String destinationAccount) {
		finalNumberConversionService.getFinalNumberConversion(destinationAccount).ifPresent(finalNumberConversionService::delete);
	}

	@RequestMapping(value = "/description/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public String getFormatRuleDescription(@PathVariable Long id) {
		return formatRuleService.getFormatRule(id).get().getDescription();
	}

}
