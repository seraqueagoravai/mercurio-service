package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.model.vo.FormatNumberParts;
import br.com.algartelecom.mercurio.relational.model.vo.FormatRule;
import br.com.algartelecom.mercurio.relational.model.vo.vw.FormatRuleVw;
import br.com.algartelecom.mercurio.relational.util.NumberPatternService;
import br.com.algartelecom.mercurio.service.web.dto.PatternDto;
import br.com.algartelecom.mercurio.service.web.service.FormatRuleService;
import br.com.algartelecom.mercurio.service.web.utils.FilterMatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.StreamSupport;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/format-rule")
public class FormatRuleResource {

	@Autowired
	FormatRuleService formatRuleService;

	@Autowired
	NumberPatternService numberPatternService;

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public List<FormatRule> getAll() {
		return formatRuleService.getAll();
	}

	@RequestMapping(value = "/all", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<FormatNumberParts> findAllNumberParts() throws URISyntaxException {
		return formatRuleService.getAllNumberParts();
	}

	@RequestMapping(value = "/get-format-rule", method = PUT, produces = APPLICATION_JSON_VALUE)
	public List<FormatRule> findByListId(@RequestBody List<Long> idFormatRuleList) throws URISyntaxException {
		return formatRuleService.getAllByListId(idFormatRuleList);
	}

	@RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<FormatRule> get(@PathVariable Long id) {
		return formatRuleService.getFormatRule(id)
				.map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody FormatRule formatRule) throws URISyntaxException {
		return formatRuleService.createFormatRule(formatRule);
	}

	@RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody FormatRule formatRule) throws URISyntaxException {
		return formatRuleService.update(formatRule);
	}

	@RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@PathVariable Long id) throws URISyntaxException {
		return formatRuleService.delete(id);
	}

	@RequestMapping(value = "/validate-regex", method = PUT, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> validateRegExp(@RequestBody PatternDto patternDto) throws URISyntaxException {
		Boolean result = numberPatternService.validateRegex(patternDto.getPattern(), patternDto.getText());
		String resp = result.toString();
		if (result)
			return new ResponseEntity<>(resp, HttpStatus.OK);
		else
			return createError("Number not match with this RegExp", BAD_REQUEST);
	}

	// @RequestMapping(value = "/match/{numberA}/{numberB}", method = GET, produces = APPLICATION_JSON_VALUE)
	// public List<FormatRuleVw> match(@PathVariable String numberA, @PathVariable String numberB) {
	// 	Iterable<FormatRuleVw> list = formatRuleService.getAll();
	// 	FilterMatch filter = new FilterMatch(numberA, numberB, numberPatternService);
	// 	return StreamSupport.stream(list.spliterator(), false).filter(filter::filterMatch).collect(toList());
	// }

}
