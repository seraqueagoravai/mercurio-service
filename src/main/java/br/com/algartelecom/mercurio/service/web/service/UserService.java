package br.com.algartelecom.mercurio.service.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.User;
import br.com.algartelecom.mercurio.relational.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public Iterable<User> getUsers() {
		return userRepository.findAll();
	}
}
