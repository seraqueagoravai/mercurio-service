package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.Subscriber;
import br.com.algartelecom.mercurio.service.web.dto.SubscriberAddition;
import br.com.algartelecom.mercurio.service.web.dto.SubscriberRemoval;
import br.com.algartelecom.mercurio.service.web.service.SPSService;

@RestController
@RequestMapping("/sps")
public class SPSResource {

	@Autowired
	SPSService service;

	@RequestMapping(value = "/deletesubscriber", method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestBody @Valid SubscriberRemoval subscriberRemoval) {
		return service.delete(subscriberRemoval);
	}

	@RequestMapping(value = "/addsubscriber", method = POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody @Valid SubscriberAddition subscriberAddition)
			throws URISyntaxException {
		return service.create(subscriberAddition);
	}

	@RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
	public List<Subscriber> getAll(){
		return service.getAll();
	}

}
