package br.com.algartelecom.mercurio.service.web.dto;

import java.util.HashSet;
import java.util.Set;

public class BindEsmeDto {

  private String name;
  private Set<EsmeDecoratorDto> esmes;
  
  public BindEsmeDto(){
    
  }
  
  public BindEsmeDto(String name, Set<EsmeDecoratorDto> esme) {
    this.name = name;
    this.esmes = esme;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<EsmeDecoratorDto> getEsmes() {
    return esmes;
  }

  public void setEsmes(HashSet<EsmeDecoratorDto> esmes) {
    this.esmes = esmes;
  }

  public Set<EsmeDecoratorDto> putEsme(EsmeDecoratorDto value) {
    this.esmes.add(value);
    return this.esmes;
  }
  
}
