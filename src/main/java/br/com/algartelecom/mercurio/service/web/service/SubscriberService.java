package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.Subscriber;
import br.com.algartelecom.mercurio.relational.repositories.SubscriberRepository;

@Service
public class SubscriberService {

	@Autowired
	AuditService auditService;

	@Autowired
	SubscriberRepository subscriberRepository;

	@Transactional(readOnly = true)
	public Subscriber findByMsisdn(String msisdn) {
		return subscriberRepository.findByMsisdn(msisdn);
	}

	@Transactional(readOnly = true)
	public Iterable<Subscriber> getAllSubscribers() {
		return subscriberRepository.findAll();
	}

	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> create(Subscriber subscriber) {
		if (findByMsisdn(subscriber.getMsisdn()) != null)
			return createError("Subscriber already exists", BAD_REQUEST);
		
		setAttributesBySubscriberType(subscriber);
		subscriber.setCreationDate(new Date());
		subscriber.setCreationUser(getLoggedUser());
		subscriberRepository.save(subscriber);
		auditLog(LogAction.SAVE, subscriber);
		return new ResponseEntity<String>(OK);
	}

	@Transactional(rollbackFor = Exception.class )
	public ResponseEntity<String> update(Subscriber subscriber) {
		Subscriber old = subscriberRepository.findByMsisdn(subscriber.getMsisdn());
		if (old == null)
			return createError("Subscriber don't exists", BAD_REQUEST);
		
		if(old.getSubscriberType() != subscriber.getSubscriberType())
			setAttributesBySubscriberType(subscriber);
		
		Subscriber before =  new Subscriber(old);

		subscriber.setUpdateDate(new Date());
		subscriber.setUpdateUser(getLoggedUser());
		subscriberRepository.save(subscriber);

		if (before.getExpireTime() != null && subscriber.getExpireTime() == null)
			subscriberRepository.nullifySubscriberExpireTime(subscriber.getMsisdn());
		
		auditUpdateLog(before, subscriber);
		return new ResponseEntity<String>(OK);
	}

	@Transactional(rollbackFor = Exception.class )
	public void delete(String msisdn) {
		Subscriber before = subscriberRepository.findByMsisdn(msisdn);
		if (before != null) {
			subscriberRepository.delete(before);
			auditLog(LogAction.DELETE, before);
		}
	}
	
	@Transactional(rollbackFor = Exception.class )
	public void delete(Subscriber subscriber) {
		if(subscriber != null) {
			subscriberRepository.delete(subscriber);
			auditLog(LogAction.DELETE, subscriber);
		}
	}

	private void setAttributesBySubscriberType(Subscriber subscriber) {
		subscriber.setPrepaid(subscriber.getSubscriberType().getPrepaid());
		subscriber.setImsi(subscriber.getSubscriberType().getImsi());
		subscriber.setPriority(subscriber.getSubscriberType().getPriority());
		subscriber.setMaxPerSecond(subscriber.getSubscriberType().getMaxPerSecond());
	}
	
	private void auditLog(LogAction action, Subscriber subscriber) {
		auditService.addSubscriberHistoryEntry(getLoggedUser(), action, subscriber);
	}

	private void auditUpdateLog(Subscriber before, Subscriber after) {
		auditService.addSubscriberHistoryUpdate(getLoggedUser(), before, after);
	}

}
