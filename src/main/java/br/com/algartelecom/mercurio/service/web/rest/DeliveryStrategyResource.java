package br.com.algartelecom.mercurio.service.web.rest;

import br.com.algartelecom.mercurio.relational.model.vo.DeliveryStrategy;
import br.com.algartelecom.mercurio.relational.model.vo.Operation;
import br.com.algartelecom.mercurio.service.web.service.DeliveryStrategyService;
import br.com.algartelecom.mercurio.service.web.utils.ConstantsFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.Arrays;

import static br.com.algartelecom.mercurio.service.web.utils.ErrorHandlerUtil.createError;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/delivery-strategy")
public class DeliveryStrategyResource {

    @Autowired
    DeliveryStrategyService deliveryStrategyService;

    @RequestMapping(value = "/{network}/{error}/{level}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DeliveryStrategy> get(@PathVariable String network, @PathVariable Integer error,
            @PathVariable Integer level) {
        return deliveryStrategyService.getDeliveryStrategy(network, error, level)
                .map(result -> new ResponseEntity<DeliveryStrategy>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public Iterable<DeliveryStrategy> getAll() {
        return deliveryStrategyService.getAll();
    }

    @RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> create(@RequestBody DeliveryStrategy deliveryStrategy) throws URISyntaxException {
        return deliveryStrategyService.getDeliveryStrategy(deliveryStrategy)
                .map(old -> createError("DeliveryStrategy already exists", BAD_REQUEST))
                .orElse(deliveryStrategyService.createDeliveryStrategy(deliveryStrategy));
    }

    @RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> update(@RequestBody DeliveryStrategy deliveryStrategy) throws URISyntaxException {
        return deliveryStrategyService.getDeliveryStrategy(deliveryStrategy)
                .map(old -> deliveryStrategyService.update(old, deliveryStrategy))
                .orElse(createError("DeliveryStrategy don't exists", BAD_REQUEST));
    }

    @RequestMapping(value = "/{network}/{error}/{level}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public void delete(@PathVariable String network, @PathVariable Integer error, @PathVariable Integer level) {
        deliveryStrategyService.getDeliveryStrategy(network, level, error).ifPresent(deliveryStrategyService::delete);
    }

    @RequestMapping(value = "/operations", method = GET, produces = APPLICATION_JSON_VALUE)
    public String[] getOperations() {
        return Arrays.stream(Operation.values()).map(Enum::name).toArray(String[]::new);
    }

    @RequestMapping(value = "/maperrors", method = GET, produces = APPLICATION_JSON_VALUE)
    public String[] getMapErrors() {
        return ConstantsFilters.getListMapErrors();
    }

    @RequestMapping(value = "/smpperrors", method = GET, produces = APPLICATION_JSON_VALUE)
    public String[] getSmppErrors() {
        return ConstantsFilters.getListSmppErrors();
    }

}
