package br.com.algartelecom.mercurio.service.web.dto;

public class BindStatusDto {

	private String name;
	private String status;
	public static final String CONNECTED = "connected";
	public static final String DISCONNECTED = "disconnected";

	public BindStatusDto() {

	}

	public BindStatusDto(String name, String status) {
		super();
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BindStatusDto [name=").append(name).append(", status=").append(status).append("]");
		return builder.toString();
	}

}
