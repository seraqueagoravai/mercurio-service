package br.com.algartelecom.mercurio.service.web.dto;

public class JsonValueDto {

  private Object values;

  public JsonValueDto(Object values) {
    this.values = values;
  }

  public Object getValues() {
    return values;
  }

  public void setValues(Object values) {
    this.values = values;
  }

}
