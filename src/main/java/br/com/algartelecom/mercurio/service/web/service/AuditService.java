package br.com.algartelecom.mercurio.service.web.service;

import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.mercurio.relational.model.vo.AuditLog;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.Subscriber;
import br.com.algartelecom.mercurio.relational.model.vo.SubscriberHistory;
import br.com.algartelecom.mercurio.relational.repositories.AuditLogRepository;
import br.com.algartelecom.mercurio.relational.repositories.SubscriberHistoryRepository;
import br.com.algartelecom.mercurio.service.web.dto.ObjectHistory;

@Service
public class AuditService {

	private static final Logger log = LoggerFactory.getLogger(AuditService.class);
	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	AuditLogRepository auditLogRepository;

	@Autowired
	SubscriberHistoryRepository subscriberHistoryRepository;

	@Transactional(rollbackFor = Exception.class )
	@Async
	public void register(String user, Object entity, LogAction action, String entityId, Object content) {
		AuditLog item = new AuditLog(entity.toString(), user, new Date(), action.toString(), entityId,
		parseObject(content));
		auditLogRepository.save(item);
	}

	@Transactional(rollbackFor = Exception.class )
	@Async
	public void registerUpdate(String user, Object entity, String entityId, Object before, Object after) {
		ObjectHistory history = new ObjectHistory(before, after);
		AuditLog item = new AuditLog(entity.toString(), user, new Date(), LogAction.UPDATE.toString(), entityId,
		parseObject(history));
		auditLogRepository.save(item);
	}

	@Transactional(rollbackFor = Exception.class )
	@Async
	public void addSubscriberHistoryEntry(String user, LogAction action, Subscriber subscriber) {
		SubscriberHistory entry = new SubscriberHistory(subscriber.getMsisdn(), new Date(), user, action.toString(),
				parseObject(subscriber));
		subscriberHistoryRepository.save(entry);
	}

	@Transactional(rollbackFor = Exception.class )
	@Async
	public void addSubscriberHistoryUpdate(String user, Subscriber before, Subscriber after) {
		ObjectHistory history = new ObjectHistory(before, after);
		SubscriberHistory entry = new SubscriberHistory(before.getMsisdn(), new Date(), user,
				LogAction.UPDATE.toString(), parseObject(history));
		subscriberHistoryRepository.save(entry);
	}

	private String parseObject(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error("Error parsing object for logging ->", e);
			return "{\"error\":\"Not able to parse object\"}";
		}
	}
}
