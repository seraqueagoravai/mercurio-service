//package br.com.algartelecom.mercurio.service.web.rest;
//
//import br.com.algartelecom.analytics.model.GraphRepresentation;
//import br.com.algartelecom.analytics.model.SMSDataSeries;
//import br.com.algartelecom.analytics.model.repository.GraphStatisticsRepository;
//import br.com.algartelecom.analytics.model.repository.ShortMessageStatisticsRepository;
//import br.com.algartelecom.mercurio.map.model.constant.MapErrorCodes;
//import br.com.algartelecom.mercurio.service.web.dto.GraphRequestDto;
//import br.com.algartelecom.mercurio.service.web.dto.JsonValueDto;
//import br.com.algartelecom.mercurio.smpp.core.constant.SmppErrorCodes;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.supercsv.io.CsvResultSetWriter;
//
//import javax.servlet.http.HttpServletResponse;
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//
//import static br.com.algartelecom.analytics.model.StatisticsConstants.ERRORS;
//import static org.springframework.web.bind.annotation.RequestMethod.GET;
//import static org.supercsv.prefs.CsvPreference.STANDARD_PREFERENCE;
//
//@RestController
//@RequestMapping("/statistics")
//public class StatisticsResource {
//
//  private static final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//  private static final SimpleDateFormat rsFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//  private static final Logger log = LoggerFactory.getLogger(StatisticsResource.class);
//
//  @Autowired
//  GraphStatisticsRepository queryRepository;
//  @Autowired
//  ShortMessageStatisticsRepository smsRepository;
//  @Autowired
//  DataSource dataSource;
//
//  @RequestMapping(value = "/graph/generate", method = GET)
//  public GraphRepresentation getErrorsByTypeAndDate(GraphRequestDto r) throws ParseException {
//    if (ERRORS.equals(r.type))
//      return transformResponse(getErrorData(r));
//    else
//      return transformResponse(getNumberData(r));
//  }
//
//  @RequestMapping(value = "/count", method = GET)
//  public JsonValueDto countByDates(@RequestParam String initialDate, @RequestParam String finalDate)
//      throws Exception {
//    Integer total = smsRepository.countByDate(parseDate(initialDate), parseDate(finalDate));
//    return new JsonValueDto(total);
//  }
//
//  @RequestMapping(value = "/report", method = GET, produces = "text/csv")
//  public void getCsvReport(HttpServletResponse response, @RequestParam String initialDate,
//      @RequestParam String finalDate) throws Exception {
//    response.setContentType("text/csv");
//    String headerKey = "Content-Disposition";
//    String headerValue = String.format("attachment; filename=\"%s\"", "report.csv");
//    response.setHeader(headerKey, headerValue);
//    Connection conn = dataSource.getConnection();
//    conn.setAutoCommit(false);
//    Statement st = conn.createStatement();
//    st.setFetchSize(1000);
//    ResultSet rs =
//        st.executeQuery("select * from short_message_statistic where  submission_date >= '"
//            + parseRsDate(initialDate, false) + "' and submission_date <= '"
//            + parseRsDate(finalDate, true) + "' ");
//    CsvResultSetWriter w = new CsvResultSetWriter(response.getWriter(), STANDARD_PREFERENCE);
//    w.write(rs);
//    response.flushBuffer();
//    rs.close();
//    w.close();
//  }
//
//  private GraphRepresentation transformResponse(List<SMSDataSeries> series) {
//    GraphRepresentation result = new GraphRepresentation();
//    List<Integer> resultData = new ArrayList<Integer>();
//    for (SMSDataSeries data : series) {
//      String metric = data.getMetric() != null ? data.getMetric() : "null";
//      result.getLabels().add(metric.split(" ")[0]);
//      resultData.add(data.getTotal());
//    }
//    result.getData().add(resultData);
//    return result;
//  }
//
//  private List<SMSDataSeries> getNumberData(GraphRequestDto r) throws ParseException {
//    String number = "specify".equals(r.number) ? r.customNumber : r.number;
//    return queryRepository.findByNumber(parseDate(r.initialDate), parseDate(r.finalDate), number,
//        r.type);
//  }
//
//  private List<SMSDataSeries> getErrorData(GraphRequestDto r) throws ParseException {
//    if ("specify".equals(r.number))
//      r.number = r.customNumber;
//    return queryRepository.findByError(r.errorType, r.errorCode, parseDate(r.initialDate),
//        parseDate(r.finalDate), r.metric, r.number);
//  }
//
//  @RequestMapping(value = "/smpp/errorcodes")
//  public JsonValueDto getSmppErrorCodes() {
//    Object[] resultArray =
//        Arrays.stream(SmppErrorCodes.values()).map(SmppErrorCodes::getCode).sorted().toArray();
//    return new JsonValueDto(resultArray);
//  }
//
//  @RequestMapping(value = "/map/errorcodes")
//  public JsonValueDto getMapErrorCodes() {
//    Object[] resultArray =
//        Arrays.stream(MapErrorCodes.values()).map(MapErrorCodes::getCode).sorted().toArray();
//    return new JsonValueDto(resultArray);
//  }
//
//  private Date parseDate(String date) throws ParseException {
//    return formatter.parse(date);
//  }
//
//  private String parseRsDate(String date, boolean last) throws ParseException {
//    return rsFormatter.format(formatter.parse(date));
//  }
//
//}
