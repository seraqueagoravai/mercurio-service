package br.com.algartelecom.mercurio.service.web.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URISyntaxException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algartelecom.mercurio.relational.model.vo.Subscriber;
import br.com.algartelecom.mercurio.relational.model.vo.SubscriberType;
import br.com.algartelecom.mercurio.relational.repositories.SubscriberHistoryRepository;
import br.com.algartelecom.mercurio.service.web.dto.SubscriberHistoryDto;
import br.com.algartelecom.mercurio.service.web.service.SubscriberService;
import br.com.algartelecom.mercurio.service.web.service.SubscriberTypeService;

@RestController
@RequestMapping("/subscriber")
public class SubscriberResource {
	
  @Autowired
  SubscriberService subscriberService;
  @Autowired
  SubscriberTypeService subscriberTypeService;
  @Autowired
  SubscriberHistoryRepository subscriberHistoryRepository;

  @RequestMapping(value = "/{msisdn}", method = GET, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Subscriber> get(@PathVariable String msisdn) {
    return Optional.ofNullable(subscriberService.findByMsisdn(msisdn))
        .map(result -> new ResponseEntity<Subscriber>(result, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @RequestMapping(value = "/history/{msisdn}", method = GET, produces = APPLICATION_JSON_VALUE)
  public Iterable<SubscriberHistoryDto> getHistory(@PathVariable String msisdn) {
    return subscriberHistoryRepository.findByMsisdn(msisdn).stream().map(SubscriberHistoryDto::new)
        .collect(Collectors.toList());
  }

  @RequestMapping(value = "/types/list", method = GET, produces = APPLICATION_JSON_VALUE)
  public Iterable<SubscriberType> getAllTypes() {
    return subscriberTypeService.getAll();
  }

  @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
  public Iterable<Subscriber> getAll() {
    return subscriberService.getAllSubscribers();
  }

  @RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<String> create(@RequestBody Subscriber subscriber)
      throws URISyntaxException {
    return subscriberService.create(subscriber);
  }

  @RequestMapping(method = PUT, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<String> update(@RequestBody Subscriber subscriber)
      throws URISyntaxException {
	  return subscriberService.update(subscriber);
  }

  @RequestMapping(value = "/{msisdn}", method = DELETE, produces = APPLICATION_JSON_VALUE)
  public void delete(@PathVariable String msisdn) {
	  subscriberService.delete(msisdn);
  }
}
