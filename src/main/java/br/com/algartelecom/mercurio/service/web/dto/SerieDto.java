package br.com.algartelecom.mercurio.service.web.dto;

public class SerieDto {
  
  private String name;
  
  public SerieDto(){
    
  }
  
  public SerieDto(String name){
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  

}
