package br.com.algartelecom.mercurio.service.web.dto;

import java.util.List;

import br.com.algartelecom.mercurio.relational.model.vo.Bind;
import br.com.algartelecom.mercurio.relational.model.vo.FormatRule;

public class FinalNumberConversionCrudDto {

    private Bind destinationAccount;
    private List<FormatRule> rules;

    public Bind getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(Bind destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public List<FormatRule> getRules() {
        return rules;
    }

    public void setRules(List<FormatRule> rules) {
        this.rules = rules;
    }

    @Override
    public String toString() {
        return "FinalNumberConversionCrudDto{" +
                "destinationAccount=" + destinationAccount +
                ", rules=" + rules +
                '}';
    }
}
