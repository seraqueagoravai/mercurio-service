package br.com.algartelecom.mercurio.service.web.service;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.ROUTE_RULE;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.relational.model.vo.RouteRule;
import br.com.algartelecom.mercurio.relational.repositories.RouteRuleRepository;

@Service
public class RouteRuleService {
	
	  @Autowired
	  RouteRuleRepository routeRuleRepository;
	  
	  @Autowired
	  AuditService auditService;
	  
		public Optional<RouteRule> getRouteRule(RouteRule routeRule) {
			return routeRuleRepository.findById(routeRule.getId());
		}
		
		public Optional<RouteRule> getRouteRule(Long id) {
			return routeRuleRepository.findById(id);
		}

		public ResponseEntity<String> update(RouteRule before, RouteRule after) {
			routeRuleRepository.save(after);
			auditUpdateLog(before, after);
			return new ResponseEntity<String>(OK);
		}

		public List<RouteRule> getAll() {
			return routeRuleRepository.findAll();
		}
		
		public ResponseEntity<String> createRoute(RouteRule routeRule) {
			routeRuleRepository.save(routeRule);
			auditLog(LogAction.SAVE, routeRule);
			return new ResponseEntity<String>(OK);
		}

		private void auditLog(LogAction action, RouteRule route) {
			auditService.register(getLoggedUser(), ROUTE_RULE, action, route.getId().toString(), route);
		}

		private void auditUpdateLog(RouteRule before, RouteRule after) {
			auditService.registerUpdate(getLoggedUser(), ROUTE_RULE, before.getId().toString(), before, after);
		}



}
