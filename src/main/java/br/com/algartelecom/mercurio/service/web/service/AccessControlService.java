package br.com.algartelecom.mercurio.service.web.service;

import br.com.algartelecom.mercurio.relational.model.vo.AccessRole;
import br.com.algartelecom.mercurio.relational.model.vo.RolesMapping;
import br.com.algartelecom.mercurio.relational.repositories.AccessRoleRepository;
import br.com.algartelecom.mercurio.relational.repositories.RolesMappingRepository;
import br.com.algartelecom.mercurio.relational.repositories.RolesRepository;
import br.com.algartelecom.mercurio.service.web.dto.AccessControl;
import br.com.algartelecom.mercurio.service.web.dto.AccessControlUpdateDto;
import br.com.algartelecom.mercurio.service.web.dto.AccessRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity.ACCESS_CONTROL;
import static br.com.algartelecom.mercurio.service.web.utils.SecurityUtils.getLoggedUser;

@Service
public class AccessControlService {

    @Autowired
    RolesMappingRepository rolesMappingRepository;
    @Autowired
    RolesRepository rolesRepository;
    @Autowired
    AuditService auditService;
    @Autowired
    AccessRoleRepository accessRoleRepository;

    public Set<AccessControl> getAllAccessControl() {
        Iterable<RolesMapping> rolemappings = rolesMappingRepository.findAll();
        Set<AccessControl> result = getAvailableRoles();

        List<AccessRole> resources =  accessRoleRepository.findAll();

        result.stream().forEach(rolemap -> {
            resources.stream().forEach(resource -> {

                Optional<RolesMapping> opt = StreamSupport.stream(rolemappings.spliterator(), false)
                        .filter(t -> t.getAccessRole().getId() == resource.getId() && t.getRoles() != null
                                && t.getRoles().getRoles().equals(rolemap.getRole())).findAny();

                if (opt.isPresent()) {
                    if (opt.get().getRoles().getRoles().contains(rolemap.getRole())) {
                        rolemap.getRules().add(new AccessRule(resource.getRoleResource(), true));
                    } else {
                        rolemap.getRules().add(new AccessRule(resource.getRoleResource(), false));
                    }
                } else {
                    rolemap.getRules().add(new AccessRule(resource.getRoleResource(), false));
                };
            });
        });

        return result;
    }

    public void updateAccessControl(AccessControlUpdateDto access) {
        AccessControl accessControlNew = access.getAccessControlNew();
        AccessControl accessControlOld = access.getAccessControlOld();
        List<RolesMapping> mappings = rolesMappingRepository.findAll();
        adjustRolesMapping(accessControlNew, mappings);
        auditUpdateLog(accessControlOld, accessControlNew);
    }

    private void adjustRolesMapping(AccessControl accessList, List<RolesMapping> mappings) {
        accessList.getRules().stream().forEach(s -> {
            if (s.isAllowed()) {
                int count = 0;
                for(RolesMapping rolesMappings : mappings){
                    if (rolesMappings.getRoles().getRoles().equals(accessList.getRole())
                            && rolesMappings.getAccessRole().getRoleResource().equals(s.getResource())){
                        count = 1;
                        break;
                    }
                }
                if (count == 0) {
                    RolesMapping rolesMappingNew = new RolesMapping();
                    rolesMappingNew.setRoles(rolesRepository.findByRoles(accessList.getRole()));
                    rolesMappingNew.setAccessRole(accessRoleRepository.findByRoleResource(s.getResource()));
                    rolesMappingNew.setCreationDate(new Date());
                    rolesMappingNew.setCreationUser(getLoggedUser());
                    rolesMappingRepository.save(rolesMappingNew);
                }
            } else {
                for(RolesMapping rolesMapping : mappings){
                    if (rolesMapping.getRoles().getRoles().equals(accessList.getRole())
                            && rolesMapping.getAccessRole().getRoleResource().equals(s.getResource())){
                        rolesMappingRepository.deleteById(rolesMapping.getId());
                    }
                }
            }
        });
    }

    private void auditUpdateLog(AccessControl before, AccessControl after) {
        auditService.registerUpdate(getLoggedUser(), ACCESS_CONTROL, before.getRole(), before, after);
    }

    private Set<AccessControl> getAvailableRoles() {
        Set<AccessControl> result = new HashSet<AccessControl>();
        rolesRepository.findByPlatform("smsc").stream()
                .forEach(t -> result.add(new AccessControl(t.getRoles())));
        return result;
    }
}
