package br.com.algartelecom.mercurio.service.web.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.algartelecom.mercurio.relational.model.vo.Bind;
import br.com.algartelecom.mercurio.relational.model.vo.BindEsme;
import br.com.algartelecom.mercurio.relational.model.vo.ClusterMember;
import br.com.algartelecom.mercurio.relational.repositories.BindEsmeRepository;
import br.com.algartelecom.mercurio.relational.repositories.BindRepository;
import br.com.algartelecom.mercurio.smpp.core.constant.BindType;
import br.com.algartelecom.mercurio.smpp.core.constant.CommandStatus;
import br.com.algartelecom.mercurio.smpp.core.model.CommandResp;
import br.com.algartelecom.mercurio.smpp.core.model.Esme;
import br.com.algartelecom.mercurio.smpp.core.model.ServerBind;


@Service
public class BindSmppUtils {

    @Autowired
    BindRepository bindRepository;

    @Autowired
    BindEsmeRepository bindEsmeRepository;

    public CommandResp createResponseError(String message, ClusterMember cluster, Exception e) {
        CommandResp commandResp = new CommandResp();
        commandResp.setStatus(CommandStatus.ERROR);
        commandResp.setMessage(
                message + " " + cluster.getIp() + ":" + cluster.getPort() + " - " + e.getMessage());
        return commandResp;
    }

    public Esme convertEsme(Long id) {
        BindEsme bindEsme = bindEsmeRepository.findById(id).get();
        return new Esme(null, bindEsme.getName(), bindEsme.getHostname(), bindEsme.getPort(),
                bindEsme.getSystemType(), bindEsme.getUserEsme(), bindEsme.getPassword(),
                bindEsme.getSmppVersion(), bindEsme.getTon(), bindEsme.getNpi(), bindEsme.getAddress(),
                BindType.valueOf(bindEsme.getSmppBindType()));
    }

    public ServerBind createServerBind(String username) {
        Bind bind = bindRepository.findBybindUsername(username);
        ServerBind serverBind = new ServerBind();
        serverBind.setName(bind.getBindUsername());
        serverBind.setPort(bind.getPort());
        serverBind.setType(bind.getType());
        serverBind.setDefaultCharset(bind.getDefaultCharset());
        serverBind.setServerIp(bind.getServerIp());
        serverBind.setSendEnquireLink(bind.getSendEnquireLink());
        serverBind.setEnquireLinkInterval(bind.getEnquireLinkInterval());
        serverBind.setEnquireLinkTimeout(bind.getEnquireLinkTimeout());
        serverBind.setMaxConnections(bind.getMaxConnections());
        serverBind.setMessagesPerSecond(bind.getMessagesPerSecond());
        initEsmes(bind, serverBind);
        return serverBind;
    }

    public void initEsmes(Bind bind, ServerBind server) {
        // BindEsme bindEsme = null;
        // List<Esme> list = new ArrayList<>();
        // Esme esme = null;
        // List<BindEsme> listId = bind.getBindEsmes() == null ? new ArrayList<>() : bind.getBindEsmes();
        // for (BindEsme bindEsmeId : listId) {
        //     bindEsme = bindEsmeRepository.findById(bindEsmeId.getId()).get();
        //     if (bindEsme != null)
        //         esme = new Esme(null, bindEsme.getName(), bindEsme.getHostname(), bindEsme.getPort(),
        //                 bindEsme.getSystemType(), bindEsme.getUserEsme(), bindEsme.getPassword(),
        //                 bindEsme.getSmppVersion(), bindEsme.getTon(), bindEsme.getNpi(), bindEsme.getAddress(),
        //                 BindType.valueOf(bindEsme.getSmppBindType()));
        //     list.add(esme);
        // }
        // server.setEsmes(list);
    }

    public String replaceIp(String ip) {
        ip = ip.replace("/", "");
        return ip;
    }

    public String replaceUrl(String url, String ip, String port) {
        url = url.replace("ip", ip);
        url = url.replace("port", port);
        return url;
    }

    public String replaceUrlWithParam(String url, String ip, String port, String username) {
        url = url.replace("ip", ip);
        url = url.replace("port", port);
        url = url.replace("param", username);
        return url;
    }

}
