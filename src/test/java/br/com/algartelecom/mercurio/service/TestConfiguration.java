package br.com.algartelecom.mercurio.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@ComponentScan(basePackages={"br.com.algartelecom.mercurio.service.web.handlers"})
public class TestConfiguration {

  @Bean
  public Validator validator() {
    return new LocalValidatorFactoryBean();
  }
  
  @Bean
  public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
      return new PropertySourcesPlaceholderConfigurer();
  }

}
