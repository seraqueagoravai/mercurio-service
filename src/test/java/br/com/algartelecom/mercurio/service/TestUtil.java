package br.com.algartelecom.mercurio.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.mockito.Mockito;

import br.com.algartelecom.mercurio.relational.model.vo.ConfigurationEntity;
import br.com.algartelecom.mercurio.relational.model.vo.LogAction;
import br.com.algartelecom.mercurio.service.web.service.AuditService;

public class TestUtil {

  public static byte[] loadFile(String location) throws Exception {
    URL resourceUrl = TestUtil.class.getResource(location);
    Path resourcePath = Paths.get(resourceUrl.toURI());
    return Files.readAllBytes(resourcePath);
  }

  public static void verifySaveAudition(AuditService auditService, ConfigurationEntity entity,
      Class clazz) {
    Mockito.verify(auditService).register(anyString(), eq(entity), eq(LogAction.SAVE), anyString(),
        Mockito.any(clazz));
  }

  public static void verifyDeleteAudition(AuditService auditService, ConfigurationEntity entity,
      Class clazz) {
    Mockito.verify(auditService).register(anyString(), eq(entity), eq(LogAction.DELETE), anyString(),
        Mockito.any(clazz));
  }

  public static void verifyUpdateAudition(AuditService auditService, ConfigurationEntity entity,
      Class clazz) {
    Mockito.verify(auditService).registerUpdate(anyString(), eq(entity), anyString(),
        Mockito.any(clazz), Mockito.any(clazz));
  }
}
